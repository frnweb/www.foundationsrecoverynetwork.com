<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PIAG3PcyHjq2A28GixnPHg1e4zInDaatRtcgGZr+D7e1G8ZtGxLQAy0KcJGAmKVWO2uQjhxB8ETqmxugrjAxUg==');
define('SECURE_AUTH_KEY',  'rVMUka9h7P2fU0pLhz9h8fdf2B9AaxVAalFQVL8AnK35UMeYTTJ4oOPrZgu93IN/TD85mli1399cNroVLNlKmw==');
define('LOGGED_IN_KEY',    'Neml3+ymcvGkDhPgm/fRbcTgVRnMzqE/PLZxpuJVdw+Rs0VkuAkD2ZW0xQBR85Fg2TXrxcqc7EJk3GicPffjZg==');
define('NONCE_KEY',        'Bq6quaQ30OQH+LVu6HH6ZxvMng/WV7wN7CKhV7MUTuweId/EtzTu+NkkxRVNtkJSu836DBcrsg74VTRlMwg9cg==');
define('AUTH_SALT',        'C/xzmcmJ9qfynVqcZYXAGKxF9GeGCVesm5RZWWUPOCSI49LQmlJWxS1sNCGFBczbjWvzHGFFzHcE0q2KY4fiIQ==');
define('SECURE_AUTH_SALT', '4avJHqhcET/zjGXCD/kJxYXZ6oA3Ab83+I9RpHWe1e8EjWkXWDz+qY4oEmX8uZplza2zWoUBKQaxpIZmdxkbFw==');
define('LOGGED_IN_SALT',   'xTmCxNDIkOxwVqi0ep+J8Qn/n8Ma50PWy/YF7v0MW93k3V7iZxSRttYUl7eMEgsMX2fTonXUKBieKa0Aj2mbig==');
define('NONCE_SALT',       'cVMmAt3T/wZ59fnFdNCGE4O/v0fTkEiZ6exyp4NvL8sjzK4u7HJf6X1G0UsgACfAlEs2VkmJaYqkLWMOlI2bcg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_ywudptqavb_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
