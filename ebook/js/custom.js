jQuery(document).ready(function() {
	jQuery('.section .inner').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated fadeInUp', // Class to add to the elements when they are visible
	    offset: 100    
	   });   
});  

///Some more specific animations for section 2
jQuery(document).ready(function() {
	jQuery('#pagetwo h2').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated zoomIn', // Class to add to the elements when they are visible
	    offset: 100    
	   });   
});  

///Some more specific animations for section 2
jQuery(document).ready(function() {
	jQuery('#pagethree .leftside').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated slideInLeft', // Class to add to the elements when they are visible
	    offset: 100    
	   });   
});  

///Some more specific animations for section 2
jQuery(document).ready(function() {
	jQuery('#pageseven .leftside, #pagenine .icon').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated slideInLeft', // Class to add to the elements when they are visible
	    offset: 100    
	   });   
}); 

///Some more specific animations for section 2
jQuery(document).ready(function() {
	jQuery('#pageseven .rightside').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated slideInRight', // Class to add to the elements when they are visible
	    offset: 100    
	   });   
});  
        
///Icon Bounce in Animation
jQuery(document).ready(function() {
	jQuery('#pagetwo .icon, #pageten .icon, #pagethirteen .icon').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated bounceIn', // Class to add to the elements when they are visible
	    offset: 100    
	   });   
});  
        		  
//smooth scroll for arrows
jQuery.fn.extend({
  scrollTo : function(speed, easing) {
    return this.each(function() {
      var targetOffset = $(this).offset().top;
      $('html,body').animate({scrollTop: targetOffset}, speed, easing);
    });
  }
});


$('.next-section').click(function(e){
    e.preventDefault();
    var $this = $(this),
        $next = $this.parent().next();
    
    $next.scrollTo(400, 'linear');
});

$('.prev-section').click(function(e){
    e.preventDefault();
    var $this = $(this),
        $prev = $this.parent().prev();

    $prev.scrollTo(400, 'linear');
});

//fancy box initiation 
$(document).ready(function() {
	$(".thumbs").fancybox({
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		helpers: {
			overlay: {
			  locked: false
			}
		  }
	});
});

//More Info Box Reveal
$("#pagefourteen .moreinfo").click(function(){
    $("#pagefourteen .moreinfo-container").toggleClass("reveal");
});

$("#pagesixteen .moreinfo").click(function(){
    $("#pagesixteen .moreinfo-container").toggleClass("reveal");
});

//Plane Animation
$("#pagenine .plane-animation").click(function(){
    $("#pagenine .plane-animation").toggleClass("reveal");
});
$("#pagenine .plane-animation").click(function(){
    $("#pagenine .planeinfo-background").toggleClass("reveal");
});
$("#pagenine .plane-animation").click(function(){
    setTimeout(function(){
       $("#pagenine .exit-plane").addClass("reveal");
   }, 2000);
});
$("#pagenine .plane-animation").click(function(){
    setTimeout(function(){
       $("#pagenine .planeinfo").addClass("reveal");
   }, 1000);
});
$("#pagenine .exit-plane").click(function(){
    $("#pagenine .exit-plane").removeClass("reveal");
});
$("#pagenine .exit-plane").click(function(){
    $("#pagenine .planeinfo-background").removeClass("reveal");
});
$("#pagenine .exit-plane").click(function(){
    $("#pagenine .plane-animation").removeClass("reveal");
});
$("#pagenine .exit-plane").click(function(){
    $("#pagenine .planeinfo").removeClass("reveal");
});

//Puzzle Animation
function puzzle_count() {
	var el = $(".place");
	//alert(el.length);
	if (el.length>4) {
		$("#person-standing").addClass("animate");
	};
}
$("#puzzle-one").click(function(){
    $("#puzzle-one").toggleClass("place");
    puzzle_count();
});

$("#puzzle-two").click(function(){
    $("#puzzle-two").toggleClass("place");
    puzzle_count();
});

$("#puzzle-three").click(function(){
    $("#puzzle-three").toggleClass("place");
    puzzle_count();
});

$("#puzzle-four").click(function(){
    $("#puzzle-four").toggleClass("place");
    puzzle_count();
});

$("#puzzle-five").click(function(){
    $("#puzzle-five").toggleClass("place");
    puzzle_count();
});


//ScaleReveal
$("#pagesix .left-scale").click(function(){
    $("#pagesix .residentialcontainer").addClass("reveal");
});
$("#pagesix .exit-residential").click(function(){
    $("#pagesix .residentialcontainer").removeClass("reveal");
});
$("#pagesix .right-scale").click(function(){
    $("#pagesix .outpatientcontainer").addClass("reveal");
});
$("#pagesix .exit-outpatient").click(function(){
    $("#pagesix .outpatientcontainer").removeClass("reveal");
});

//Toggle Process Addiction Glow
$("#pagefour .button.games").click(function(){
    $("#pagefour .xbox-glow").toggleClass("reveal");
});
$("#pagefour .button.games").click(function(){
    $("#pagefour .xbox").toggleClass("reveal");
});
$("#pagefour .button.games").click(function(){
    $("#pagefour .button.games").toggleClass("reveal");
});
$("#pagefour .button.internet").click(function(){
    $("#pagefour .computer-glow").toggleClass("reveal");
});
$("#pagefour .button.internet").click(function(){
    $("#pagefour .computer").toggleClass("reveal");
});
$("#pagefour .button.internet").click(function(){
    $("#pagefour .button.internet").toggleClass("reveal");
});
$("#pagefour .button.sex").click(function(){
    $("#pagefour .book-glow").toggleClass("reveal");
});
$("#pagefour .button.sex").click(function(){
    $("#pagefour .book").toggleClass("reveal");
});
$("#pagefour .button.sex").click(function(){
    $("#pagefour .button.sex").toggleClass("reveal");
});
$("#pagefour .button.eating").click(function(){
    $("#pagefour .tape-glow").toggleClass("reveal");
});
$("#pagefour .button.eating").click(function(){
    $("#pagefour .measuring-tape").toggleClass("reveal");
});
$("#pagefour .button.eating").click(function(){
    $("#pagefour .button.eating").toggleClass("reveal");
});
$("#pagefour .button.gamble").click(function(){
    $("#pagefour .gambling-glow").toggleClass("reveal");
});
$("#pagefour .button.gamble").click(function(){
    $("#pagefour .gambling").toggleClass("reveal");
});
$("#pagefour .button.gamble").click(function(){
    $("#pagefour .button.gamble").toggleClass("reveal");
});

// Pig Coin Animation
$("#pageeleven .coin").click(function(){
    $("#pageeleven .coin").toggleClass("enter");
});

$("#pageeleven .coin").click(function(){
    $("#pageeleven .moreinfo-container").toggleClass("reveal");
}); 

$("#pageeleven .close").click(function(){
    $("#pageeleven .moreinfo-container").removeClass("reveal");
});

$("#pageeleven .close").click(function(){
    $("#pageeleven .coin").removeClass("enter");
});



//Accordion
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
jQuery(document).ready(function() {
    jQuery('#pageeighteen .accordion').viewportChecker({
        classToAdd: 'rolldown', // Class to add to the elements when they are visible
        offset: -400,    
       });   
}); 

