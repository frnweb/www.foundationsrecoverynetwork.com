//Used for changing phone numbers into links only for mobile devices.
  //frn_phoneID = the id of the span where the phone number is in
  //The following adds a link to objects using the id only for mobile devices
  if(isTierIphone || isTierTablet) {
    if(document.getElementById(frn_phoneID)) {
	var frn_objcontent = document.getElementById(frn_phoneID).innerHTML;
        //Removes everything but numbers
        frn_phoneBasic = frn_phone.replace(/\D/g,'');
        //rewrites phone number but with a link this time
        document.getElementById(frn_phoneID).innerHTML = '<a href="tel:'+frn_phoneBasic+'" '+frn_phoneStyle+'>'+frn_objcontent+'</a>';
    }
  }

