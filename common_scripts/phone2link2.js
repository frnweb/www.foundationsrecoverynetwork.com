//Notes:
  //Used for changing phone numbers into links only for mobile devices.
  //Affects not just mobile: Also sets a global page variable (frn_phone) that will include the code within the object ID. Variable then can be used in live chat slideout.
  //frn_phoneID = the id of the span where the phone number is in. Variable is set in the script code after the phone number object (DIV or SPAN)
  //The following allows flexibility in case a phone number in the header isn't defined but one is in the page
  
//Makes sure the object ID is on the page
if (typeof frn_phoneID === 'undefined') var frn_objcontent = ""; //does nothing
  else var frn_objcontent = document.getElementById(frn_phoneID).innerHTML; //ideally grabs the phone number from the DIV or SPAN with matching ID var already set

if(frn_objcontent!="") {
  if (typeof frn_phone === 'undefined') { //if someone forgot the var in the header
      var frn_phone=frn_objcontent.replace(/\D/g,''); //sets the main variable with stripped innerHTML content if that variable is blank at this point (should only affect RandT.com)
      if(frn_phone.length==11) {frn_phoneFormat = /^1?([0-1])([2-9]..)([2-9]..)(....)$/; 
          frn_phone = frn_phone.replace(frn_phoneFormat,'$1 ($2) $3-$4');}
      else if(frn_phone.length==10) {frn_phoneFormat = /^1?([2-9]..)([2-9]..)(....)$/; 
          frn_phone = frn_phone.replace(frn_phoneFormat,'1 ($1) $2-$3');}
      else {frn_phoneFormat = /^1?([0-9].)([0-1])([2-9]..)([2-9]..)(....)$/;
               frn_phone = frn_phone.replace(frn_phoneFormat,'$1 $2 ($3) $4-$5');}
	  
  }
  else if(frn_phone=="") { //if it's there but blank
      frn_phone=frn_objcontent.replace(/\D/g,''); //sets the main variable with stripped innerHTML content if that variable is blank at this point (should only affect RandT.com)
      frn_phoneFormat = /^1?([2-9]..)([2-9]..)(....)$/;
      frn_phone = frn_phone.replace(frn_phoneFormat,'1 ($1) $2-$3');
  }
  
  //Removes everything but numbers
  frn_phoneBasic = frn_phone.replace(/\D/g,'');
  //alert(frn_phone); //for testing
  
  //The following adds a link to objects using the id only for mobile devices
  if(isTierIphone || isTierTablet) {
    if(document.getElementById(frn_phoneID)) {
        //rewrites the content in the SPAN or DIV object but with a link this time surrounding it
        document.getElementById(frn_phoneID).innerHTML = '<a href="tel:'+frn_phoneBasic+'" '+frn_phoneStyle+'>'+frn_objcontent+'</a>';
    }
  }
}
