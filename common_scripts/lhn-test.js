
if(typeof lhnButtonN4HO != 'undefined') lhnButtonN=lhnButtonN4HO;  //old style consideration


//The following adds custom text below the two buttons in slide out
function LHN_HelpPanel_onLoad(){
    wireLHNEvents();
    //For PPC rehab & intervention custom phone numbers. frn_phone is not set on PPC site. frn_phoneBasic could be set by phone2link.js. Either way, it works great to replace the frn_phone variable.
    if(frn_phone=="") {
		if (typeof frn_phoneBasic === 'undefined') ''; // variable is undefined so do nothing
        else if(frn_phoneBasic!="") {
          frn_phone=frn_phoneBasic;
          
          //The following formats the visual frn_phone number to a normal format: 1 (###) ###-#### 
          frn_phone = frn_phone.replace(/[^0-9]/g,'');
          frn_phoneFormat = /^1?([2-9]..)([2-9]..)(....)$/;
          frn_phone = frn_phone.replace(frn_phoneFormat,'1 ($1) $2-$3');
		}
	}
    
	//Set phone number code for mobile devices
	if(frn_phone!="" && (isTierIphone || isTierTablet)) {
      frn_phoneBasic = frn_phone.replace(/\D/g,'');
      frn_phone = '<a href="tel:'+frn_phoneBasic+'">'+frn_phone+'</a>';
    }
	
	//Set custom text below phone number
    if(frn_customText.trim(frn_customText)=="") frn_customText='<span><p class="flyout-phone">' + frn_phone + '</p><p class="flyout-bottom">Confidential & Private</p></span>'; 
    
    //Create DIV with the content
    frn_addphone = document.createElement('div');
    frn_addphone.class = 'lhn_live_phone';
    frn_addphone.style = 'cursor:pointer;';
    frn_addphone.innerHTML = frn_customText;
    
    //Find the DIV with the class that content needs to be added within
    var classObjs = document.getElementsByTagName('*'), i; //get all tag names in page
    for (i in classObjs) {
        if(typeof classObjs[i].className === 'undefined') "";
		else {
    		if((classObjs[i].className).indexOf('lhn_btn_container') > -1) {
              //alert('lhn_btn_container was found!');
    		  //if it doesn't find "a div with lhn_btn_container, then it will error out;
              classObjs[i].appendChild(frn_addphone);
        	}
		}
    }
  }
  
//Tracks clicks/events on LiveHelpNow assets in Analytics
function wireLHNEvents() {
 window.lhn("#lhn_help_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out');
   });
   window.lhn("#lhn_helppanel #lhn_help_exit").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Close Button');
   });
   window.lhn("#lhn_helppanel .lhn_options_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: More Options');
   });
   window.lhn("#lhn_helppanel #lhn_search_box").blur(function(){
    if(window.lhn(this).val() != ""){
     lhnCurVal = window.lhn(this).val();
     if(typeof(lhnOldVal) == 'undefined' || lhnOldVal != lhnCurVal){
      lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Search', encodeURIComponent(window.lhn(this).val()));
      lhnOldVal = lhnCurVal;
     }
    }
   });
   window.lhn("#lhnChatButton #lhnchatimg").click(function(){
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Chat Clicks');
   });
   window.lhn("#lhnEmailButton").click(function(){
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Email Clicks');
   });
   if(isTierIphone || isTierTablet) {
     window.lhn("#lhn_helppanel .lhn_live_phone").click(function(){
      lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Phone');
     });
	 window.lhn("#frn_phoneNumber").click(function(){
      lhnPushAnalytics('Phone Numbers', 'Clicks on Phone (generic)');
     }); 
   }
   window.lhn("#lhn_helppanel .lhn_live_chat_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Chat Button');
   });
   window.lhn("#lhn_helppanel .lhn_ticket_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Email Button');
   });
   window.lhn("#lhn_helppanel .lhn_callback_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Callback Button');
   });
  }
 
  function lhnPushAnalytics(category, event, label){
   if(typeof(_gaq) != 'undefined'){
    _gaq.push(['_trackEvent', category, event, label]);
   }
  }
  

