
        var lhnVersion = 5.3;
        var lhnAccountN = 14160;
        var lhnButtonN = -1;
        var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
        var lhnInviteEnabled = 1;
        var lhnInviteChime = 0;
        var lhnWindowN = 14960;
        var lhnDepartmentN = 0;
        var lhnCustomInvitation = '';
        var lhnCustom1 = '';
        var lhnCustom2 = '';
        var lhnCustom3 = '';
        var lhnTrackingEnabled = 't';
        var lhnTheme = "green";
        var lhnHPPanel = true;
        var lhnHPKnowledgeBase = false;
        var lhnHPMoreOptions = true;
        var lhnHPChatButton = true;
        var lhnHPTicketButton = true;
        var lhnHPCallbackButton = false;
        var lhnLO_helpPanel_knowledgeBase_find_answers = "Find answers";
        var lhnLO_helpPanel_knowledgeBase_please_search = "Please search our knowledge base for answers or click [More options]";
        var lhnLO_helpPanel_typeahead_noResults_message = "No results found for";
        var lhnLO_helpPanel_typeahead_result_views = "views";
        var loadLHNFile = function (url, type) { if (type == "js") { var file = document.createElement('script'); file.setAttribute("type", "text/javascript"); file.setAttribute("src", url); } else if (type = "css") { var file = document.createElement('link'); file.setAttribute("rel", "stylesheet"); file.setAttribute("type", "text/css"); file.setAttribute("href", url); } if (typeof file != "undefined") { document.getElementsByTagName('head')[0].appendChild(file) } }
        var loadLHNFiles = function () {
			if (lhnHPChatButton == true && typeof lhnInstalled == "undefined") {
				if (!document.getElementById('lhnChatButton')) {
						if (document.body) {
								var lhnBTNdiv = document.createElement('div');
								lhnBTNdiv.id = 'lhnChatButton';
								if (document.body.lastChild) { document.body.insertBefore(lhnBTNdiv, document.body.lastChild); } else { document.body.appendChild(lhnBTNdiv); }
						}
						else {
								document.write('<div id="lhnChatButton"></div>');
						}
				}
				loadLHNFile(lhnJsHost + 'www.livehelpnow.net/lhn/scripts/livehelpnow.aspx?lhnid=' + lhnAccountN + '&iv=' + lhnInviteEnabled + '&d=' + lhnDepartmentN + '&ver=' + lhnVersion + '&rnd=' + Math.random(), "js");
				window.setTimeout("if (typeof bLHNOnline != 'undefined' && bLHNOnline == 0 && lhnHPPanel == true){document.getElementById('lhn_live_chat_btn').style.display='none';}", 2000);
			}
            loadLHNFile(lhnJsHost + "www.livehelpnow.net/lhn/js/build/helppanel.ashx", "js");
            loadLHNFile(lhnJsHost + "www.foundationsrecoverynetwork.com/support/style.css", "css"); //You may use your CSS here, these classes must be used: http://foundationsrecoverynetwork.com/support/style.css
		}
        if (window.addEventListener) {
                window.addEventListener('load', function () {
                        loadLHNFiles();
                }, false);
        } else if (window.attachEvent) {
                window.attachEvent('onload', function () {
                        loadLHNFiles();
                });
        }


//Tracks clicks/events on LiveHelpNow assets in Analytics

function LHN_HelpPanel_onLoad(){
   wireLHNEvents();
   //The following adds custom text below the two buttons in slide out
   if(frn_customText=="") jQuery('.lhn_btn_container').append('<div class="lhn_live_phone" style="cursor:pointer;"><span><p class="flyout-phone">' + frn_phone + '</p><p class="flyout-bottom">Confidential & Private</p></span></div>');
   else jQuery('.lhn_btn_container').append( frn_customText );
  }
function wireLHNEvents() {
 window.lhn("#lhn_help_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out');
   });
   window.lhn("#lhn_helppanel #lhn_help_exit").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Close Button');
   });
   window.lhn("#lhn_helppanel .lhn_options_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: More Options');
   });
   window.lhn("#lhn_helppanel #lhn_search_box").blur(function(){
    if(window.lhn(this).val() != ""){
     lhnCurVal = window.lhn(this).val();
     if(typeof(lhnOldVal) == 'undefined' || lhnOldVal != lhnCurVal){
      lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Search', encodeURIComponent(window.lhn(this).val()));
      lhnOldVal = lhnCurVal;
     }
    }
   });
   window.lhn("#lhnChatButton #lhnchatimg").click(function(){
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Chat Clicks');
   });
   window.lhn("#lhnEmailButton").click(function(){
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Email Clicks');
   });
   if(isTierIphone || isTierTablet) {
     window.lhn("#lhn_helppanel .lhn_live_phone").click(function(){
      lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Phone');
     });
	 window.lhn("#frn_phoneNumber").click(function(){
      lhnPushAnalytics('Phone Numbers', 'Clicks on Phone (generic)');
     }); 
   }
   window.lhn("#lhn_helppanel .lhn_live_chat_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Chat Button');
   });
   window.lhn("#lhn_helppanel .lhn_ticket_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Email Button');
   });
   window.lhn("#lhn_helppanel .lhn_callback_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Callback Button');
   });
  }
 
  function lhnPushAnalytics(category, event, label){
   if(typeof(_gaq) != 'undefined'){
    _gaq.push(['_trackEvent', category, event, label]);
   }
  }
