
//default variables if not previously defined
var lhnVersion = 5.3;
if(typeof lhnAccountN === 'undefined') var lhnAccountN = "14160";
if(typeof lhnWindowN === 'undefined') var lhnWindowN = 14960; //0=default, can customize window per site
if(typeof lhnDepartmentN === 'undefined') var lhnDepartmentN = 0;  //default=0
if(typeof lhnHPCallbackButton === 'undefined') var lhnHPCallbackButton = false;
if(typeof lhnHPKnowledgeBase === 'undefined') var lhnHPKnowledgeBase = false;
if(typeof lhnHPMoreOptions === 'undefined') var lhnHPMoreOptions = false;
if(typeof lhnInviteEnabled === 'undefined') var lhnInviteEnabled = 1;
if(typeof lhnInviteChime === 'undefined') var lhnInviteChime = 0;
if(typeof lhnCustom1 === 'undefined') var lhnCustom1 = encodeURIComponent(document.domain);
if(typeof lhnCustom2 === 'undefined') var lhnCustom2 = encodeURIComponent(getElementsByTagName("TITLE")[0].innerHTML);
if(typeof lhnCustom3 === 'undefined') var lhnCustom3 = encodeURIComponent(window.location.href);
if(typeof lhnTheme === 'undefined') var lhnTheme = "green";  //default=green
if(typeof lhnCustomInvitation === 'undefined') var lhnCustomInvitation = '';
if(typeof lhnJsHost === 'undefined') var lhnJsHost = (("https:" == document.location.protocol) ? "https://" : "http://"); 
if(typeof lhnCss === 'undefined') {
	var lhnCss = lhnJsHost + "s3.amazonaws.com/spanning/scripts/lhn_frn_style.css";
}
if(typeof lhnButtonN4HO === 'undefined') var lhnButtonN = -1; //default -1
	else if(lhnButtonN4HO=="") var lhnButtonN = -1;
	else var lhnButtonN=lhnButtonN4HO;
var lhnTrackingEnabled = 't';
var lhnHPPanel = true;
var lhnHPChatButton = true;
var lhnHPTicketButton = true;
var lhnLO_helpPanel_knowledgeBase_find_answers = "Find answers";
var lhnLO_helpPanel_knowledgeBase_please_search = "Please search our knowledge base for answers or click [More options]";
var lhnLO_helpPanel_typeahead_noResults_message = "No results found for";
var lhnLO_helpPanel_typeahead_result_views = "views";




//The following adds custom text below the two buttons in slide out
function LHN_HelpPanel_onLoad(){
    wireLHNEvents();
    
	//For PPC rehab & intervention custom phone numbers. frn_phone is not set on PPC site. frn_phoneBasic could be set by phone2link.js. Either way, it works great to replace the frn_phone variable.
    if(frn_phone=="") {
		if(frn_phoneBasic!="") {
          frn_phone=frn_phoneBasic;
          
          //The following formats the visual frn_phone number to a normal format: 1 (###) ###-#### 
          frn_phone = frn_phone.replace(/[^0-9]/g,'');
          frn_phoneFormat = /^1?([2-9]..)([2-9]..)(....)$/;
          frn_phone = frn_phone.replace(frn_phoneFormat,'1 ($1) $2-$3');
		}
	}
    
	//Set phone number code for mobile devices
	if(frn_phone!="" && (isTierIphone || isTierTablet)) {
      frn_phoneBasic = frn_phone.replace(/\D/g,'');
      frn_phone = '<a href="tel:'+frn_phoneBasic+'">'+frn_phone+'</a>';
    }
	
	//Set custom text below phone number
    if(frn_customText.trim(frn_customText)=="") frn_customText='<span><p class="flyout-phone">' + frn_phone + '</p><p class="flyout-bottom">Confidential & Private</p></span>'; 
    
    //Create DIV with the content
    frn_addphone = document.createElement('div');
    frn_addphone.class = 'lhn_live_phone';
    frn_addphone.style = 'cursor:pointer;';
    frn_addphone.innerHTML = frn_customText;
    
    //Find the DIV with the class that content needs to be added within
    var classObjs = document.getElementsByTagName('*'), i; //get all tag names in page
    for (i in classObjs) {
        if(typeof classObjs[i].className === 'undefined') "";
		else {
    		if((classObjs[i].className).indexOf('lhn_btn_container') > -1) {
              //alert('lhn_btn_container was found!');
    		  //if it doesn't find "a div with lhn_btn_container, then it will error out;
              classObjs[i].appendChild(frn_addphone);
        	}
		}
    }
  }
  
//Tracks clicks/events on LiveHelpNow assets in Analytics
function wireLHNEvents() {
   //Helpout elements
   
   window.lhn("#lhn_help_btn").click(function(){  //initial help out tab button
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out');
   });
   window.lhn("#lhn_helppanel .lhn_live_chat_btn").click(function(){ //chat button
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Chat Button');
   });
   window.lhn("#lhn_helppanel .lhn_ticket_btn").click(function(){  //email button
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Email Button');
   });
   if(isTierIphone || isTierTablet) {
     window.lhn("#lhn_helppanel .flyout-phone").click(function(){  //Phone number clicks
      lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Phone Number');
     });
   }
   //On-page buttons
   window.lhn("#lhnChatButton #lhnchatimg").click(function(){  //Chat button
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Chat Clicks');
   });
   window.lhn("#lhnEmailButton").click(function(){  //Email button
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Email Clicks');
   });
   
   //Less used Helpout elements
   window.lhn("#lhn_helppanel #lhn_help_exit").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Close Button');
   });
   window.lhn("#lhn_helppanel .lhn_options_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: More Options');
   });
   window.lhn("#lhn_helppanel #lhn_search_box").blur(function(){
    if(window.lhn(this).val() != ""){
     lhnCurVal = window.lhn(this).val();
     if(typeof(lhnOldVal) == 'undefined' || lhnOldVal != lhnCurVal){
      lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Search', encodeURIComponent(window.lhn(this).val()));
      lhnOldVal = lhnCurVal;
     }
    }
   });
   window.lhn("#lhn_helppanel .lhn_callback_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Callback Button');
   });
   }
 
 GA_count=1;
  function lhnPushAnalytics(category, event, label){
   if(typeof(_gaq) != 'undefined'){
    GA_count=GA_count+1;
	_gaq.push(['_trackEvent', category, event, label]);
	alert("Tracked '" +GA_count+ "' times. Category: " +category+ "; Event: " +event );
   }
  }
  

