//Notes:
  //Used for changing phone numbers into links only for mobile devices.
  //Affects not just mobile: Also sets a global page variable (frn_phone) that will include the code within the object ID. Variable then can be used in live chat slideout.
  //frn_phoneID = the id of the span where the phone number is in. Variable is set in the script code after the phone number object (DIV or SPAN)
  //The following allows flexibility in case a phone number in the header isn't defined but one is in the page
  
//Makes sure the object ID is on the page
if (typeof frn_phoneID === 'undefined') var frn_objcontent = ""; //does nothing
  else var frn_objcontent = document.getElementById(frn_phoneID).innerHTML; //ideally grabs the phone number from the DIV or SPAN with matching ID var already set

  //alert(frn_phone + "; " + frn_objcontent);

if(frn_objcontent.trim!="") {

  /*
  //Removed since this script will only get the phone number from between the SPANs
  if (typeof frn_phone === 'undefined') { //if someone forgot the var in the header
     
	
	 
	 frn_phone=frn_objcontent.replace(/\D/g,''); //sets the main variable with stripped innerHTML content if that variable is blank at this point (should only affect RandT.com)
      if(frn_phone!="") {
		if(frn_phone.length==11) {frn_phoneFormat = /^1?([0-1])([0-9]..)([0-9]..)([0-9]...)$/; 
		  frn_phone = frn_phone.replace(frn_phoneFormat,'$1 ($2) $3-$4');}
		else if(frn_phone.length==10) {frn_phoneFormat = /^1?([0-9]..)([0-9]..)([0-9]...)$/; 
		  frn_phone = frn_phone.replace(frn_phoneFormat,'1 ($1) $2-$3');}
		else if(frn_phone.length>11){frn_phoneFormat = /^1?([0-9].)([0-1])([0-9]..)([0-9]..)([0-9]...)$/;
		  frn_phone = frn_phone.replace(frn_phoneFormat,'$1 $2 ($3) $4-$5');}
		}
  }
  */
  
  //if(frn_phone=="" || frn_phone==null) { //if it's there but blank
 	
	//The following:
		//defines frn_phone with innerHTML
		//looks for the following phone number patterns in the innerHTML (frn_objcontent)
	
	//International for R&T site -- need to check first since U.S. is always last part anyway
	// 001 (###) ###-####  18chars
	var pattern = /[0-9]{3}\s\(+[A-Za-z0-9]{3}\)+.{9}/;
	frn_phoneBasic = pattern.exec(frn_objcontent);
	
	if(frn_phoneBasic==null) {
		// 01 1 (###) ###-####  19chars
		var pattern = /[0-9]{2}\s[0-1]\s\(+[A-Za-z0-9]{3}\)+.{9}/; 
		frn_phoneBasic = String(pattern.exec(frn_objcontent));
		frn_phoneBasic_len = frn_phoneBasic.length;
	}
	if(frn_phoneBasic==null) {
		// 1 (###) ###-####  16chars
		var pattern = /1\s\(+[A-Za-z0-9]{3}\)+.{9}/;
		frn_phoneBasic = pattern.exec(frn_objcontent);
	}
	if(frn_phoneBasic==null) {
		// (###) ###-####
		var pattern = /\(+[A-Za-z0-9]{3}\)+.{9}/; 
		frn_phoneBasic = pattern.exec(frn_objcontent);
	}
	if(frn_phoneBasic==null) {
		// ###-###-####
		var pattern = /[A-Za-z0-9]{3}-[A-Za-z0-9]{3}-[A-Za-z0-9]{4}/;
		frn_phoneBasic = pattern.exec(frn_objcontent);
	}
	if(frn_phoneBasic==null) {
		// ###-####
		var pattern = /[A-Za-z0-9]{3}-[A-Za-z0-9]{4}/;
		frn_phoneBasic = pattern.exec(frn_objcontent);
	}
  
	if(frn_phoneBasic!="") frn_phoneBasic="";
	//alert(frn_phoneBasic + "; " + frn_objcontent);
  
      //frn_phone2=frn_objcontent.replace(/\D/g,''); //sets the main variable with stripped innerHTML content if that variable is blank at this point (should only affect RandT.com)
      //frn_phoneFormat = /^1?([2-9]..)([2-9]..)(....)$/;
      //frn_phone2 = frn_phone2.replace(frn_phoneFormat,'1 ($1) $2-$3');
  //}
  
  //Removes everything but numbers to set format for mobile phones
	if(frn_phoneBasic!="") {
		frn_phoneBasic = String(frn_phoneBasic).replace(/\D+/g,'');
		//alert(frn_phoneBasic);
		if(frn_phoneBasic!="") {
			//Parenthesis here just means to group the number segments
			if(frn_phoneBasic.length==11) {frn_phoneFormat = /^([0-1])([0-9]..)([0-9]..)([0-9]...)$/; 
			  frn_phoneBasic = frn_phoneBasic.replace(frn_phoneFormat,'$1-$2-$3-$4');} // assuming a 1 or 0 starts the number.
			else if(frn_phoneBasic.length==10) {frn_phoneFormat = /^([0-9]..)([0-9]..)([0-9]...)$/; 
			  frn_phoneBasic = frn_phoneBasic.replace(frn_phoneFormat,'1-$1-$2-$3');}  // assumes the number doesn't have a 1 at the front
			else if(frn_phoneBasic.length==12){frn_phoneFormat = /^([0-9]..)([0-9]..)([0-9]..)([0-9]...)$/;
			  frn_phoneBasic = frn_phoneBasic.replace(frn_phoneFormat,'$1-$2-$3-$4');} //International. Can start with 1 or 2 digits. Next with normal U.S. number
			else if(frn_phoneBasic.length>12){
			  if(frn_phoneBasic_len==19) {
				  frn_phoneFormat = /^([0-9].)([0-9])([0-9]..)([0-9]..)([0-9]...)$/;
				  frn_phoneBasic = frn_phoneBasic.replace(frn_phoneFormat,'$1 $2 ($3) $3-$4');
			  } //International. Can start with 1 to 3 digits. Next with normal U.S. number
			  else {
				  frn_phoneFormat = /^([0-9]..)([0-9]..)([0-9]..)([0-9]...)$/;
				  frn_phoneBasic = frn_phoneBasic.replace(frn_phoneFormat,'$1 ($3) $3-$4');
			  } //International. Can start with 1 to 3 digits. Next with normal U.S. number
			}
		}
	}
	//alert(frn_phoneBasic);
  
  //The following adds a link to objects using the id only for mobile devices
  if((isTierIphone || isTierTablet) && (frn_phoneBasic!="" || frn_phoneBasic!=null)) {
    if(document.getElementById(frn_phoneID)) {
        //rewrites the content in the SPAN or DIV object but with a link this time surrounding it
        document.getElementById(frn_phoneID).innerHTML = '<a href="tel:'+frn_phoneBasic+'" '+frn_phoneStyle+'>'+frn_objcontent+'</a>';
    }
  }
}
frn_phoneBasic="";
