
<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="robots" content="noindex">
  <title>Foundations Recovery Network</title>
  <link rel="stylesheet" href="https://use.typekit.net/gaq1zcr.css">
  <link rel="stylesheet" href="assets/css/app.css">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="assets/img/favicons/manifest.json">
  <link rel="mask-icon" href="assets/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">
  <meta name="robots" content="noindex, nofollow">

  <!-- Start Visual Website Optimizer Synchronous Code -->
  <script type='text/javascript'>
    var _vis_opt_account_id = 341821;
    var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
    document.write('<s' + 'cript src="' + _vis_opt_protocol +
    'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='
    +encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');
    </script>

    <script type='text/javascript'>
    if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol +
    'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }
    /* if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above */
    </script>

    <script type='text/javascript'>
    if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
            _vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });
    }
  </script>
  <!-- End Visual Website Optimizer Synchronous Code -->  

  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '402262266824056');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=402262266824056&ev=PageView&noscript=1"
  /></noscript>
  <!-- End Facebook Pixel Code -->
</head>

<body>

      <div id="nav" class="">            
          <div class="title-bar">
              <a href="#" class="title-bar-left">
                  <img src="assets/img/frn_horizontal.svg" class="site-logo" alt="" />
              </a>
          </div>
      </div>
  
  <div class="content">
  
  <div id="acknowledgement" class="module wysiwyg ">
  	<div class="accordion-container"> 
  	<h2>Maybe you're worried about...</h2>
  		<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
  		  <li class="accordion-item" data-accordion-item>
  		    <a href="#" class="accordion-title">What if I don’t think my problem is that bad?</a>
  
  		    <div class="accordion-content" data-tab-content>
  		      Whether or not you label yourself an addict, most people who reach out to us are struggling with cravings or compulsions to use; disruptions in their work, school or family lives; problems with judgment or risky behaviors, mood problems or even legal problems. We aren’t interested in labeling you; we’re here to help you get your life back on track. Most of the time when you think you or your loved one might need treatment, you do.
  		    </div>
  		  </li>
  		  <li class="accordion-item" data-accordion-item>
  		    <a href="#" class="accordion-title">How will my treatment affect my family?</a>
  
  		    <div class="accordion-content" data-tab-content>
  		      Involving the whole family in the recovery process can improve the happiness and health of the household. We want therapy sessions to be life-changing for everyone involved, so we focus on treatment education and on bringing everyone together to transform negative dynamics. Our evidence-based techniques work to heal each member of the family.
  		    </div>
  		  </li>
  		  <li class="accordion-item" data-accordion-item>
  		    <a href="#" class="accordion-title">Will I lose my job if I get the help I need?</a>
  
  		    <div class="accordion-content" data-tab-content>
  		      The Family and Medical Leave Act (FMLA) legally assures that employees have the opportunity to take time off on unpaid leave for serious family needs and medical reasons, including treatment for addiction. Covered employees can take job-protected leave for a total of 12 work weeks in any 12-month time period for treatment and recovery for themselves or close family members. If this is a concern of yours we can help guide you through the entire process and answer any questions you may have.
  		    </div>
  		  </li>
  		  <li class="accordion-item" data-accordion-item>
  		    <a href="#" class="accordion-title">How will I pay for treatment?</a>
  
  		    <div class="accordion-content" data-tab-content>
  		      Navigating the maze of health insurance terminology and the specifics of coverage can be stressful and the last thing you want to worry about when you or your loved one is dealing with a substance abuse problem. We accept most private insurance plans and our admissions center has a special team focused solely on working with insurance companies to verify benefits and sort out coverage. We can help you explore what options your policy covers and will work hard to help you get the treatment you’re entitled to.
  		    </div>
  		  </li>
  		</ul>
  	</div>
  
  	<div class="inner">
  
  
  
  		<h4 class="button-cta">
  
  		</h4>
  		
  		<a class="button " href="tel:615-371-4163" onClick="fbq('track', 'Lead', {
      content_name: 'FRN Facebook Ad Landing Page', 
      content_category: 'Landing Pages',
      content_type: 'Phone Clicks'
    });       " data-ga-event data-ga-category="Phone Numbers" data-ga-action="Phone Clicks" data-ga-label="Phone Clicks First">615-371-4163</a>
  	</div>
  </div><div id="treatment" class="module wysiwyg grey">
  	<div class="inner">
  
  
  
  		<ul>
  			<li>Nationally recognized treatment program</li>
  			<li>Evidenced-based methods with proven results</li>
  			<li>Dual diagnosis treatment for mental health and substance abuse</li>
  			<li>Wide variety of therapy options based on your interests and needs</li>
  			<li>Ongoing support after you leave with our Life Challenge program</li>
  			<li>First-rate staff comprised of highly qualified and compassionate professionals</li>
  		</ul>
  	</div>
  </div><div id="facility">
  	<div class="module wysiwyg">
  		<div class="inner">
  
  
  
  
  		</div>
  	</div>
  	<div class="module gallery">
  	  <div class="inner">
  	        <div id="locations-slider" class="locations__slider">
  	              <img alt="Black Bear Cabin" src="assets/img/locations-gallery/black-bear-lodge.jpg"/>
  	              <img alt="Black Bear Lodge Indoor" src="assets/img/locations-gallery/black-bear-lodge-indoor.jpg"/>
  	              <img alt="Michaels House Womens Campus" src="assets/img/locations-gallery/michaels-house-womens.jpg"/>
  	              <img alt="Skywood Facility" src="assets/img/locations-gallery/skywood-facility-night.jpg"/>
  	              <img alt="The Oaks" src="assets/img/locations-gallery/theoaks-outdoor.jpg"/>
  	              <img alt="The Canyon Outdoor" src="assets/img/locations-gallery/the-canyon.jpg"/>
  	        </div>
  	  </div>
  	</div>
  	<div class="module wysiwyg">
  		<div class="inner">
  
  
  
  			<h4 class="button-cta">
  
  			</h4>
  
  			<a class="button " href="tel:615-371-4163" onClick="fbq('track', 'Lead', {
      content_name: 'FRN Facebook Ad Landing Page', 
      content_category: 'Landing Pages',
      content_type: 'Phone Clicks'
    });       " data-ga-event data-ga-category="Phone Numbers" data-ga-action="Phone Clicks" data-ga-label="Phone Clicks Facility">615-371-4163</a>
  		</div>
  </div>
  
  <div id="what-to-expect" class="module wysiwyg grey">
  	<div class="inner">
  
  
  
  	</div>
  </div>
  
  <div class="module wysiwyg">
  	<div class="inner">
  
  
  
  	</div>
  </div>
  
  <div class="module wysiwyg grey">
  	<div class="inner">
  
  
  
  	</div>
  </div>
  
  
  <div class="footer">
  	<div class="inner">
  		<p><a href="https://www.foundationsrecoverynetwork.com/wp-content/plugins/frn_plugins/privacy-policy.pdf">Privacy Policy</a></p>
  	</div>
  </div>
  <a id="live-chat-sticky" class="button" data-chat data-ga-event data-ga-category="Live Chat" data-ga-action="Chat Button" data-ga-label="Live Chat Popover">Not Ready to Call?</a></div>

    <script src="assets/js/app.js"></script>
</body>

</html>