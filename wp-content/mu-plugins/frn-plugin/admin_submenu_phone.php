<?php

include("admin_submenu_phone_infinity.php");

function frn_reg_ph_var() {
	//registers the variable for the database
	//using a new master data holder to avoid overwriting variable on main settings page (there is a way to avoid it, but unnecessary)
	register_setting( 'frn_phones', 'phones', array('','','','frn_save_phone_settings','','')  );
}


// This PHP file adds the phone number management options SUB menu page to the main FRN Settings Menu
add_action('admin_menu', 'frn_plugin_phone_admin_layout_1');
function frn_plugin_phone_admin_layout_1() {
	//$phones=frn_phone_old_to_new();

	//changes the submenu page for the Main Settings - needs to be  on the first subpage function
	frn_rename_settings();
	add_submenu_page( 'frn_features',"FRN Phone Number Managment", "Phone Numbers", 'manage_options', 'frn_phones', 'frn_subpage_layout');

	add_settings_section('site_phone_section', 'Main Phone Number Settings', '', 'frn_phones');
		//if(!isset($phones['tracking'])) $phones['tracking']="";
		if(function_exists('frn_admin_infinity_main')) {
			add_settings_field('frn_phone_main_pool', "Primary <span style='white-space:nowrap;'>Pool <a href='javascript:showhide(\"frn_plugin_main_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_admin_infinity_main', 'frn_phones', 'site_phone_section');
		}
		$inf_main = frn_infinity_activate(); //array(activate type,class,data,class_text,$data_text)
		if(!isset($inf_main['activate'])) $inf_main['activate']="no"; //if not set, it likely means that this has run prior to infinity being activated (i.e. unlikely)
		$help="<a href='javascript:showhide(\"main_number_help\")' ><img src='".$GLOBALS['help_image']."' /></a>";
		if($inf_main['activate']!=="no") {
			$field_name = "<span style='white-space:nowrap;'>Fallback ".$help."</span>";
		}
		else $field_name = "Main <span style='white-space:nowrap;'>Number ".$help."</span>";
		add_settings_field('frn_plugin_phone', $field_name, 'frn_phones_field', 'frn_phones', 'site_phone_section');
		//echo "phones['tracking']: " .$phones['tracking'];
		add_settings_field('frn_phone_mobile', "<span style='white-space:nowrap;'>Linked <a href='javascript:showhide(\"frn_plugin_scantype_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_phones_mobile', 'frn_phones', 'site_phone_section');
		add_settings_field('frn_phone_auto', "Scan for <span style='white-space:nowrap;'>Numbers <a href='javascript:showhide(\"frn_plugin_autoscan_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_phones_auto', 'frn_phones', 'site_phone_section');
		//add_settings_field('frn_plugin_sc_info', "Shortcode <span style='white-space:nowrap;'>Examples <a href='javascript:showhide(\"frn_plugin_sc_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_phones_sc_info', 'frn_phones', 'site_phone_section');
		//add_settings_field( $id, $title, $callback, $page, $section, $args );
	
	//DialogTech+Infinity
	add_settings_section('tracking_section', 'Phone Tracking', '', 'frn_tracking'); //options: $id, $title, $callback, $page
		add_settings_field('tracking_options', "Tracking <span style='white-space:nowrap;'>Service <a href='javascript:showhide(\"frn_plugin_tracking_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_tracking_options', 'frn_tracking', 'tracking_section');
		add_settings_field('tracking_services', "Service  <span style='white-space:nowrap;'>Options <a href='javascript:showhide(\"frn_plugin_infinity_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_tracking_services', 'frn_tracking', 'tracking_section');
		if(function_exists('frn_infinity_analytic_options')) {
			frn_infinity_analytic_options(); //adds infinity options
		} 

	add_settings_section('site_phone_report', 'Phone Report', '', 'frn_phone_report'); //options: $id, $title, $callback, $page
		add_settings_field('phone_sc_posts', "Review Posts", 'frn_phone_report', 'frn_phone_report', 'site_phone_report');
		add_settings_field('phone_sc_php', "Review PHP Files", 'frn_phone_search_php', 'frn_phone_report', 'site_phone_report');
		
	add_action('admin_init', 'frn_reg_ph_var');
}

function frn_subpage_layout() {
	?>
	<div class="wrap">
		<h2>FRN Plugin: New Phone Number Management System</h2>
		<div class="frn_wrap" >
			<div class="intro_text" style="margin-top:50px;">
				<p>Click the help icons <img src="<?=$GLOBALS['help_image'];?>" /> to know the options for each section.</p>
				<br />
				<hr />
				<br />
			</div>
			<form action="options.php" method="post" class="frn_styles" id="frn_settings">
				<?php settings_fields( 'frn_phones' ); ?>
				<?php do_settings_sections( 'frn_phones' ); ?>
				<table class="form-table">
					<tr>
						<th></th>
						<td>
							<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
						</td>
					</tr>
				</table>
				<div id="tracking_services"><?php do_settings_sections( 'frn_tracking' ); ?></div>
				<table class="form-table">
					<tr>
						<th></th>
						<td>
							<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
						</td>
					</tr>
				</table>
				<?php do_settings_sections( 'frn_phone_report' ); ?>
				<table class="form-table">
					<tr>
						<th></th>
						<td>
							<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
						</td>
					</tr>
				</table>
				</div>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<?php 
}


/////
// Fallback/Backup Number

// This is the original main site number
// This serves as a backup if the Infinity system fails or turns off or if DialogTech is activated.
function frn_phones_field() {
	$phones=frn_phone_old_to_new();

	//Due to the input name, once saved, the old "site_phone_number" will be erased and all settings will switch to the new "phones" primary array parent var.
	?>
	<input id="frn_plugin_phone" name="phones[site_phone]" size="37" type="text" value="<?=(isset($phones['site_phone']) ? $phones['site_phone'] : null);?>" />
	<div id="main_number_help" class="frn_help_boxes">
		<ul class="frn_level_1" >
	<?php 
	global $inf_main;
	if(!isset($inf_main['activate'])) $inf_main['activate']="no";
	if($inf_main['activate']!="no") : 
	?>
			<li>When Infinity pools are active, this number becomes a fallback in case the pool system fails in some way.</li>
			<li>When DialogTech is active, this becomes the main number used for all facility shortcodes unless a number attribute is provided.</li>
			<li>To stop pools from being activated, select that option in the Infinity settings below.</li>
	<?php else : ?>
			<li>The Infinity system is deactivated.</li>
			<li>This is the main number all facility shortcodes use unless the "number" attribute is provided.</li>
	<?php endif; ?>
		</ul>
	</div>
	<small><a href="javascript:showhide('old_new_settings');">[Old/New Values]</a></small>
	<div id="old_new_settings" style="display:none;">
		<br />
		<?php
			//for testing:
			$old_phone = get_option('site_phone_number');
			$new_phones = get_option('phones');
			echo "<b>old \"site_phone_number\": </b>";
			print_r($old_phone);
			echo "<br /> <br /><b>new \"phones\": </b>";
			print_r($new_phones);
			echo "<br /> <br />";
		?>
	</div>
	<?php
}

function frn_phones_sc_info() { 
	$frn_shortcode='[frn_phone number="" action="Phone Clicks in ##page location##"]';
	$php_shortcode = '&lt;?php echo do_shortcode(\''.$frn_shortcode.'\'); ?&gt;';
	$frn_shortcode2='[frn_phone action="Phone Clicks in ##page location##" id="" class="" text="" desktop="" desktop_url="" image_url="" alt="" title="" css_style="" ]';
	$php_shortcode2 = '&lt;?php echo do_shortcode(\''.$frn_shortcode2.'\'); ?&gt;';
	$phones=frn_phone_old_to_new();
	?>
	<br />
	<div id="sc_info" style="display:none;">
		<?php 
			if($phones['tracking']=="infinity" || $phones['tracking']=="dt-ads" && function_exists('frn_phones_sc_info')) : ?>
				<p>
					Facility phone number shortcodes listed in the pools table below can easily be selected from a toolbar dropdown when editing content. 
					Plus, the shortcode attributes provided as an example for the frn_number shortcode will also apply to every facility shortcode listed in the table of pools -- whether or not Infinity is activated.
				</p>
			<?php endif;
		?>
		<div class="frn_options_table">
			<table class="frn_options_table"><tr>
				<td valign="top">Shortcode for Text: </td>
				<td valign="top"><b><span class="frn_shortcode_sel"><?=$frn_shortcode;?></span></b> <font size="1">(remove <b>"action"</b> if adding to content; add <b>number=""</b> you want a different number than the default)</font></td>
			</tr><tr>
				<td valign="top">PHP: </td>
				<td valign="top"><b><span class="frn_shortcode_sel"><?=$php_shortcode;?></span></b> <font size="1">(most common for use in header.php)</font></td>
			</tr><tr>
					<td valign="top" style="padding-top:5px;border-top:1px solid gray;">Shortcode for Images: </td>
					<td valign="top" style="padding-top:5px;border-top:1px solid gray;"><b><span class="frn_shortcode_sel"><?=$frn_shortcode2;?></span></b></td>
			</tr><tr>
					<td valign="top">Image PHP: </td>
					<td valign="top"><b><span class="frn_shortcode_sel"><?=$php_shortcode2;?></span></b></td>
			</tr></table>
		</div>
		<small>See FRN HELP tab at top-right for more customizing and tracking options</small>
	</div>
	<div id="frn_plugin_sc_help" class="frn_help_boxes">
		<li><b>Global Var for PHP: </b>$frn_main_number. It stores the main number selected based on Infinity destination numbers and typical processing priorities.</li>
		<li><strong>Number Custom:  </strong>Add <b>number=""</b> to the shortcode if you want to use a number other than what's defined in the settings above. It'll then apply the normal linking for mobile devices and Analytics tracking.</li>
		<li><strong>Formatting:  </strong>Do not use letters in phone number or they will be stripped out in the link. The number format will display on the page just like you enter it here but will be formatted differently in the link itself (a friendly version for smartphones).</li>
		<li><strong>Styling:     </strong>You can use IDs by inserting id="" and CLASSes by adding class="".</li>
		<li><strong>Text:        </strong>You can link any text to a phone number call option. Just add the text="" variable to the shortcode and whatever you have inside it will be linked instead. Use mobile_text or desktop_text to be specific for device type.</li>
		<li><strong>Remove:     </strong>If you want to remove the item for a particular device, just use "remove" for the text value (e.g. desktop_text="remove"). It will completely remove the code and nothing will display.</li>
		<li><strong>No Text, Only CSS: </strong>If you want to solely rely on CSS, such as utilizing only an icon. Use text="empty" and no text will be inserted--only the A HREF code. On mobile, the link would show. On Desktop, only the SPAN tags would show as well as any ID or CSS you included. <b>IMPORTANT:</b> Keep in mind, that IDs and CLASSes are used in the link on mobile and in the SPAN tag for desktops.</li>
		<li><strong>Images with Numbers: </strong>The shortcode for images with phone numbers actually builds the image HTML so it can be wrapped in a SPAN for mobile linking. See the FRN Help tab above for details and more options.</li>
		<li><strong>Analytics:   </strong>In Analytics, all phone numbers that don't have an "action" set with the shortcode will use the event label "Phone Clicks (General)". The only time an action should be set is when the phone is in a prominent location on a page you wanted tracked separately. If the number is in the body of an article, you can just use the plain "[frn_plugin]" shortcode.</li>
		<li><strong>Activate Custom Fields: </strong>By default, this phone shortcode only works in frn_plugin fields and shortcodes, widget content (default), widget titles (activate at bottom), Yoast SEO fields (activate at bottom), and content (always). You can put them in other places by <a href="https://developer.wordpress.org/reference/functions/do_shortcode/" target="_blank">filtering the content through the shortcode activation</a>.</li>
		<li style="margin-top:15px;">
			<strong>Options for All Devices: </strong>
			<ul class="frn_level_2" >
				<li><b>text:</b> What you put here will be the text used for all devices.</li>
				<li><b>text="remove":</b> Using text="remove" will ONLY deactivate the shortcode for desktops. The initial concept is that we want to use the same text for all devices and just change it for desktops. That became confusing, but we had to maintain the original intent for sites that still use expect that.</li>
				<li><b>text="empty":</b> On desktops, this leaves the SPAN HTML with any defined ID or CLASS and no text. For mobile, the phone number wouldn't be displayed but the link for it would remain.</li>
				<li><b>id:</b> IMPORTANT: This is added to the link for mobile devices and the SPAN for desktops. </li>
				<li><b>class:</b> IMPORTANT: This is added to the link for mobile devices and the SPAN for desktops. </li>
				<li><b>category:</b> Don't use this unless necessary. The default is "Phone Numbers". But if you're analyzing a global contact options feature, you may want phone number clicks to be grouped with other contact clicks.</li>
				<li><b>action:</b> You MUST include "Phone Clicks" into whatever action you determine. That text is what makes sure phone number clicks are tracked as conversions in Analytics. Customize this only if you want to track a phone number position on a page seperate from other phone numbers. It's really helpful when testing a design position across many pages on a site if single pages may not have enough traffic to be conclusive.</li>
				<li><b>Images:</b> If you put anything in the "image_url" attribute, the "text" attribute will be ignored. For desktops only, if you use text="remove", the image will also be removed. This is used frequently on niche sites. Essentially, images are links on phones and not linked on desktop unless "desktop_url" is used.</li>
			</ul>
		</li>
		<li style="margin-top:15px;">
			<strong>Desktop Customizations: </strong>
			<ul class="frn_level_2" >
				<li><b>desktop_text:</b> Customize text by including just the "desktop" attribute. It will override the "text" and "mobile_text" attributes.</li>
				<li><b>desktop_text="remove":</b> Using desktop="remove" or text="remove" essentially deactivates the shortcode for desktops. You can use this if you just want a phone number option to disappear for desktop users.</li>
				<li><b>desktop_text="empty":</b> Leaves the SPAN HTML with any defined ID or CLASS but doesn't display text.</li>
				<li><b>desktop_id:</b> Overrides the normal "id" and "mobile_id" to customize any effects. Since phone numbers aren't linked on desktops, this is used in the SPAN tag.</li>
				<li><b>desktop_class:</b> Overrides the normal "class" and "mobile_class" to customize any effects. Since phone numbers aren't linked on desktops, this is used in the SPAN tag.</li>
				<li><b>desktop_category:</b> Overrides the normal "category" and "mobile_category" to customize what's reported to Analytics.</li>
				<li><b>desktop_action:</b> Overrides the normal "action" and "mobile_action" to customize what's reported to Analytics. If using desktop_url, the default action is "Desktop Switch with [url]" unless you fill this out.</li>
				<li><b>desktop_url:</b> If you'd like to switch out the number with a link to the contact page, for example, use both "desktop" and "desktop_url" attributes (e.g. <span class="frn_shortcode_sel">'desktop="Contact" desktop_url="/contact/"</span>). Mobile devices will still get a linked phone number, but it'll switch to just a "/contact/" link for desktop users. Only "desktop_url" is required. "Contact" is the default text unless you want something else. The TEXT attribute becomes a mobile-only attribute at that point, so you'll likely want both attributes if you want to use a text version for mobile.</li>
				<li><b>Images:</b> If you put anything in the "image_url" attribute, the "text" attribute will be ignored. For desktops only, if you use text="remove", the image will also be removed. This is used frequently on niche sites. For desktops, if you use an ID or CLASS, they will apply to the image, not the link.</li>
			</ul>
		</li>
		<li style="margin-top:15px;">
			<strong>Mobile Customizations: </strong>
			<ul class="frn_level_2" >
				<li><b>mobile_text:</b> Customize text by including just the "mobile" attribute. It will override the "text" and "desktop_text" attributes.</li>
				<li><b>mobile_text="remove":</b> Using mobile="remove" essentially deactivates the shortcode for smartphones and tablets (text="remove" will not affect mobile in order to maintain the shortcode's initial use concept). You can use this if you just want a phone number option to disappear for mobile users.</li>
				<li><b>mobile_text="empty":</b> Leaves the SPAN HTML with any defined ID or CLASS but doesn't display text. Useful if relying on CSS to provide an icon or other formatting.</li>
				<li><b>mobile_id:</b> Overrides the normal "id" and "mobile_id" to customize any effects.</li>
				<li><b>mobile_class:</b> Overrides the normal "class" and "mobile_class" to customize any effects.</li>
				<li><b>mobile_category:</b> Overrides the normal "category" and "mobile_category" to customize what's reported to Analytics. Default is "Phone Numbers".</li>
				<li><b>mobile_action:</b> Overrides the normal "action" and "mobile_action" to customize what's reported to Analytics. The default is "Phone Clicks [General]" unless specified.</li>	
				<li><b>Images:</b> If you put anything in the "image_url" attribute, the "text" and "mobile_text" attributes will be ignored. This is used frequently on niche sites.</li>
			</ul>
		</li>
	</div>
	<?php
}

function frn_phones_mobile() {
	$phones=frn_phone_old_to_new();
	//Due to the input name, once saved, the old "site_phone_number" will be erased and all settings will switch to the new "phones" primary array parent var.
	//phone_linking = processing method
	if(isset($phones['phone_linking'])) $phone_linking=$phones['phone_linking'];
		else $phone_linking="";
	?>
	<select id='frnphone_linking' name='phones[phone_linking]'>
	  <option value="PHP" <?=($phone_linking=="PHP" || $phone_linking==="") ? "selected " : "" ; ?>>Activated</option>
	  <!--
		//Removed since option doesn't work. With DT/Infinity, we'd have to time the linking prior to their functions running to make sure the tel: link matching the number linked.
	  	<option value="JS" <?=($phone_linking=="JS") ? "selected " : "" ; ?>>JavaScript (not working as of v2.7)</option>
	  -->
	  <option value="N" <?=($phone_linking=="N") ? "selected " : "" ; ?>>Deactivated</option>
	  <option value="R" <?=($phone_linking=="R") ? "selected " : "" ; ?>>Remove Shortcodes</option>
	</select> 
	
	<div id="frn_plugin_scantype_help" class="frn_help_boxes">
		Activated is the default linking setting. It adds links around phone numbers before the page is loaded in the browser.
		<!--<ul class="frn_level_1">
			<li>Activated is the default linking setting. It adds links around phone numbers before the page is loaded in the browser.</li>
			<li>The JavaScript processor adds SPAN tags around phone numbers for mobile phones only. Then when mobile devices visit a page, that triggers the JavaScript code to scan a page for SPANs using a specific ID and then turns those into linked numbers as the page loads. Page loading is a little slower but it works when PHP can't.</li>
			<li>When phone numbers using the shortcode are in fields other than content and widgets, JavaScript is the better option if you can\'t program the PHP shortcode in those areas. JavaScript looks for those within everything between the BODY tags.</li>
			<li>If the script for a JavaScript phone number program (like DialogTech or Infinity) is added to the phone field instead of a typical phone number, this system automatically switches to the JavaScript method of adding Analytics tracking and linking. You can turn the linking off all together by selecting "none" for the processing.</li>
		</ul>-->
	</div>
	<?php
}

function frn_phones_auto() {
	$phones=frn_phone_old_to_new();
	//Due to the input name, once saved, the old "site_phone_number" will be erased and all settings will switch to the new "phones" primary array parent var.
	$auto_scan="C";  //default setting if not already set
	if(isset($phones['auto_scan'])) $auto_scan = $phones['auto_scan'];
	?>
	<select id='frnphone_auto_scan' name='phones[auto_scan]'>
	  <option value="DA" <?=($auto_scan=="D" || $auto_scan==="") ? "selected " : "" ; ?>>Disabled</option>
	  <option value="WC" <?=($auto_scan=="WC" || $auto_scan=="A") ? "selected " : "" ; ?>>Widgets & Content</option>
	  <option value="C" <?=($auto_scan=="C") ? "selected " : "" ; ?>>Only Main Content</option>
	  <option value="W" <?=$auto_scan=="W" ? "selected " : "" ; ?>>Only Widgets</option>
	  <!--<option value="A" <?=$auto_scan=="A" ? "selected " : "" ; ?>>Entire Page (JavaScript)</option> //disabling since issues with JavaScript autoscan-->
	</select>
	<div id="frn_plugin_autoscan_help" class="frn_help_boxes">
		Auto Scan: This scans the selected content area for phone number patterns. To be found, phone numbers must have a period, bracket, &lt; or &gt;, or space before and/or after the number. See the FRN Help tab above for all details.
	</div>
	<br />
	<br />
	<div><small><a href="javascript:showhide('sc_info');">View Shortcode Implementation Options</a></small></div>
	<?php
	frn_phones_sc_info();
}

function frn_tracking_options() {
	$phones=frn_phone_old_to_new();
	//Due to the input name, once saved, the old "site_phone_number" will be erased and all settings will switch to the new "phones" primary array parent var.
	$dt_activate_chkd=""; $dt_ads_activate_chkd=""; 
	$section_id_dt='dt_options'; 
	$section_id_inf='inf_options';
	$section_id_none='no_service';
	if(!isset($phones['dialogtech'])) $phones['dialogtech']="";
	if($phones['dialogtech']!=="" || $phones['tracking']=="dt") $dt_activate_chkd=" checked";
	if($phones['tracking']=="dt-ads") $dt_ads_activate_chkd=" checked";
	if(!isset($phones['tracking'])) $phones['tracking']="";
	?>
	<!--<input type="radio" name="phones[tracking]" value="dt" <?=$dt_activate_chkd;?> 
		onClick="hide_infinity('none',[3,4,5,6,7]);showhide('<?=$section_id_dt;?>','block');showhide('<?=$section_id_inf;?>','none');showhide('<?=$section_id_none;?>','none');"
	 	/> DialogTech<br />
	 <input type="radio" name="phones[tracking]" value="dt-ads" <?=$dt_ads_activate_chkd;?> 
		onClick="hide_infinity('table-row',[3,4,5,6,7]);showhide('<?=$section_id_dt;?>','block');showhide('<?=$section_id_inf;?>','block');showhide('<?=$section_id_none;?>','none');"
	 	/> Infinity+DialogTech [advertising (&st-t)]<br />-->
	<input type="radio" name="phones[tracking]" value="infinity" <?=($phones['tracking']=="infinity") ? ' checked' : null ;?> 
		onClick="hide_infinity('table-row',[3,4,5,6,7]);showhide('<?=$section_id_dt;?>','none');showhide('<?=$section_id_inf;?>','block');showhide('<?=$section_id_none;?>','none');" 
		/> Infinity<br />
	<input type="radio" name="phones[tracking]" value="none" <?=($phones['tracking']=="none" || $phones['tracking']=="") ? ' checked' : null ;?> 
		onClick="hide_infinity('none',[3,4,5,6,7]);showhide('<?=$section_id_dt;?>','none');showhide('<?=$section_id_inf;?>','none');showhide('<?=$section_id_none;?>','block');" 
		/> None (All fallback to main number)
	<div id="frn_plugin_tracking_help" class="frn_help_boxes">
		<ul class="frn_level_1">
			<!--<li>When the Infinity+DialogTech option is activated:
				<ul class="frn_level_2">
					<li>It means DT will turn on only when the "st-t" url tag is present in the URL.</li>
					<li>Infinity pools are also deactivated (i.e. auto discovery numbers are removed and the main number displays instead), but their analytics will still be turned on. So activity will show, but there won't be any call conversions or any admit tracking.</li>
					<li>To help the user's experience remain consistent, the url tag is stored in a cookie for up to one day. That makes sure DT follows them through the site and Infinity pools remain deactivated on successive pages for that user for up to a 24 hours.</li>
					<li>If cookies are disabled, the PHP session takes over and as long as that person is active on the site, DT will be turned on and Infinity pools will be deactivated.</li>
					<li>COOKIES: If you need to clear the DialogTech trigger, simply add <b>?st-t=cancel</b> to the end of any URL. This will clear the cookie if you intended to just test it.</li>
				</ul>
			-->
		</ul>
	</div>
	<?php
}

function frn_tracking_services() {
	$hide_none="";
	$phones=frn_phone_old_to_new();
	//Due to the input name, once saved, the old "site_phone_number" will be erased and all settings will switch to the new "phones" primary array parent var.
	if(!isset($phones['tracking'])) $phones['tracking']="";
	if(!isset($phones['dialogtech'])) $phones['dialogtech']="";
	if($phones['dialogtech']=="" && ($phones['tracking']=="" || $phones['tracking']=="none")) {
		$hide_none="yes";
		frn_admin_infinity_hide(array(3,4,5,6,7,8)); //hide infinity options
	}
	?>
	<div id="no_service" style="display:<?=(($hide_none=='yes') ? 'block' : 'none' );?>" >
		[No tracking services selected]
	</div>
	<?php
	frn_admin_dialogtech();
	if(function_exists('frn_infinity_analytic_options')) {
		frn_admin_infinity_act($phones);
	}
}

function frn_admin_dialogtech($phones="",$old_phone="") {
	$phones=frn_phone_old_to_new();
	$section_id_dt='dt_options'; $dt_activate_chkd="";
	if(!isset($phones['tracking'])) $phones['tracking']="";
	if($phones['tracking']=="" && $phones['dialogtech']!=="") $phones['tracking']="dt";
	if(!isset($phones['dialogtech'])) $phones['dialogtech']="";
	if($phones['dialogtech']!=="" || $phones['tracking']=="dt") $dt_activate_chkd="yes";

	if(!isset($phones['dialogtech_id'])) $phones['dialogtech_id']="";
	if($phones['dialogtech_id']=="") $phones['dialogtech_id']="29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";
	?>
	<div id="<?=$section_id_dt;?>" style="display:<?=(($dt_activate_chkd!=='') ? 'block' : 'none' );?>" >
		DT ID: <input name="phones[dialogtech_id]" size="50" type="text" value="<?=$phones['dialogtech_id'];?>" />
		<div id="frn_plugin_dt_help" class="frn_help_boxes">
			<ul class="frn_level_1"  >
				<li>Activate this if this site has been set up in DialogTech's services. Zander is our typical contact for this vendor.</li>
				<li>This service allows us to track all phone calls in Analytics. Activating the service here adds their JavaScript code into the footer of all pages. Their JavaScript looks for the phone number you added above on all pages and replaces it with a reserved phone number for this site. </li>
				<li>As of Fall 2016, we try to reserve enough phone numbers to account for the peak of concurrent users on a site multiplied by our typical conversion rate. This makes sure if the expected number of people called around the same time, their individual activities would be appropriately recorded in Analytics.</li>
				<li>If you change the phone number above, it will deactivate DialogTech tracking. Be sure to change the phone number in their settings and above ASAP to avoid any disruption.</li>
				<li>Employees need to know that the phone numbers showing on the site are not FRN's. So they can't plan to use the number they find anywhere else. Furthermore, if people save the number in their phone, someday the number will stop working. But before that, their activity will inappropriately be reported to Analytics as a conversion.</li>
				<li><a href="https://secure.dialogtech.com/login.php" target="_blank">Link to DialogTech login</a></li>
			</ul>
		</div>
	</div>
	<?php
}



function frn_phone_report() {
	//formats only work if everything removed but numbers and letters
	$pattern_10 = '/([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
	$format_10 = '($1) $2-$3';
	$pattern_11 = '/([0-9])([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
	$format_11 = '($1) $2-$3';
	$list_id = "find_numbers";
	?>
	<div id="infinity_report">
		<p>
			The phone numbers below are typically old numbers that were used on all sites. This report helps you find them on the site more quickly to replace with the facility shortcode.
			The system searches all content types, not just pages and posts.
		</p>
		<div class="frn_options_table">
		<?php 
		$oldnumbers=frn_phone_old_numbers(); //get standard list of old numbers
		$oldnumbers=array_column($oldnumbers, 0 );
		//print_r($oldnumbers);
		frn_admin_sc_search_posts($oldnumbers,'old_numbers'); //search pages/posts for the phone numbers (returns array of numbers and found totals)
		


		/*
		//Initial approach where we could modify the list of old numbers directly in the table.
		//After "any" post type fail, simply incorporated new shortcodes report. 2/25/19
		?>
		<a href="javascript:edit_table('<?=$list_id;');">Edit List</a>
		<table class="frn_options_table frn_data" id="<?=$list_id;?>">
			<tr>
				<th style="width:15px;"></th>
				<th style="width:inherit;">Old Number</th>
				<th style="width:inherit;">Call-In Code</th>
				<th style="width:inherit; text-align:center;">Pages/Posts</th>
				<th style="width:inherit; text-align:center;">Search</th>
				<th style="width:inherit;"></th>
			</tr>
			<?php
			//Numbers found in content
			if(function_exists('frn_phone_old_numbers')) {
				$i=0; $oldnumbers=frn_phone_old_numbers(); //get standard list of old numbers
				$oldnumbers=array_column($oldnumbers, 0 );
				//print_r($oldnumbers);
				
				foreach($results[0] as $result) {
					//$array_id =       [0]       [1]     [2]
					//$result = array($number,$callcode,$total);
					$oldnumber=frn_phone_patterns($result[0]);
					$search_number=substr($result[0],3,3)."-".substr($result[0],6,4);
					$search='<a href="/wp-admin/edit.php?s='.$search_number.'&post_status=all&post_type=post">posts</a><br /><a href="/wp-admin/edit.php?s='.$search_number.'&post_status=all&post_type=page">pages</a><br />custom?'; 
					//$oldnumber=substr($old_number,0,5)."-<b>".substr($old_number,6,8)."</b>";
					frn_table_row_template($list_id,$i,null,0,array($oldnumber,$result[1],$result[2],$search)); //table ID, row #, row edit option, input fields, cells array(old number, call-in code, quantity found, search links)
					$i++;
				}
				//Numbers not found (doing this next to sort these lowest in the table)
				$notfound=$results[1];
				//print_r($notfound);
				//$array_id=        [0]      [1]
				//$oldnumber=array($number,$callcode);
			}
			else $notfound=array();
			?>
		</table>
		<a href="javascript:showhide('posts_not_found');">View numbers not found</a>
		<table id="posts_not_found" style="display:none;">
			<tr>
				<th style="width:15px;"></th>
				<th style="width:inherit;">Old Number</th>
				<th style="width:inherit;">Call-In Code</th>
			</tr>
			<?php 
			foreach($notfound as $oldnumber) {
				//print_r($oldnumber);
				$number=frn_phone_patterns($oldnumber[0]);
				frn_table_row_template($list_id,$i,null,0,array($number,$oldnumber[1])); //table ID, row #, row edit option, input fields, cells array(old number, call-in code, quantity found, search links)
				$i++;
			} ?>
		</table>
		<br />
		<div>
			<a href="javascript:add_row('<?=$list_id;?>',-1,'<?=$list_id."_".($rows);?>',2,'-');" title="Add another row">Add Row</a>
			<?php // add_row(table_id,id,parent_var,inputs,edit) ?>
		</div>
		*/ ?>
		</div>
	</div>
	<br />
	<br />
	<?php
}


function frn_phone_search($oldnumbers) {
	/*
	//Discontinued 2/24/19 after "any" post type search failed on DD.org.
	//incorporated this phone number search into the shortcodes report.
	//print_r($oldnumbers);
	$notfound=array();
	$found=array(); $i=1;
	foreach($oldnumbers as $old_number) {
		//$old_number = array($number,$callcode);
		$s = substr($old_number[0],3,3)."-".substr($old_number[0],6,4);
		$results = new WP_Query( array( 
			's' 				=> $s, 
			'post_type' 		=> 'any',
			'posts_per_page' 	=> 0,
			'post_status' 		=> 'any'
		));
		$total=$results->found_posts;
		//if($i==1) echo "Dax: ".$s."; ";
		if(!$total) $total=0;
		if($total>0) {
			$found[] = array($old_number[0],$old_number[1],$total); //number, call code, total found
		}
		else {
			//if($notfound!=="") $notfound.=",";
			$notfound[] = array($old_number[0],$old_number[1]);

		}
		wp_reset_postdata(); $i++;
	}
	//print_r($found);
	//echo "<br />";
	//print_r($notfound);
	return array($found,$notfound);
	*/
}
///need to search the php results for the numbers to match results
//results need to be in a DIV in the cell that expands based on plus sign
function frn_phone_search_php(){
	if(function_exists('frn_phone_old_numbers')) {
		$oldnumbers=frn_phone_old_numbers(); //get standard list of old numbers
		//print_r($oldnumbers);
		$php_finds=frn_admin_sc_search_php($oldnumbers);
		//print_r($php_finds);
		$found_cnt=count($php_finds); $plural="";
		$plural = ($found_cnt==1) ? "" : "s";
		$colon = ($found_cnt===0) ? "" : ":";
		echo '<div style="margin-bottom:7px;font-weight:bold;">'.$found_cnt . " phone number" . $plural . " found".$colon." </div>";
		$found_php=$php_finds; $theme="";
		if($found_cnt>0) {
			$my_theme = wp_get_theme();
			$theme=$my_theme->get( 'Name' );
			$found_prior="";
			foreach( $found_php as $found) {
				$i=0; $array_search=str_replace("-","",$found[0]);
				foreach ($oldnumbers as $number) {
			      if (strpos($number[0], $array_search) !== FALSE) {
			         $key = $i; break;
			      } $i++;
			    }
				
				if($found_prior==$found[1]) echo ", [".$found[0]."; ".$oldnumbers[20][1]."]";
				else {
					echo '<li><b><a href="/wp-admin/theme-editor.php?file='.$found[1].'&theme='.$theme.'" target="_blank" >'.$found[1]."</a>:</b> [". $found[0]."; ".$oldnumbers[$key][1]."]";
				}
				$found_prior = $found[1];
			}
		}
	}
	else {
		echo "<li>Old numbers array not available for frn_phone_search_php function.</li>";
	}
}

function frn_table_row_template($table_id="",$row_i=0,$edit="-",$inputs=0,$cols) {
	//data submitted: $phones[infinity][table-group][phone number][field sequence ID]
	//											- although "field" uses numbers like an array sequence, since the number is actually provided in the field name, it becomes a key instead. But this kept things programatically simpler.
	// 											- the fields align with the column numbers to help the sequence be easier to match up
	$table_type="";
	if(stripos($table_id,"tag")!==false) $table_type="tags";
	elseif(stripos($table_id,"pool")!==false) $table_type="pools";
	?>
			<tr id="<?=$table_id;?>_<?=$row_i;?>">
				<?php
	
	//col[0] is always used as the field variable. It's important to clean it and make sure it's unique.
	//"blended" class causes the input styling to be hidden when an input is included. Otherwise, the class just doesn't activate naturally.
	if(is_array($cols)) { 
		//print_r($cols);
		$col_n=1; foreach($cols as $col) { 
			//print_r($col); 
			if($col=="count") { ?>
				<td><?=$row_i+1;?>.</td>
				<?php }
			else {
				$input_size="20";
				if($col_n==2) {
					$data_var=$table_id."_".$row_i; //typically 0 is a phone number, but covering the bases
					if($table_type=="pools") $input_size="17";
					elseif($table_type=="tags") $input_size="12";
				}
				elseif($col_n==3 && $table_type=="pools") $input_size="35";
				elseif($col_n==4) {
					if($table_type=="tags") {$input_size="35";}
					elseif($table_type=="pools") {$input_size="15";}
				} 
				
				if($data_var!=="") $data_var="[".$data_var."]";
				if($col=="empty") $col="";
				$col=str_replace("##i##",$col_n,$col);
				?>
				<td class="blended">
					<?php if($inputs>=$col_n) {
					?><input name="phones[infinity][<?=$table_id;?>]<?=$data_var;?>[<?=$col_n;?>]" size="<?=$input_size;?>" type="text" value="<?=$col;?>" onClick="remove_class(this);" /><?php 
					}
					else echo $col;
					?>
				</td>
				<?php 
				$col_n++;
			}
		}
	}
	if($edit!=="no") { ?>
				<td style="white-space:nowrap;">
					<?php if($edit=="yes" || $edit=="-") { ?><a href="javascript:delete_row('<?=$table_id;?>','<?=$table_id;?>_<?=$row_i;?>');" title="Remove this">[&#8211;]</a><?php } ?>
					<?php if($edit=="yes" || $edit=="+") { ?><a href="javascript:add_row('<?=$table_id;?>',<?=($row_i+1);?>,'<?=$data_var;?>',<?=$inputs;?>,'<?=$edit;?>');" title="Add another row">[+]</a><?php }
																				// add_row(table_id,id,parent_var,inputs,edit) ?>
				</td><?php } ?>
			</tr>
	<?php 
}


//////
// TINY MCE TOOLBAR SHORTCODE DROPDOWNS
//////
include("admin_toolbar_shortcodes.php");



function frn_save_phone_settings($input) {
	//sets up an array in case more variables should be added
	$input['site_phone'] = trim($input['site_phone']);
	if($input['dialogtech_id']!=="") {
		$input['dialogtech_id'] = trim($input['dialogtech_id']);
	}

	//print_r($input);
	return $input;
}