<?php // MyPlugin - Validate Settings



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}



// callback: validate options
function myplugin_callback_validate_options( $input ) {
		
	// custom title
	if ( isset( $input['shortcode_class_field'] ) ) {
		
		$input['shortcode_class_field'] = sanitize_text_field( $input['shortcode_class_field'] );
		
	}
		
	// custom scheme
	$select_options = myplugin_options_select();
	
	if ( ! isset( $input['facility_selector'] ) ) {
		
		$input['facility_selector'] = null;
		
	}
	
	if ( ! array_key_exists( $input['facility_selector'], $select_options ) ) {
		
		$input['facility_selector'] = null;
	
	}
	
	return $input;
	
}