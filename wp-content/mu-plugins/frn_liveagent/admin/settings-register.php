<?php // FRN LiveAgent - Register Settings



// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}



// register plugin settings
function frn_liveagent_register_settings() {
	
	/*
	
	register_setting( 
		string   $option_group, 
		string   $option_name, 
		callable $sanitize_callback = ''
	);
	
	*/
	
	register_setting( 
		'frn_liveagent_options', 
		'frn_liveagent_options', 
		'frn_liveagent_callback_validate_options' 
	); 
	
	/*
	
	add_settings_section( 
		string   $id, 
		string   $title, 
		callable $callback, 
		string   $page
	);
	
	*/
		
	add_settings_section( 
		'frn_liveagent_section_admin', 
		'Admin Options', 
		'frn_liveagent_callback_section_admin', 
		'frn_liveagent'
	);

	add_settings_section( 
		'frn_liveagent_section_shortcode', 
		'Shortcode Options', 
		'frn_liveagent_callback_section_shortcode', 
		'frn_liveagent'
	);

	add_settings_section( 
		'frn_webtolead_section_shortcode', 
		'Shortcode Options', 
		'frn_webtolead_callback_section_shortcode', 
		'frn_liveagent'
	);

	/*
	
	add_settings_field(
    	string   $id, 
		string   $title, 
		callable $callback, 
		string   $page, 
		string   $section = 'default', 
		array    $args = []
	);
	
	*/
	//Shortcode Admin Field
	add_settings_field(
		'shortcode_class_field',
		'Shortcode Class',
		'frn_liveagent_callback_field_text',
		'frn_liveagent', 
		'frn_liveagent_section_shortcode', 
		[ 'id' => 'shortcode_class_field', 'label' => 'Add class styling to the Live Agent Shortcode.' ]
	);
	//Activate LiveAgent Check	
	add_settings_field(
		'activate_liveagent',
		'Activate LiveAgent',
		'frn_liveagent_callback_field_checkbox',
		'frn_liveagent', 
		'frn_liveagent_section_admin', 
		[ 'id' => 'activate_liveagent', 'label' => 'Activate LiveAgent for use on this Wordpress site. (Please select corresponding site below.)' ]
	);
	//Facility Dropdown Menu
	add_settings_field(
		'facility_selector',
		'Facility Selector',
		'frn_liveagent_callback_field_select',
		'frn_liveagent', 
		'frn_liveagent_section_admin', 
		[ 'id' => 'facility_selector', 'label' => 'Select what facility this LiveAgent instance is associated with.' ]
	);
	//Web To Lead Form Field
	add_settings_field(
		'webtolead_url_field',
		'Web To Lead Shortcode URL',
		'frn_liveagent_callback_field_text',
		'frn_liveagent', 
		'frn_webtolead_section_shortcode', 
		[ 'id' => 'webtolead_url_field', 'label' => 'Add URL to page where form shortcode is located.' ]
	);

	//WTL Thank You Link
	add_settings_field(
		'webtolead_thankyou_field',
		'Web To Lead Thank You Page URL',
		'frn_liveagent_callback_field_text',
		'frn_liveagent', 
		'frn_webtolead_section_shortcode', 
		[ 'id' => 'webtolead_thankyou_field', 'label' => 'Add URL for Thank You page after form is filled out.' ]
	);
    
} 
add_action( 'admin_init', 'frn_liveagent_register_settings' );


