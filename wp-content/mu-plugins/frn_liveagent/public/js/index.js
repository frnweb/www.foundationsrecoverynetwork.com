window.jQuery = $;
window.$ = $;

$(document).ready(function(){
    $('#00Nj000000BKJje').on('change', function() {
      if ( this.value === 'Self')
      {
        $(".patient-reveal").hide();
        $(".contact-reveal").hide();
      }
        
      else
      {
        $(".patient-reveal").show();
        $(".contact-reveal").show();  
      }
    });


    $("input[type='radio']#speed-up-yes").click(function(){
    // alert("The paragraph was clicked.");
    $("#additional-fields").show();
    });

    $("input[type='radio']#speed-up-no").click(function(){
    //alert("The paragraph was clicked.");
    $("#additional-fields").hide();
    });

    $('.sl_tooltip.insurance-company').html('<img src="/wp-content/mu-plugins/frn_liveagent/public/img/insurance-company.jpg">');
    $('.sl_tooltip.insurance-id').html('<img src="/wp-content/mu-plugins/frn_liveagent/public/img/insurance-id.jpg">');
    $('.sl_tooltip.insurance-phone').html('<img src="/wp-content/mu-plugins/frn_liveagent/public/img/insurance-phone.jpg">');
 
});///this is for a dropdown menu to show or hide a div on click 

    //reCAPTCHA
    // function recaptcha_callback(){
    //     var response = document.getElementById("g-recaptcha-response"); 
    //     if (response == null || response.value.trim() == "") {
    //         var elems = JSON.parse(document.getElementsByName("captcha_settings")[0].value);
    //         elems["ts"] = JSON.stringify(new Date().getTime());document.getElementsByName("captcha_settings")[0].value = JSON.stringify(elems);
    //     } 

    //     $('.sl_button--secure').prop("disabled", false);
    // };

/*
    Note : Call after page is loaded
    Library API:
    {
        "onlineButtonId"         : "online_button_id",
        "offlineButtonId"        : "offline_button_id",
        "orgId"                  : "",
        "deploymentId"           : "",
        "buttonId"               : "",
        "windowWidth"            : 1200, 
        "windowHeight"           : 900,
        "preChatHeaderHtml"      : "<h1>Header</h1>"
        "preChatSideSectionHtml" : "<h1>Header</h1>"
        "preChatFormCSS"         : "//branding.css"
        "callincode"             : "Google (Web Bear)"
    }
*/
	
$(document).ready(function() {
    //Adds classes to the chat buttons via the class field in Admin
    // $('#liveagent_button_online_573g00000008PGW').addClass(add_class.class);
    // $('#liveagent_button_offline_573g00000008PGW').addClass(add_class.class);

    // $('#liveagent_button_online_573g00000008PGW').click(function() {
    //     ga('send', 'event', 'Live Chat', 'Chat Button', 'Chat Button Click');
    // });

    //Adds Web To Lead Url to Email Us Button.
    // $('#liveagent_button_offline_573g00000008PGW').href = add_url.url;

    $('#00Nj000000BKJjr').append(add_url.url);

    //Adds Thank you URL to form after completion.
    $('#thankyou-url').val(add_thankyou_url.ty_url);
});


// var frn = frn || {};
// var LIVEAGENT;
// frn.initializeLive = function(){
// 	LIVEAGENT = FRNLiveAgent({
// 		"onlineButtonId"   : "liveagent_button_online_573g00000008PGW",
// 		"offlineButtonId"  : "liveagent_button_offline_573g00000008PGW",
// 		"windowWidth"      : 1024, 
// 		"windowHeight"     : 650,
// 		"preChatFormCSS"   : add_scheme.scheme
// 	});

// 	LIVEAGENT.init();
// };

// window.addEventListener('load', frn.initializeLive , false);

// var FRNLiveAgent = function (o) {

//     //private variables
//     var MAX_RETRY       = 10; 
//     var retryCount      = 0;
//     var CHAT_API_URL    = 'https://d.la1-c2cs-iad.salesforceliveagent.com/chat';
//     var DEPLOYMENT_API  = 'https://c.la1-c2cs-iad.salesforceliveagent.com/content/g/js/44.0/deployment.js';
//     var defaultCustomFields = [{
//             "label" : "Site_Domain" ,
//             "value" : window.location.hostname
//         },
//         {
//             "label" : "preChatHeaderHtml" ,
//             "value" : o.preChatHeaderHtml
//         },
//         {
//             "label" : "preChatSideSectionHtml" ,
//             "value" : o.preChatSideSectionHtml
//         },
//         {
//             "label" : "callincode" ,
//             "value" : "Unknown"
//         },
//         {
//             label   : "preChatFormCSS",
//             "value" : o.preChatFormCSS
//         },
//         {
//             label   : "leadPreScreeningSource",
//             "value" : "Online"
//         }
//     ];

//     //load deployment api first
//     var addLiveAgentToPage = function(){
//         var script_tag = document.createElement('script');
//         script_tag.setAttribute('src', DEPLOYMENT_API);
//         document.head.appendChild(script_tag);
//     };
//     addLiveAgentToPage();

//     //utility method
//     function extend(){
//         for(var i=1; i<arguments.length; i++) {
//             for(var key in arguments[i]) {
//                 if(arguments[i].hasOwnProperty(key)) {
//                     arguments[0][key] = arguments[i][key];
//                 }
//             }
//         }
//         return arguments[0];
//     }

//     o = extend({
//         "orgId"            : "00Dg0000001qSFe",
//         "buttonId"         : "573g00000008PGW"
//     },o);

//     var deploymentIdMap = {
//         "127.0.0.1"             : "572g00000008Ohj", 
//         "frn.hashbase.io"       : "572g00000008Ohj", 
//         "frnliveagent.surge.sh" : "572g00000008Ohj",
//         "blackbearrehab.com" : "572g00000008Ohj",
//         "www.foundationsrecoverynetwork.com" : "572g00000008Ohj",
//         "talbottcampus.com" : "572g00000008Ohj",
//         "www.michaelshouse.com" : "572g00000008Ohj",
//         "theoakstreatment.com" : "572g00000008Ohj",
//         "www.dualdiagnosis.org" : "572g00000008Ohj",
//         "thecanyonmalibu.com" : "572g00000008Ohj",
//         "www.soberhero.com" : "572g00000008Ohj",
//         "skywoodrecovery.com" : "572g00000008Ohj",
// 		"www.rehabandtreatment.com" : "572g00000008Ohj"
//     };

//     if( !o.deploymentId ){
//         o.deploymentId = "572g00000008Ohj";
//     }

//     var initialize = function(){
//         //if live agent is not loaded try again after some time
//         try{
//             if(!liveagent && retryCount<MAX_RETRY){
//                 ++retryCount;
//                 return setTimeout(initialize,500);
//             }
//         }catch(exp){
//             return setTimeout(initialize,500);
//         }
        
//         window._laq = window._laq || []; 
//         window._laq.push(function(){
//             liveagent.showWhenOnline(o.buttonId, document.getElementById(o.onlineButtonId));
//             liveagent.showWhenOffline(o.buttonId, document.getElementById(o.offlineButtonId));
//         });

//         // Sets the width of the chat window 
//         liveagent.setChatWindowWidth(o.windowWidth);
//         // Sets the height of the chat window
//         liveagent.setChatWindowHeight(o.windowHeight);

//         //add custom field
//         initDefaultCustomFields();

//         liveagent.init(CHAT_API_URL, o.deploymentId, o.orgId);
//     };

//     var initDefaultCustomFields = function(){
//         for(var index=0;index<defaultCustomFields.length; ++index){
//             var customField = defaultCustomFields[index];
//             if( customField && customField.value){
//                 liveagent.addCustomDetail(customField.label, customField.value);
//             }
//         }
//     };

//     //public api 
//     return {
//         init : function(){
//             initialize();
//         },
//         startChat : function(){
//             return liveagent && liveagent.startChat(o.buttonId);
//         },
//         startChatInIFrame : function(iframeName){
//             //example <iframe name="chat-window" then iframeName = "chat-window"
//             return liveagent && liveagent.startChatWithWindow(o.buttonId,iframeName);
//         },
//         addCustomDetail : function(label,value){
//             return liveagent && liveagent.addCustomDetail(label,value);
//         },
//         setCallInCode : function(callInCodeValue){
//             return this.addCustomDetail("callincode",callInCodeValue);
//         }
//     };
// };