<?php // FRN LiveAgent - Core Functionality

//If activated, enqueues the javascript for functionality
function frn_liveagent_activation() {
	
	// $activation = false;
	
	// $options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );
	
	// if ( isset( $options['activate_liveagent'] ) && ! empty( $options['activate_liveagent'] ) ) {
		
	// 	$activation = filter_var( $options['activate_liveagent'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );
		
    // }
    	
	// if ( $activation === true ) {
		
		/*
				
		wp_enqueue_script( 
			string           $handle, 
			string           $src = '', 
			array            $deps = array(), 
			string|bool|null $ver = false, 
			bool             $in_footer = false 
		)
		
		*/
		wp_enqueue_style('frn_liveagent_css', plugin_dir_url( dirname( __FILE__ ) ) . 'public/css/button_styling.css', array(), null, true );

		wp_deregister_script('jQuery');

		wp_enqueue_script('jQuery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

		wp_enqueue_script( 'frn_liveagent', plugin_dir_url( dirname( __FILE__ ) ) . 'public/js/index.js', array('jQuery'), null, true );

	// }
	
}
add_action( 'wp_enqueue_scripts', 'frn_liveagent_activation' );



//sanitizes and localizes class field for use in shortcode
function send_shortcode_value() {
	
	$options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );
	
	if ( isset( $options['shortcode_class_field'] ) && ! empty( $options['shortcode_class_field'] ) ) {
		
        $shortcode_class = esc_attr($options['shortcode_class_field']);
	}
	else {
		$shortcode_class = null;
	}

	wp_localize_script('frn_liveagent', 'add_class', array(
		'class' => $shortcode_class
	)
); 
}
add_action( 'wp_enqueue_scripts', 'send_shortcode_value' );


//sanitizes and localizes class field for use in shortcode
function send_webtolead_value() {
	
	$options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );
	
	if ( isset( $options['webtolead_url_field'] ) && ! empty( $options['webtolead_url_field'] ) ) {
		
        $webtolead_url = esc_attr($options['webtolead_url_field']);
	}
	else {
		$webtolead_url = null;
	}

	wp_localize_script('frn_liveagent', 'add_url', array(
		'url' => $webtolead_url
	)
); 
}
add_action( 'wp_enqueue_scripts', 'send_webtolead_value' );


//sanitizes and localizes class field for use in shortcode
function send_thankyou_url_value() {
	
	$options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );
	
	if ( isset( $options['webtolead_thankyou_field'] ) && ! empty( $options['webtolead_thankyou_field'] ) ) {
		
        $thankyou_url = esc_attr($options['webtolead_thankyou_field']);
	}
	else {
		$thankyou_url = null;
	}

	wp_localize_script('frn_liveagent', 'add_thankyou_url', array(
		'ty_url' => $thankyou_url
	)
); 
}
add_action( 'wp_enqueue_scripts', 'send_thankyou_url_value' );



// custom admin color scheme
function liveagent_chat_styles() {
	
	$scheme = 'default';
	
	$options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );
	
	if ( isset( $options['facility_selector'] ) && ! empty( $options['facility_selector'] ) ) {
		
		$scheme = sanitize_text_field( $options['facility_selector'] );
		
	}

	/*
	wp_enqueue_style( 
		string           $handle, 
		string           $src = '', 
		array            $deps = array(), 
		string|bool|null $ver = false, 
		string           $media = 'all' 
	)
	*/

	wp_localize_script('frn_liveagent', 'add_scheme', array(
			'scheme' => plugin_dir_url( dirname( __FILE__ ) ) . 'public/css/' . $scheme . '.css'
		)
	);
}
add_action( 'wp_enqueue_scripts', 'liveagent_chat_styles' );