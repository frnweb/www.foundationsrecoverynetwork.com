<?php
// LIVEAGENT WEB TO LEAD SHORTCODE
	function frn_webtolead_form () {
        return '
		<script src="https://www.google.com/recaptcha/api.js"></script>

		<script>
function timestamp() {
var response = document.getElementById("g-recaptcha-response");
if (response == null || response.value.trim() == "")
{
var elems = JSON.parse(document.getElementsByName("captcha_settings")[0].value);
elems["ts"] = JSON.stringify(new Date().getTime());
document.getElementsByName("captcha_settings")[0].value = JSON.stringify(elems);
}
}
setInterval(timestamp, 500);
</script>
<script type="text/javascript">
function recaptcha_callback(){
	$(".sl_button--secure").prop("disabled", false);
}
</script>
		
		<div class="sl_vob_form" id="vob-holder">
      	
		<!---form stuff will go here--->
		<form class="sl_form" action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding&#x3D;UTF-8" method="POST">

	    <input type="hidden" name="captcha_settings" value="{"keyname":"FRNCaptcha2","fallback":"true","orgId":"00Dj0000001rMuh","ts":""}"/> 
			<input type="hidden" name="oid" value="00Dj0000001rMuh"/>
			<input id="thankyou-url" type="hidden" name="retURL" value=""/>    
			        
			<div class="sl_inner">  
				<h2>Check Your Coverage</h2>
				<p>By requesting a <strong>risk-free, confidential assessment</strong> we promise to guide you in finding the best solution for your individual needs.</p>
		         
				<div class="sl_row">

					<div class="sl_cell medium-6">
						<h4>Who are you seeking help for today?</h4>
						<select id="00Nj000000BKJje" class="sl_form__select" name="00Nj000000BKJje" title="Calling for" required>
							<option value="" disabled selected>Select One</option>
							<option value="Self">Myself</option>
							<option value="Loved One">Loved One</option>
							<option value="Client/Patient">Client/Patient</option>
						</select><!-- end calling for dropdown-->
					 </div><!-- end sl_cell -->

					 <div class="sl_cell medium-6"> 
						<h4>Is the person in need of help 18 or older?</h4>
						<input class="sl_form__checkbox" id="00N0a00000CVU7J" name="age" type="checkbox" value="yes" /><label for="00N0a00000CVU7J">Yes</label>
						<input class="sl_form__checkbox" id="under_eighteen" name="age" type="checkbox" value="no" /><label for="under_eighteen">No</label>
					</div><!-- end sl_cell -->

				</div> <!---end sl_row -->  

				<div class="sl_divider"></div>

				<div class="sl_row">
					<div class="medium-6 sl_cell">
						<label for="first_name">
							<h4><span class="patient-reveal">Patient</span> First Name</h4>
						</label>
						<input class="sl_form__input" id="first_name" maxlength="40" name="first_name" type="text" required>
					</div><!-- end sl_cell first name -->

					<div class="medium-6 sl_cell">
						<label for="last_name">
							<h4><span class="patient-reveal">Patient</span> Last Name</h4>
						</label>
						<input class="sl_form__input" id="last_name" maxlength="80" name="last_name" type="text" required>
					</div><!-- end sl_cell last name-->
				</div><!-- end sl_row -->

			   <div id="caller-contact" class="patient-reveal">
					 <div class="sl_row">
						<div class="medium-6 sl_cell">
							<label for="00Nj000000BKJjY">
								<h4>Contact First Name</h4>
							</label>
							<input class="sl_form__input" id="00Nj000000BKJjY" maxlength="72" name="00Nj000000BKJjY" type="text" />
						</div><!-- end sl_cell contact first name-->

						<div class="medium-6 sl_cell">
							<label for="00Nj000000BKJja">
								<h4>Contact Last Name</h4>
							</label>
							<input class="sl_form__input" id="00Nj000000BKJja" maxlength="72" name="00Nj000000BKJja" type="text" /><!-- caller last name-->
						  	</div><!-- end sl_cell -->
					   	</div><!-- end sl_row -->
				</div><!-- end caller-contact -->

				<div class="sl_row">
				  <div class="medium-6 sl_cell">
						<label for="phone">
							<h4><span class="contact-reveal">Contact</span> Phone Number</h4>
						</label>
						<input class="sl_form__input" id="phone" maxlength="40" name="phone" type="text" placeholder="XXX-XXX-XXXX" required>
				  </div><!-- contact phone number-->

				   <div class="medium-6 sl_cell">
						<label for="email">
						<h4><span class="contact-reveal">Contact</span> Email <span class="optional">(Optional)</span></h4>
						</label>
						<input class="sl_form__input" id="email" maxlength="80" name="email" type="text">
				  </div><!-- end email-->
				</div><!-- end sl_row -->

			   <div class="sl_row">
					<div class="medium-3 sl_cell">
						<label for="city">
							<h4><span class="patient-reveal">Patient</span> City</h4>
						</label>
						<input class="sl_form__input" id="city" maxlength="40" name="city" size="20" type="text" requirepatient cityd/>
					</div><!-- end cell for -->

					<div class="medium-3 sl_cell">
						<label for="state_code">
							<h4><span class="patient-reveal">Patient</span> State</h4>
						</label>
							<select class="sl_form__select" id="state_code" name="state_code" required>
								<option value="AL">AL</option>
	                          <option value="AK">AK</option>
	                          <option value="AZ">AZ</option>
	                          <option value="AR">AR</option>
	                          <option value="CA">CA</option>
	                          <option value="CO">CO</option>
	                          <option value="CT">CT</option>
	                          <option value="DE">DE</option>
	                          <option value="FL">FL</option>
	                          <option value="GA">GA</option>
	                          <option value="HI">HI</option>
	                          <option value="ID">ID</option>
	                          <option value="IL">IL</option>
	                          <option value="IN">IN</option>
	                          <option value="IA">IA</option>
	                          <option value="KS">KS</option>
	                          <option value="KY">KY</option>
	                          <option value="KY">LA</option>
	                          <option value="ME">ME</option>
	                          <option value="MD">MD</option>
	                          <option value="MA">MA</option>
	                          <option value="MI">MI</option>
	                          <option value="MN">MN</option>
	                          <option value="MS">MS</option>
	                          <option value="MO">MO</option>
	                          <option value="MT">MT</option>
	                          <option value="NE">NE</option>
	                          <option value="NV">NV</option>
	                          <option value="NH">NH</option>
	                          <option value="NJ">NJ</option>
	                          <option value="NM">NM</option>
	                          <option value="NY">NY</option>
	                          <option value="NC">NC</option>
	                          <option value="ND">ND</option>
	                          <option value="NO">NO</option>
	                          <option value="OH">OH</option>
	                          <option value="OK">OK</option>
	                          <option value="OR">OR</option>
	                          <option value="PA">PA</option>
	                          <option value="RI">RI</option>
	                          <option value="SC">SC</option>
	                          <option value="SD">SD</option>
	                          <option value="TN">TN</option>
	                          <option value="TX">TX</option>
	                          <option value="UT">UT</option>
	                          <option value="VA">VA</option>
	                          <option value="VT">VT</option>
	                          <option value="WA">WA</option>
	                          <option value="WV">WV</option>
	                          <option value="WI">WI</option>
	                          <option value="WY">WY</option>
							</select><!-- end drop down for states -->
				  </div><!-- end sl_cell for state dropdown-->

				  <div class="medium-6 sl_cell">
						<label for="00N0a00000CVU7I">
							<h4><span class="patient-reveal">Patient</span> Insurance Type</h4>
						</label>
						<select class="sl_form__select" id="00N0a00000CVU7I" name="00N0a00000CVU7I" title="Insurance Type" required>
							<option value="" disabled selected>View Options</option>
							<option value="Employer Provided/Individually Provided">Employer Provided/Individually Provided</option>
							<option value="Government Provided (Medicaid)">Government Provided (Medicaid)</option>
							<option value="Government Provided (Medicare)">Government Provided (Medicare)</option>
							<option value="No Insurance Cash Pay">No Insurance Cash Pay</option>
							<option value="No Insurance, Will Need Financial Assistance">No Insurance, Will Need Financial Assistance</option>
						</select>
					</div><!-- end type of insurance --> 
			   </div><!-- end sl_row for patient state and city--> 

			</div><!-- end sl_inner -->
			
			<div class="sl_vob_form__insurance">
				<div class="sl_inner">
					<h3>Want to speed up the process? <span class="optional">(Optional)</span></h3>
					<p>If you can provide a little more information now, we&#x27;ll go ahead and get to work on finding personalized options based on your situation and coverage.</p> 
		
					<div id="speed-up-fields">
						<div class="sl_vob_form__conditional">
							<input class="sl_form__radio" type="radio" name="speed-up" id="speed-up-yes" checked><label for="speed-up-yes">Yes</label>
							<input class="sl_form__radio" type="radio" name="speed-up" id="speed-up-no"><label for="speed-up-no">No thanks</label>
						</div>

						<div id="additional-fields">
							<div class="sl_row">

					  			<div class="medium-6 sl_cell">
	                              <label for="00N0a00000CVU7F">
	                                  <h4><span class="patient-reveal">Patient</span> Insurance Company <span class="optional">(Optional)</span><span data-tooltip tabindex="1" title="" data-tooltip-class="sl_tooltip sl_tooltip--insurance insurance-company"><img src="/wp-content/mu-plugins/frn_liveagent/public/img/question-circle.png" width="15" height="15" class="form-tool-tip"></span></h4>
	                              </label> 
	                              <input class="sl_form__input" id="00N0a00000CVU7F" maxlength="255" name="00N0a00000CVU7F" type="text">
	                          </div><!-- end sl_cell for the insurance company name-->
	  
	                          <div class="medium-6 sl_cell">
	                              <label for="00N0a00000CVU7G">
	                                  <h4><span class="patient-reveal">Patient</span> Insurance ID Number <span class="optional">(Optional)</span><span data-tooltip tabindex="1" title="" data-tooltip-class="sl_tooltip sl_tooltip--insurance insurance-id"><img src="/wp-content/mu-plugins/frn_liveagent/public/img/question-circle.png" width="15" height="15" class="form-tool-tip"></span> </h4>
	                              </label>
	                              <input class="sl_form__input" id="00N0a00000CVU7G" maxlength="50" name="00N0a00000CVU7G" type="text">
	                            </div><!-- end cell for the insurance ID number-->

	   					</div> <!-- end sl_row -->

	   					<div class="sl_row">

	                          <div class="medium-6 sl_cell">
	                              <label for="00N0a00000CVU7H">
	                                  <h4><span class="patient-reveal">Patient</span> Insurance Phone Number <span class="optional">(Optional)</span><span data-tooltip tabindex="1" title="" data-tooltip-class="sl_tooltip sl_tooltip--insurance insurance-phone"><img src="/wp-content/mu-plugins/frn_liveagent/public/img/question-circle.png" width="15" height="15" class="form-tool-tip"></span></h4>
	                              </label> 
	                              <input class="sl_form__input id="00N0a00000CVU7H" maxlength="50" name="00N0a00000CVU7H" type="text">
	                            </div><!-- end sl_cell -->
		
								<div class="medium-6 sl_cell">
										<label for="00Nj000000BKJjV">
										<h4><span class="patient-reveal">Patient</span> Date of Birth <span class="optional">(Optional)</span></h4>
										</label>
										<span class="dateInput dateOnlyInput">
											<input class="sl_form__input" id="00Nj000000BKJjV" name="00Nj000000BKJjV" type="text" placeholder="MM/DD/YYYY" />
										</span>
							  	</div><!-- end sl_cell for the DOB-->
		
						   	</div><!-- end sl_row --> 
		
							<label for="00Nj000000BKJk6">
								<h4>Tell us about your situation: <span class="optional">(Optional)</span></h4>
							</label>
						 	<textarea class="sl_form__textarea" placeholder="Tell us a little bit about what is going on..." cols="30" rows="6" id="00Nj000000BKJk6" name="00Nj000000BKJk6" type="text"></textarea>
					 	 </div><!-- end #additional fields -->

					</div><!-- end speed up feilds -->

				</div><!-- end sl_inner -->
			</div><!-- end sl_vob_form__insurance -->
	      
	      	<div id="hidden-info">
	          <input id="00N0a00000CRYdN" maxlength="150" name="00N0a00000CRYdN" size="20" type="text" value="Web To Lead Shortcode" class="hide">
	          
				<select id="lead_source" name="lead_source" class="hide">
					<option value="Online" selected>Online</option>
				</select>

				<textarea id="00Nj000000BKJjr" name="00Nj000000BKJjr" rows="1" type="text" class="hide"></textarea>

				<select  id="00Nj000000BKJjt" name="00Nj000000BKJjt" title="Origin" class="hide">
					<option value="Web Form" selected>Web Form</option>
				</select>

			  	<select  id="country_code" name="country_code" class="hide"><option value="US">US</option></select>
	      	</div><!-- end hidden info -->
	      
	        
			<div class="sl_inner">
	        	<div class="g-recaptcha" data-sitekey="6LeXL6cUAAAAAJFVIDFrRycf6GVy7coxu3jreHTV" data-callback="recaptcha_callback"></div>
	        	<input type="submit" class="sl_button sl_button--secure" value="Submit Form" disabled="true" data-ga-event data-ga-category="Online Form" data-ga-action="Form Submission"data-ga-label="VOB Page VOB Submission">   
			</div><!-- end inner -->
		</form><!-- end the form -->
		
	</div><!-- end #vob-holder -->';
};
add_shortcode ('webtolead_form', 'frn_webtolead_form' );
///WEB TO LEAD SHORTCODE
?>