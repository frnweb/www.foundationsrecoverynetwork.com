<?php
/**
 * Plugin Name: FRN Offload Images to S3
 * Description: This plugin installs the code needed to offload images to Amazon S3.  This must be here since FlyWheel blocks direct editing of wp-config.  It is used in conunction with the <a href="/wp-admin/options-general.php?page=amazon-s3-and-cloudfront"/>WP Offload Media</a> plugin.
 * Version: 1.0.0
 * Author: Matt King / Foundations Recovery Network
 **/

require_once( 'offload_images/offload_images.php' );
