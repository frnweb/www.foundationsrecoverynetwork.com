<?php
/**
 * Plugin Name: Locations CPTS Plugin
 * Description: This is the Custom Post Type for FRN research.
 * Author: M.M.
 * License: GPL2
*/
// Custom taxonomy "Type"
function sl_research_cpts_taxonomy() {
 
  $labels = array(
    'name' 						=> _x( 'Types', 'taxonomy general name' ),
    'singular_name' 			=> _x( 'Type', 'taxonomy singular name' ),
    'search_items' 				=>  __( 'Search Types' ),
    'all_items' 				=> __( 'All Types' ),
    'parent_item' 				=> __( 'Parent Type' ),
    'parent_item_colon' 		=> __( 'Parent Type:' ),
    'edit_item' 				=> __( 'Edit Type' ), 
    'update_item' 				=> __( 'Update Type' ),
    'add_new_item' 				=> __( 'Add New Type' ),
    'new_item_name' 			=> __( 'New Type Name' ),
    'menu_name' 				=> __( 'Types' ),
  ); 	
 
  register_taxonomy('research-type', array('sl_research_cpts'), array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'show_admin_column' => true,
	'query_var' => true
  ));
}

add_action( 'init', 'sl_research_cpts_taxonomy', 0 );

// Register Custom Post Type
function sl_research_cpts() {

	$labels = array(
		'name'                  => _x( 'Research', 'Post Type General Name', 'sl_research_cpts' ),
		'singular_name'         => _x( 'Research', 'Post Type Singular Name', 'sl_research_cpts' ),
		'menu_name'             => __( 'Research', 'sl_research_cpts' ),
		'name_admin_bar'        => __( 'Research', 'sl_research_cpts' ),
		'archives'              => __( 'Research Archives', 'sl_research_cpts' ),
		'attributes'            => __( 'Research Attributes', 'sl_research_cpts' ),
		'parent_item_colon'     => __( 'Parent Research:', 'sl_research_cpts' ),
		'all_items'             => __( 'All Research', 'sl_research_cpts' ),
		'add_new_item'          => __( 'Add New Research', 'sl_research_cpts' ),
		'add_new'               => __( 'Add New', 'sl_research_cpts' ),
		'new_item'              => __( 'New Research', 'sl_research_cpts' ),
		'edit_item'             => __( 'Edit Research', 'sl_research_cpts' ),
		'update_item'           => __( 'Update Research', 'sl_research_cpts' ),
		'view_item'             => __( 'View Research', 'sl_research_cpts' ),
		'view_items'            => __( 'View Research', 'sl_research_cpts' ),
		'search_items'          => __( 'Search Research', 'sl_research_cpts' ),
		'not_found'             => __( 'Not found', 'sl_research_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_research_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_research_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_research_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_research_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_research_cpts' ),
		'insert_into_item'      => __( 'Insert into Research', 'sl_research_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Research', 'sl_research_cpts' ),
		'items_list'            => __( 'Research list', 'sl_research_cpts' ),
		'items_list_navigation' => __( 'Research list navigation', 'sl_research_cpts' ),
		'filter_items_list'     => __( 'Filter Research list', 'sl_research_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Research', 'research' ),
		'description'           => __( 'Custom Post Type for FRN Research', 'research' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields'),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'for-professionals/mental-health-white-papers','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-book-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'true',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_research_cpts', $args );

}
add_action( 'init', 'sl_research_cpts', 0 );
?>