<?php
/**
 * Plugin Name: Sample CPTS Plugin
 * Description: This is a sample Custom Post Type.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_POST_TYPE_NAME() {

	$labels = array(
		'name'                  => _x( 'POST TYPE NAME', 'Post Type General Name', 'sl_POST_TYPE_NAME' ),
		'singular_name'         => _x( 'POST TYPE NAME', 'Post Type Singular Name', 'sl_POST_TYPE_NAME' ),
		'menu_name'             => __( 'POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'name_admin_bar'        => __( 'POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'archives'              => __( 'POST TYPE NAME Archives', 'sl_POST_TYPE_NAME' ),
		'attributes'            => __( 'POST TYPE NAME Attributes', 'sl_POST_TYPE_NAME' ),
		'parent_item_colon'     => __( 'Parent POST TYPE NAME:', 'sl_POST_TYPE_NAME' ),
		'all_items'             => __( 'All POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'add_new_item'          => __( 'Add New POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'add_new'               => __( 'Add New', 'sl_POST_TYPE_NAME' ),
		'new_item'              => __( 'New POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'edit_item'             => __( 'Edit POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'update_item'           => __( 'Update POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'view_item'             => __( 'View POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'view_items'            => __( 'View POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'search_items'          => __( 'Search POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'not_found'             => __( 'Not found', 'sl_POST_TYPE_NAME' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_POST_TYPE_NAME' ),
		'featured_image'        => __( 'Featured Image', 'sl_POST_TYPE_NAME' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_POST_TYPE_NAME' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_POST_TYPE_NAME' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_POST_TYPE_NAME' ),
		'insert_into_item'      => __( 'Insert into POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'uploaded_to_this_item' => __( 'Uploaded to this POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'items_list'            => __( 'POST TYPE NAME list', 'sl_POST_TYPE_NAME' ),
		'items_list_navigation' => __( 'POST TYPE NAME list navigation', 'sl_POST_TYPE_NAME' ),
		'filter_items_list'     => __( 'Filter POST TYPE NAME list', 'sl_POST_TYPE_NAME' ),
	);
	$args = array(
		'label'                 => __( 'POST TYPE NAME', 'sl_POST_TYPE_NAME' ),
		'description'           => __( 'SAMPLE Custom Post Type', 'sl_POST_TYPE_NAME' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-id',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_POST_TYPE_NAME', $args );

}
add_action( 'init', 'sl_POST_TYPE_NAME', 0 );
?>