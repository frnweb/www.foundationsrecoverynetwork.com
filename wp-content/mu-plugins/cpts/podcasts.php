<?php
/**
 * Plugin Name: Podcasts CPTS Plugin
 * Description: This is the Custom Post Type for FRN Podcasts.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_podcasts_cpts() {

	$labels = array(
		'name'                  => _x( 'Recovery Unscripted Podcast', 'Post Type General Name', 'sl_podcasts_cpts' ),
		'singular_name'         => _x( 'Podcast', 'Post Type Singular Name', 'sl_podcasts_cpts' ),
		'menu_name'             => __( 'Podcasts', 'sl_podcasts_cpts' ),
		'name_admin_bar'        => __( 'Podcasts', 'sl_podcasts_cpts' ),
		'archives'              => __( 'Recovery Unscripted Podcast', 'sl_podcasts_cpts' ),
		'attributes'            => __( 'Podcast Attributes', 'sl_podcasts_cpts' ),
		'parent_item_colon'     => __( 'Parent Podcast:', 'sl_podcasts_cpts' ),
		'all_items'             => __( 'All Podcasts', 'sl_podcasts_cpts' ),
		'add_new_item'          => __( 'Add New Podcast', 'sl_podcasts_cpts' ),
		'add_new'               => __( 'Add New', 'sl_podcasts_cpts' ),
		'new_item'              => __( 'New Podcast', 'sl_podcasts_cpts' ),
		'edit_item'             => __( 'Edit Podcast', 'sl_podcasts_cpts' ),
		'update_item'           => __( 'Update Podcast', 'sl_podcasts_cpts' ),
		'view_item'             => __( 'View Podcast', 'sl_podcasts_cpts' ),
		'view_items'            => __( 'View Podcasts', 'sl_podcasts_cpts' ),
		'search_items'          => __( 'Search Podcasts', 'sl_podcasts_cpts' ),
		'not_found'             => __( 'Not found', 'sl_podcasts_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_podcasts_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_podcasts_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_podcasts_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_podcasts_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_podcasts_cpts' ),
		'insert_into_item'      => __( 'Insert into Podcasts', 'sl_podcasts_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Podcasts', 'sl_podcasts_cpts' ),
		'items_list'            => __( 'Podcasts list', 'sl_podcasts_cpts' ),
		'items_list_navigation' => __( 'Podcasts list navigation', 'sl_podcasts_cpts' ),
		'filter_items_list'     => __( 'Filter Podcasts list', 'sl_podcasts_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Podcast', 'sl_podcasts_cpts' ),
		'description'           => __( 'Custom Post Type for FRN Podcasts', 'sl_podcasts_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'recovery-unscripted-podcast','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-microphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_podcasts_cpts', $args );

}
add_action( 'init', 'sl_podcasts_cpts', 0 );

// Custom taxonomy "Type"
function sl_podcasts_cpts_taxonomy() {
 
	$labels = array(
	  'name' 						=> _x( 'Categories', 'taxonomy general name' ),
	  'singular_name' 				=> _x( 'Category', 'taxonomy singular name' ),
	  'search_items' 				=>  __( 'Search Categories' ),
	  'all_items' 					=> __( 'All Categories' ),
	  'parent_item' 				=> __( 'Parent Category' ),
	  'parent_item_colon' 			=> __( 'Parent Category:' ),
	  'edit_item' 					=> __( 'Edit Category' ), 
	  'update_item' 				=> __( 'Update Category' ),
	  'add_new_item' 				=> __( 'Add New Category' ),
	  'new_item_name' 				=> __( 'New Category Name' ),
	  'menu_name' 					=> __( 'Categories' ),
	); 	
   
	register_taxonomy('podcast-category', array('sl_podcasts_cpts'), array(
	  'hierarchical' => true,
	  'labels' => $labels,
	  'show_ui' => true,
	  'show_admin_column' => true,
	  'query_var' => true
	));
  }
  
  add_action( 'init', 'sl_podcasts_cpts_taxonomy', 0 );