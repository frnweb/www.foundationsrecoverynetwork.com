<?php
/**
 * Plugin Name: Locations CPTS Plugin
 * Description: This is the Custom Post Type for FRN locations.
 * Author: M.M.
 * License: GPL2
*/
// Custom taxonomy "Type"
function sl_locations_cpts_taxonomy() {
 
  $labels = array(
    'name' 						=> _x( 'Types', 'taxonomy general name' ),
    'singular_name' 			=> _x( 'Type', 'taxonomy singular name' ),
    'search_items' 				=>  __( 'Search Types' ),
    'all_items' 				=> __( 'All Types' ),
    'parent_item' 				=> __( 'Parent Type' ),
    'parent_item_colon' 		=> __( 'Parent Type:' ),
    'edit_item' 				=> __( 'Edit Type' ), 
    'update_item' 				=> __( 'Update Type' ),
    'add_new_item' 				=> __( 'Add New Type' ),
    'new_item_name' 			=> __( 'New Type Name' ),
    'menu_name' 				=> __( 'Types' ),
  ); 	
 
  register_taxonomy('location-category', array('sl_locations_cpts'), array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'show_admin_column' => true,
	'query_var' => true,
  ));
}

add_action( 'init', 'sl_locations_cpts_taxonomy', 0 );

// Register Custom Post Type
function sl_locations_cpts() {

	$labels = array(
		'name'                  => _x( 'Locations', 'Post Type General Name', 'sl_locations_cpts' ),
		'singular_name'         => _x( 'Location', 'Post Type Singular Name', 'sl_locations_cpts' ),
		'menu_name'             => __( 'Locations', 'sl_locations_cpts' ),
		'name_admin_bar'        => __( 'Locations', 'sl_locations_cpts' ),
		'archives'              => __( 'Location Archives', 'sl_locations_cpts' ),
		'attributes'            => __( 'Location Attributes', 'sl_locations_cpts' ),
		'parent_item_colon'     => __( 'Parent Location:', 'sl_locations_cpts' ),
		'all_items'             => __( 'All Locations', 'sl_locations_cpts' ),
		'add_new_item'          => __( 'Add New Location', 'sl_locations_cpts' ),
		'add_new'               => __( 'Add New', 'sl_locations_cpts' ),
		'new_item'              => __( 'New Location', 'sl_locations_cpts' ),
		'edit_item'             => __( 'Edit Location', 'sl_locations_cpts' ),
		'update_item'           => __( 'Update Location', 'sl_locations_cpts' ),
		'view_item'             => __( 'View Location', 'sl_locations_cpts' ),
		'view_items'            => __( 'View Locations', 'sl_locations_cpts' ),
		'search_items'          => __( 'Search Locations', 'sl_locations_cpts' ),
		'not_found'             => __( 'Not found', 'sl_locations_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_locations_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_locations_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_locations_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_locations_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_locations_cpts' ),
		'insert_into_item'      => __( 'Insert into Locations', 'sl_locations_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Locations', 'sl_locations_cpts' ),
		'items_list'            => __( 'Locations list', 'sl_locations_cpts' ),
		'items_list_navigation' => __( 'Locations list navigation', 'sl_locations_cpts' ),
		'filter_items_list'     => __( 'Filter Locations list', 'sl_locations_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Locations', 'locations' ),
		'description'           => __( 'Custom Post Type for FRN locations', 'locations' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields'),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'locations','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-location',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_locations_cpts', $args );

}
add_action( 'init', 'sl_locations_cpts', 0 );

?>