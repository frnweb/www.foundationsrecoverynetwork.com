<?php
// THUMBNAIL CARD
	function sl_thumbnail_card ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> '',
			'img'		=> ''
			), $atts );
		$content = wpautop(trim($content));
		return '<div class="sl_card sl_card--thumbnail ' . esc_attr($specs['class'] ) . '"><div class="sl_row"><div class="sl_cell sl_card__image" style="background-image: url('. esc_attr($specs['img'] ) .'"></div><div class="sl_cell auto sl_card__content">' . do_shortcode ( $content ) . '</div></div></div>';
	}
	add_shortcode ('thumbnail-card', 'sl_thumbnail_card' );
///THUMBNAIL CARD
?>