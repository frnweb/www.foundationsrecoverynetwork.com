<?php
// CONTAINER
	function sl_content_locker ( $atts, $content = null ) {
	    $specs = shortcode_atts( array(
	        'heading'     => 'Subscribe to Unlock Content',
			), $atts );
		$content = wpautop(trim($content));
		$emailform = '<div id="mc_embed_signup">
				<form action="https://foundationsrecoverynetwork.us5.list-manage.com/subscribe/post?u=a517fa2b2970f6fb110a5a1dc&amp;id=3675106f6e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" novalidate><div class="sl_form sl_form--email" id="mc_embed_signup_scroll"><input type="email" value="" name="EMAIL" class="sl_input sl_form__input" id="mce-EMAIL" placeholder="email address" required><div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a517fa2b2970f6fb110a5a1dc_3675106f6e" tabindex="-1" value=""></div><div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="sl_button sl_button--small"></div></div></form></div><script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"></script><script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]="EMAIL";ftypes[0]="email";fnames[1]="FNAME";ftypes[1]="text";fnames[2]="LNAME";ftypes[2]="text";fnames[3]="ADDRESS";ftypes[3]="address";fnames[4]="PHONE";ftypes[4]="phone";fnames[5]="BIRTHDAY";ftypes[5]="birthday";fnames[6]="MMERGE6";ftypes[6]="text";fnames[7]="MMERGE7";ftypes[7]="text";fnames[8]="MMERGE8";ftypes[8]="text";fnames[9]="MMERGE9";ftypes[9]="text";fnames[10]="MMERGE10";ftypes[10]="text";fnames[11]="MMERGE11";ftypes[11]="text";fnames[12]="MMERGE12";ftypes[12]="text";}(jQuery));var $mcj = jQuery.noConflict(true);</script>';

	    return '<div class="sl_content-locker"><div class="sl_content-locker__form"><div class="sl_card sl_card--content-locker"><img src="' . get_site_url()  .'/wp-content/themes/slate/dist/img/icon/lock-icon.svg"><h3>' . esc_attr($specs['heading']) . '</h3>' . $emailform .'</div></div><div class="sl_content-locker__content">' . do_shortcode ( $content ) . '</div></div>';
		}

	add_shortcode ('content-locker', 'sl_content_locker' );
///CONTAINER
?>


