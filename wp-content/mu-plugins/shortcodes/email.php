<?php
// EMAIL
	function sl_email() {

		return '<div class="sl_card sl_card--email">
		            <h3 class="sl_email_header">Subscribe to our email list</h3>
		            <form class="sl_form sl_form--email" action="http://cloud.frn-mail.com/subscribe" method="GET">
                        <input placeholder="Email Address" id="sl_input" class="sl_form__input" type="text" name="email">
                        <button type="submit" value="Sign Me Up" class="sl_button sl_button--small">Sign Me Up</button>
                    </form>
	            </div>';

	}
	add_shortcode( 'email', 'sl_email' );
///EMAIL
?>