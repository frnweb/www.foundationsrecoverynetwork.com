<?php
// BLOCKGRID
	function sl_blockgrid ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'small'		=> '1',
			'medium'	=> '4',
			'large'		=> '4',
			), $atts );
		return '<div class="sl_row small-up-' . esc_attr($specs['small']) . ' medium-up-' . esc_attr($specs['medium']) .' large-up-' . esc_attr($specs['large']) . '">' . do_shortcode ( $content ) . '</div>';
	}

	add_shortcode ('blockgrid', 'sl_blockgrid' );
///BLOCKGRID

// BLOCKGRID ITEM
	function sl_blockgrid_item ( $atts, $content = null ) {
			return '<div class="sl_cell">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('item', 'sl_blockgrid_item' );
///BLOCKGRID ITEM
?>