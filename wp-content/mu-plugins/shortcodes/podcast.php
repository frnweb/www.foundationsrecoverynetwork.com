<?php
// Shortcode Embed Iframe
	function sl_podcast_embed( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'data' => ''
        ), $atts );
		return '<div class="sl_podcast__player">
                    <iframe style="border: none" src="'. esc_attr($specs['data'] ) . '/height/90/theme/custom/thumbnail/no/direction/backward/render-playlist/no/custom-color/ff3b3b/" height="90" width="100%" scrolling="no"  allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
                </div>';
	}
	add_shortcode ('podcast_embed', 'sl_podcast_embed' );
///Shortcode Embed
?>