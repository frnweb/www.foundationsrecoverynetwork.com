<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;



// Footer options
	$option = new FieldsBuilder('options');
	$option
	    ->addFields(get_field_partial('partials.global-options'));
	$option 
	    ->addFields(get_field_partial('partials.header-options'));
	$option
	    ->addFields(get_field_partial('partials.footer-options'));
	$option
	    ->addFields(get_field_partial('partials.articles-options'));
	$option
	    ->setLocation('options_page', '==', ['theme-general-settings', 'theme-footer-settings', 'theme-header-settings']);

	return $option;