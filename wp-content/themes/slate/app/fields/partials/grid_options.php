<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$grid = new FieldsBuilder('grid');

$grid
	->addTrueFalse('grid_settings', [
		'label' => 'Grid Settings',
		'wrapper' => ['width' => 10]
	])

		->addSelect('grid_type', [
			'label' => 'Grid Type',
			'wrapper' => ['width' => 90]
		])
		->setInstructions('Method for sizing cells')
		->conditional('grid_settings', '==', 1 )
	  	->addChoices(
		  ['block' => 'Block Grid'],
		  ['cell' => 'Cell Size']
		)

			//Large Block
			->addSelect('block_size', [
				'label' => 'Number of Cells (Large)',
				'wrapper' => ['width' => 50]
			])
		  	->addChoices(
			  ['1' => '1'],
			  ['2' => '2'],
			  ['3' => '3'],
			  ['4' => '4'],
			  ['5' => '5'],
			  ['6' => '6']
			)
			->setInstructions('Choose the number of columns you want in a row')
			->conditional('grid_type', '==', 'block' )
			//Medium Block
			->addSelect('block_size_med', [
				'label' => 'Number of Cells (Medium)',
				'wrapper' => ['width' => 50]
			])
		  	->addChoices(
			  ['1' => '1'],
			  ['2' => '2'],
			  ['3' => '3'],
			  ['4' => '4'],
			  ['5' => '5'],
			  ['6' => '6']
			)
			->setDefaultValue('1')
			->setInstructions('Choose the number of columns you want in a row')
			->conditional('grid_type', '==', 'block' )

			//Cell Large
			->addSelect('cell_size', [
				'label' => 'Width of Cell (Large)',
				'wrapper' => ['width' => 50]
			])
		  	->addChoices(
			  ['2' => '2'],
			  ['3' => '3'],
			  ['4' => '4'],
			  ['5' => '5'],
			  ['6' => '6'],
			  ['7' => '7'],
			  ['8' => '8'],
			  ['9' => '9'],
			  ['10' => '10'],
			  ['11' => '11'],
			  ['12' => '12']
			)
			->setDefaultValue('6')
			->setInstructions('Choose the width of each cell based on a 12 column grid. For duos, just choose the width of the media and the content will adjust.')
			->conditional('grid_type', '==', 'cell' )
			//Cell Medium
			->addSelect('cell_size_med', [
				'label' => 'Width of Cell (Medium)',
				'wrapper' => ['width' => 50]
			])
		  	->addChoices(
			  ['2' => '2'],
			  ['3' => '3'],
			  ['4' => '4'],
			  ['5' => '5'],
			  ['6' => '6'],
			  ['7' => '7'],
			  ['8' => '8'],
			  ['9' => '9'],
			  ['10' => '10'],
			  ['11' => '11'],
			  ['12' => '12']
			)
			->setDefaultValue('12')
			->setInstructions('Choose the width of each cell based on a 12 column grid. For duos, just choose the width of the media and the content will adjust.')
			->conditional('grid_type', '==', 'cell' );

return $grid;