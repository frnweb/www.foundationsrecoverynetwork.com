<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

$research = new FieldsBuilder('research');

$research
    ->setLocation('post_type', '==', 'sl_research_cpts');
  
$research
->addUrl('download_file', [
    'Download PDF']);

return $research;