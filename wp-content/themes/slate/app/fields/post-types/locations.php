<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

//Fields for Locations Post Type
$locations = new FieldsBuilder('locations');

$locations
    ->setLocation('post_type', '==', 'sl_locations_cpts');
  
$locations

	->addText('facility_name', [
		'label' => 'Facility Name'
	])

	->addWysiwyg('facility_features', [
		'label' => 'Facility Features'
	])

	->addUrl('website_url', [
		'label' => 'Website URL'
	])

	->addUrl('job_board_url', [
		'label' => 'Job Board URL'
	])

	->addUrl('direction_url', [
		'label' => 'Google Maps URL'
	])

	->addImage('logo_field', [
		'label' => 'Logo'])

	->addImage('image_field', [
		'label' => 'Image'
	])

	->addText('facility_lat', [
		'label' => 'Facility Latitude'
	])

	->addText('facility_lng', [
		'label' => 'Facility Longitude'
	])

	//Facility Phone
	->addText('facility_phone', [
        'label' => 'Facility Phone Shortcode'
    ])

	->addText('street_address', [
	    'label' => 'Steet Address'
	])
	->setInstructions('The street address for the facility. This will be used throughout the Site.')

	->addText('city', [
	    'label' => 'City',
	    'ui' => $config->ui,
	    'wrapper' => $config->wrapper
	])
	->setInstructions('Put the City i.e. Nashville')

	->addText('state', [
		'label' => 'State',
		'ui' => $config->ui,
		'wrapper' => $config->wrapper
	])
	->setInstructions('Put the State here i.e. TN')

	->addText('zip_code', [
	    'label' => 'Zip Code',
	    'ui' => $config->ui,
	    'wrapper' => $config->wrapper
	])
	->setInstructions('The Zip Code of the facility')

	->addCheckbox('accreditations', [
		'layout' => 'vertical',
		'toggle' => 1,
		'wrapper' => ['width' => 80]
	])
	->addChoices(
		['naatp' => 'NAATP'],
		['carf' => 'CARF'],
		['joint_commission' => 'Joint Commission'],
		['legitscript' => 'LegitScript']
	);

//Adding Modules to Post Type
$modules = new FieldsBuilder('Modules');

$modules
  ->setLocation('post_type', '==', 'sl_locations_cpts');
  
$modules
  ->addFields(get_field_partial('partials.builder'));


// Footer Options Tabs & Fields
$locationsfields = new FieldsBuilder('locations_fields');

	$locationsfields
		->setLocation('post_type', '==', 'sl_locations_cpts')
		// Social Icons Tab
		->addTab('Facility Info', ['placement' => 'top'])
			->addFields($locations)			
		// Accrediations Tab
		->addTab('Page Layout', ['placement' => 'top'])
			->addFields($modules);

	return $locationsfields;