<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$email = new FieldsBuilder('email');

$email
    ->addFields(get_field_partial('partials.module-settings'))

    ->addTab('content', ['placement' => 'left'])
    ->addText('head_text', [
        'label' => 'Heading'
    ]);
    
return $email;