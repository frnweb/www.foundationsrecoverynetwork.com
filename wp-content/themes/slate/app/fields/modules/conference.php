<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$conference = new FieldsBuilder('conference');

$conference
	->addFields(get_field_partial('partials.module-settings'));

$conference
    ->addTab('content', ['placement' => 'left'])
    
    ->addText('head_text', [
        'label' => 'Heading'
    ])

    ->addTextArea('paragraph', [
        'label' => 'Paragraph Text'
    ])

 	//Image 
    ->addImage('event_image', ['wrapper' => ['width' => 50]])

    //Video
    ->addText('video_text', [
        'wrapper' => ['width' => 50]
    ])

    ->addWysiwyg('video', [
        'label' => 'Video(iframe)',
    ]);

return $conference;