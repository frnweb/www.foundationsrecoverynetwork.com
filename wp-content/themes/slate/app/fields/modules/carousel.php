<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$carousel = new FieldsBuilder('carousel');

$carousel
	->addFields(get_field_partial('partials.module-settings'));

$carousel
	->addTab('carousel_type', ['placement' => 'left'])
	->addSelect('type_select', [
		'label' => 'Carousel Type',
		'wrapper' => ['width' => 20]
	])
  	->addChoices(
  	['image_card' => 'Image/Card'],
  	['duo' => 'Duo']
)

	->addTab('content', ['placement' => 'left'])

    // Repeater
    ->addRepeater('carousel', [
	    'min' => 1,
	    'max' => 7,
	    'button_label' => 'Add Card/Image',
	    'layout' => 'block',
		])
	//Image 
	->addImage('carousel_image')

	//Card
	->addFields(get_field_partial('modules.card'));

return $carousel;