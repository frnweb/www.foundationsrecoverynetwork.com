<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$card = new FieldsBuilder('card');

$card
	->addGroup('card')

		//PreHeader
		->addTrueFalse('check_box', [
			'label' => 'Pre-Header',
			'wrapper' => ['width' => 30]
		])
		->setInstructions('Optional pre-header for the card')
		
		->addText('pre_header', [
			'label' => 'Pre-header',
			'wrapper' => ['width' => 70]
		])
		->conditional('check_box', '==', 1 )

		// Header
		->addText('header', [
			'label' => 'Header',
			'ui' => $config->ui
		])
		->setInstructions('Header for the card')

		// WYSIWYG
		->addWysiwyg('paragraph', [
			'label' => 'WYSIWYG',
			'ui' => $config->ui
		])
		->setInstructions('Paragraph for the card')
		
		//Button
		->addFields(get_field_partial('modules.button'))
		  
  ->endGroup();

return $card;