<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$webinar = new FieldsBuilder('webinar');

$webinar
	->addFields(get_field_partial('partials.module-settings'));

$webinar
    ->addTab('content', ['placement' => 'left'])
    
    ->addText('head_text', [
        'label' => 'Heading'
    ])

    ->addText('wysiwyg', [
        'label' => 'Paragraph Text'
    ]);
    

return $webinar;