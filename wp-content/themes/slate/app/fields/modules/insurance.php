<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$insurance = new FieldsBuilder('insurance');

$insurance
	->addFields(get_field_partial('partials.module-settings'));

$insurance
	// Type Selector
	->addTab('Type Selecter', ['placement' => 'left'])
		// Select
		->addSelect('type_select', [
			'label' => 'Module Type',
			'wrapper' => ['width' => 30]
		])
		
			->addChoices(
				['duo' => 'Duo'],
				['deck' => 'Deck']
		)
		
	->addTab('content', ['placement' => 'left'])

  	->addSelect('row', [
  		'label' => 'Row Count',
  		'wrapper' => ['width' => 25]
	])
	->setInstructions('# of icons on a row')

    ->addChoices(
        ['1' => '1'],
        ['2' => '2'],
        ['3' => '3'],
        ['4' => '4'],
        ['5' => '5'],
        ['6' => '6']
	)
	 
	->addCheckbox('insurance', [
			'layout' => 'vertical',
			'toggle' => 1,
			'wrapper' => ['width' => 75]
	])
		->addChoices(
			['4yourchoice' => '4YourChoice'],
			['bluecross' => 'BlueCross'],
			['beaconhealth' => 'BeaconHealth'],
			['healthnet' => 'HealthNet'],
			['magellan' => 'Magellan'],
			['compsych' => 'ComPsych'],
			['coresource' => 'CoreSource'],
			['coventry' => 'Coventry'],
			['cigna' => 'Cigna'],
			['aetna' => 'Aetna'],
			['unitedhealthcare' => 'UnitedHealthcare'],
      		['humana' => 'Humana'],
      		['valueoptions' => 'ValueOptions'],
      		['priorityhealth' => 'PriorityHealth'],
      		['tricare' => 'Tricare']

	)
	
	//Button
	->addFields(get_field_partial('modules.button'));
    
return $insurance;