<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$articles_podcasts = new FieldsBuilder('articles_podcasts');

$articles_podcasts
	->addFields(get_field_partial('partials.module-settings'));

$articles_podcasts
    ->addTab('content', ['placement' => 'left'])

		// Post Relationship Field
		->addRelationship('article', [
	      'label' => 'Article Picker',
          'post_type' => 'post',
	      'ui' => $config->ui,
	      'wrapper' => ['width' => 80]
        ])

        // Post Relationship Field
		->addRelationship('podcast', [
            'label' => 'Podcast Picker',
            'post_type' => 'sl_podcasts_cpts',
            'ui' => $config->ui,
            'wrapper' => ['width' => 80]
        ]);
      
return $articles_podcasts;