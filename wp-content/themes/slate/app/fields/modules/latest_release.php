<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$latest_release = new FieldsBuilder('latest_release');

$latest_release
		->addSelect('type_select', [
			'label' => 'Latest Type',
			'ui' => $config->ui,
		])
	  	->addChoices(
		  	['press_release' => 'Press Release'],
        	['webinar' => 'Webinar']
      );
	  	
return $latest_release;