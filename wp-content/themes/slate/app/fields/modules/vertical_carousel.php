<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$vert_carousel = new FieldsBuilder('vertical_carousel');

$vert_carousel
	->addFields(get_field_partial('partials.module-settings'));

$vert_carousel
    ->addTab('card_content', ['placement' => 'left'])
		//Card
        ->addFields(get_field_partial('modules.card'))
        
    ->addTab('carousel_content', ['placement' => 'left'])

    	//Image 
        ->addImage('carousel_image', ['wrapper' => ['width' => 30]])

	    // Repeater
	    ->addRepeater('carousel', [
		    'min' => 1,
		    'max' => 7,
		    'button_label' => 'Add Item',
		    'layout' => 'block',
			])
			
	        //Carousel Text
	        ->addWysiwyg('carousel_text', ['wrapper' => ['width' => 70]]);

return $vert_carousel;