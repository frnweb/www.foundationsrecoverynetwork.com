<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 50],
];

$modal = new FieldsBuilder('modal');

$modal
	->addFields(get_field_partial('partials.module-settings'));


$modal
	->addTab('content', ['placement' => 'left'])
		->addText('modal_id', [
			'wrapper' => ['width' => 40]
		])
			->setInstructions('Modal id, use this to target the modal from a button')
    
		// Modal Type Select
    ->addSelect('type_select', [
    	'wrapper' => ['width' => 60]
    ])
    	->setInstructions('Controls the type of content inside the modal')
    	->addChoices('Content', 'Video', 'Gallery')
    
    // Content
    ->addWysiwyg('content')
      ->conditional('type_select', '==', 'Content')

    // Video
    ->addWysiwyg('video', ['label' => 'Video(iframe)','ui' => $config->ui])
        ->conditional('type_select', '==', 'Video')

    // Gallery
    ->addRepeater('Gallery', [
	    'min' => 1,
	    'max' => 7,
	    'button_label' => 'Add Card/Image',
	    'layout' => 'block',
		])
	//Image 
	->addImage('gallery_image')
		
	//Card
	->addFields(get_field_partial('modules.card'))
    ->conditional('type_select', '==', 'Gallery');


        
return $modal;