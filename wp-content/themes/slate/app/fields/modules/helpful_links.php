<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$helpful_links = new FieldsBuilder('helpful_links');

$helpful_links
	->addFields(get_field_partial('partials.module-settings'));

$helpful_links

    // Type Selector
    ->addTab('Type Selecter', ['placement' => 'left'])
        // Select
        ->addSelect('type_select', [
            'label' => 'Module Type',
        ])
        ->setInstructions('Select the type of content you want to display next to the helpful links')
        
            ->addChoices(
                ['articles' => 'Articles'],
                ['deck' => 'Deck']
        )

    ->addTab('Module Header', ['placement' => 'left'])
        // Header
        ->addText('header', [
            'label' => 'Header',
            'ui' => $config->ui
        ])

    ->addTab('Helpful Links', ['placement' => 'left'])

        // Header
        ->addText('helpful_header', [
            'label' => 'Helpful Links Header',
        ])
    
		//Repeater
		->addRepeater('helpful_link', [
            'min' => 1,
            'max' => 10,
            'button_label' => 'Add Helpful Links',
            'layout' => 'block',
        ])
        ->addImage('image_field', [
            'label' => 'Image Field'
        ])
        ->addText('head_text', [
            'label' => 'Heading Text'
        ])
        ->addText('sub_text', [
            'label' => 'Sub Text'
        ])
        ->addLink('anchor_link', [
            'label' => 'Link'
        ])

        ->endRepeater()

    //ARTICLE TYPE
    ->addTab('Article Picker', ['placement' => 'left'])
    ->conditional('type_select', '==', 'articles' )
        //Article Header
        ->addText('article_header', [
            'label' => 'Heading',
            'ui' => $config->ui,
        ])

		// Article Picker
		->addRelationship('article', [
	      'label' => 'Article Picker',
          'post_type' => 'post',
          'min' => 4,
          'max' => 4,
	      'ui' => $config->ui,
        ])

    //DECK TYPE
    ->addTab('Deck', ['placement' => 'left'])
    ->conditional('type_select', '==', 'deck' )

    //Repeater
        ->addRepeater('deck', [
          'min' => 1,
          'max' => 10,
          'button_label' => 'Add Card',
          'layout' => 'block',
        ])
        ->addTrueFalse('link_wrapper', [
            'wrapper' => ['width' => 15]
        ])
        ->addLink('link_url', [
            'wrapper' => ['width' => 85]
        ])
        ->conditional('link_wrapper', '==', 1)

        //Image 
        ->addImage('deck_image')  
        // Card
        ->addFields(get_field_partial('modules.card'));
      
return $helpful_links;