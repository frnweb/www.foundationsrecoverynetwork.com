<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$facility_tour = new FieldsBuilder('facility_tour');

$facility_tour
	->addFields(get_field_partial('partials.module-settings'));

$facility_tour
    ->addTab('Facility Map', ['placement' => 'left'])
 		//Google Maps
		->addGoogleMap('google_map', [
			'label' => 'Facility Location'
		])
  
    ->addTab('content', ['placement' => 'left'])
		
		//Social Icons
		->addGroup('social_schema', [
			'label' => 'Social Icons'
		])
		->addTrueFalse('social_box', [
			'label' => 'Add Icons',
			'wrapper' => ['width' => 10]
		])
		->addCheckbox('social_urls', [
			'layout' => 'vertical',
			'toggle' => 1,
			'wrapper' => ['width' => 90]
		])
		->addChoices(
			['facebook' => 'Facebook'],
			['linkedin' => 'LinkedIn'],
			['twitter' => 'Twitter']
		)
        ->conditional('social_box', '==', 1 )
                	  
        ->addUrl('facebook_url')
            ->conditional('social_urls', '==', 'facebook')
          
        ->addUrl('linkedin_url')
            ->conditional('social_urls', '==', 'linkedin')
          
        ->addUrl('twitter_url')
            ->conditional('social_urls', '==', 'twitter')

        ->endGroup()

    ->addTab('Carousel Content', ['placement' => 'left'])
        ->addUrl('tour_url', [
			'label' => 'Tour URL'
		])
        // Repeater
        ->addRepeater('carousel', [
            'min' => 1,
            'max' => 7,
            'button_label' => 'Add Card/Image',
            'layout' => 'block',
            ])
        //Image 
        ->addImage('carousel_image');

return $facility_tour;