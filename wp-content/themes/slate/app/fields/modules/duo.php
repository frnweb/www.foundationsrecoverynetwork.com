<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$duo = new FieldsBuilder('duo');

$duo
	->addFields(get_field_partial('partials.module-settings'));

$duo
	->addTab('media_type', ['placement' => 'left'])
		->addSelect('media', [
			'label' => 'Media Type',
			'wrapper' => ['width' => 20]
		])
	  	->addChoices(
		  ['image' => 'Image'],
		  ['video' => 'Video']
		)

 		//Image 
		->addImage('duo_image', ['wrapper' => ['width' => 80]])

		//Video
	 	->addText('video_text', [
	 		'wrapper' => ['width' => 50]
	 	])
	 	->conditional('media', '==', 'video' )
        ->addWysiwyg('video_embed', [
            'label' => 'Video(iframe)',
        ])
		->conditional('media', '==', 'video' )

  
  	->addTab('content', ['placement' => 'left'])
		// Card
		->addFields(get_field_partial('modules.card'))
		
		// Accreditations
		->addGroup('accreditation_schema', [
			'label' => 'Accreditations'
		])
			->addTrueFalse('accreditation_box', [
					'label' => 'Add Logos',
					'wrapper' => ['width' => 10]
				])
				->addCheckbox('accreditations', [
					'layout' => 'vertical',
					'toggle' => 1,
					'wrapper' => ['width' => 90]
				])
				->addChoices(
					['naatp' => 'NAATP'],
					['carf' => 'CARF'],
					['joint_commission' => 'Joint Commission'],
					['legitscript' => 'LegitScript']
				)
				->conditional('accreditation_box', '==', 1 );	  
    

return $duo;