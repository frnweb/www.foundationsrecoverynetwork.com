<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$facility_duo = new FieldsBuilder('facility_duo');

$facility_duo
	->addFields(get_field_partial('partials.module-settings'));

$facility_duo
  	->addTab('content', ['placement' => 'left'])
		// Paragraph
		->addTextArea('paragraph', [
	        'label' => 'Paragraph Text'
	    ])
			->setInstructions('Paragraph for the duo. This text will automatically get wrapped in a p tag')
    
		// Phone CTA
		->addText('cta_text', [
			'label' => 'Phone CTA Text',
			'ui' => $config->ui
		]);

$facility_duo
  	->addTab('media', ['placement' => 'left'])
  		//Video Modal
		->addTrueFalse('add_video', [
			'label' => 'Add Video Modal',
			'wrapper' => ['width' => 30]
		])

			// Video
	    	->addWysiwyg('video', [
	    		'label' => 'Video(iframe)',
	    		'wrapper' => ['width' => 70]
	    	])
    	->conditional('add_video', '==', 1 )

    	//Video CTA
		->addTrueFalse('add_cta', [
			'label' => 'Change Video CTA',
			'wrapper' => ['width' => 30]
		])
		->setInstructions('The default text is "Discover *facility-name*" ')

			// Video
	    	->addText('video_cta', [
	    		'label' => 'Video CTA',
	    		'wrapper' => ['width' => 70]
	    	])
    	->conditional('add_cta', '==', 1 );

return $facility_duo;