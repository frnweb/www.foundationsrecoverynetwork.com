<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$similar_locations = new FieldsBuilder('similar_locations');

$similar_locations
	->addFields(get_field_partial('partials.module-settings'));

$similar_locations
    ->addTab('content', ['placement' => 'left'])
    
    // Post Relationship Field
	->addRelationship('locations', [
	    'label' => 'Similar Treatment Options',
        'post_type' => 'sl_locations_cpts',
        'min' => 1,
		'max' => 3,
	    'ui' => $config->ui,
    ]);
      
return $similar_locations;