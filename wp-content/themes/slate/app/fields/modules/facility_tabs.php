<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 50],
];

$facility_tabs = new FieldsBuilder('facility_tabs');

$facility_tabs
    ->addFields(get_field_partial('partials.module-settings'));


$facility_tabs
    ->addTab('content', ['placement' => 'left'])

    //Repeater
    ->addRepeater('tabs', [
      'min' => 1,
      'max' => 10,
      'button_label' => 'Add Tabs',
      'layout' => 'block',
    ])

        // Tab Title
        ->addText('tab_title')

        ->addFlexibleContent('tab_content', ['button_label' => 'Add Pieces'])
            // WYSIWYG
            ->addLayout('wysiwyg')
                ->addWysiwyg('wysiwyg', [
                    'label' => 'Wysiwyg',
                ])
            //Button
            ->addLayout('button')
                ->addFields(get_field_partial('modules.button'))
            //Staff Deck
            ->addLayout('staff_deck')
                ->addRelationship('staff_deck', [
                    'label' => 'Staff Members',
                    'post_type' => 'sl_staff_cpts',
                    'min' => 1,
                ])
                ->setInstructions('Choose multiple staff members to highlight')
            //Staff Spotlight
            ->addLayout('staff_spotlight')
                ->addRelationship('staff_spotlight', [
                    'label' => 'Staff Member',
                    'post_type' => 'sl_staff_cpts',
                    'min' => 1,
                    'max' => 1,
                ])
                ->setInstructions('Choose one staff member to highlight');

return $facility_tabs;
      	