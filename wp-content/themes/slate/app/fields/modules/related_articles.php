<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$related_articles = new FieldsBuilder('related_articles');

$related_articles
	->addFields(get_field_partial('partials.module-settings'));

$related_articles
    ->addTab('content', ['placement' => 'left'])
    
    // Post Relationship Field
	->addRelationship('article', [
	    'label' => 'Article Picker',
        'post_type' => 'post',
        'min' => 5,
		'max' => 5,
	    'ui' => $config->ui,
	    'wrapper' => ['width' => 80]
    ]);
      
return $related_articles;