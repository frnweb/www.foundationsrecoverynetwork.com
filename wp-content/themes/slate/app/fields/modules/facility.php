<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$facility = new FieldsBuilder('facility');

$facility
	->addFields(get_field_partial('partials.module-settings'));

$facility
	->addTab('content', ['placement' => 'left'])

	// Header
	->addText('header', [
			'label' => 'Header',
			'ui' => $config->ui
		])
	
	// Type Select
	->addSelect('type_select', [
		'label' => 'Facility Display Select',
		'ui' => $config->ui,
		'wrapper' => ['width' => 20]
	])
		->addChoices(
			'deck',
			'carousel',
			'careers',
			'contact-deck', 
			'residential',
			'outpatient'
		)
	// Add Fields for FRN Corporate for Career Module Type
	->addGroup('foundations_corporate')
		->conditional('type_select', '==', 'careers' )
		->addImage('image_field', [
			'label' => 'Image'
		])
		->addText('facility_name', [
			'label' => 'Facility Name',
			'wrapper' => ['width' => 50]
		])
		->addUrl('job_board_url', [
			'label' => 'Job Board URL',
			'wrapper' => ['width' => 50]
		])
		->addText('street_address', [
		    'label' => 'Steet Address',
		    'wrapper' => ['width' => 50]
		])
		->addText('city', [
		    'label' => 'City',
		    'wrapper' => ['width' => 20]
		])
		->addText('state', [
			'label' => 'State',
			'wrapper' => ['width' => 10]
		])
		->addText('zip_code', [
		    'label' => 'Zip Code',
		    'wrapper' => ['width' => 20]
		])
	->endGroup()

	// Staff Relationship Field
	->addRelationship('locations', [
      'label' => 'Facility',
      'post_type' => 'sl_locations_cpts',
      'ui' => $config->ui,
      'wrapper' => ['width' => 80]
  ])
	->conditional('type_select', '==', 'carousel' )
		->or('type_select', '==', 'deck' )
		->or('type_select', '==', 'contact-deck' );

return $facility;