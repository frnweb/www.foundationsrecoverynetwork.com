<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],

];

$billboard = new FieldsBuilder('billboard');

$billboard
	->addFields(get_field_partial('partials.module-settings'));


$billboard
	
	->addTab('content', ['placement' => 'left'])
		->addGroup('billboard')

			// Header
			->addText('header', [
					'label' => 'Header',
					'ui' => $config->ui
				])
				->setInstructions('Header for the billboard')
			
			//Button
			->addFields(get_field_partial('modules.button'))
			
	   ->endGroup();

return $billboard;