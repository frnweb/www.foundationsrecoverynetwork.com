<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$deck = new FieldsBuilder('deck');

$deck
	->addFields(get_field_partial('partials.module-settings'));

$deck
	->addTab('content', ['placement' => 'left'])

		//Header
		->addText('header', [
			'label' => 'Deck Header'
	    ])

		//Repeater
		->addRepeater('deck', [
		  'min' => 1,
		  'max' => 10,
		  'button_label' => 'Add Card',
		  'layout' => 'block',
		])
		->addTrueFalse('link_wrapper', [
			'wrapper' => ['width' => 15]
		])
		->addLink('link_url', [
			'wrapper' => ['width' => 85]
		])
		->conditional('link_wrapper', '==', 1)

		//Image 
		->addImage('deck_image')  
		// Card
	  	->addFields(get_field_partial('modules.card'));
    

return $deck;