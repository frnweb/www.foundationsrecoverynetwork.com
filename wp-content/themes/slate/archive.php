<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

$templates = array( 'archive.twig', 'index.twig');

$context = Timber::get_context();

$pagination = 'Timber\Pagination';

$podargs = array(
    // Get post type project
    'post_type' => 'sl_podcasts_cpts',
    'posts_per_page' => 10,
    'paged' => $paged,
    // Order by post date
    'orderby' => array(
        'date' => 'DESC'
    )
);

$resargs = array (
    'post_type' => 'sl_research_cpts', 
    'posts_per_page' => 12,
    'tax_query' => array(
        array(
            'taxonomy' => 'research-type',
            'field'    => 'slug',
            'terms'    => 'research-abstracts',
        ),
    ),
    'paged' => $paged,
    'orderby' => array (
        'date' => 'DESC'
    )
);

$whiteargs = array (
    'post_type' => 'sl_research_cpts', 
    'posts_per_page' => 12,
    'tax_query' => array(
        array(
            'taxonomy' => 'research-type',
            'field'    => 'slug',
            'terms'    => 'white-papers',
        ),
    ),
    'paged' => $paged,
    'orderby' => array (
        'date' => 'DESC'
    )
);

$myCollection = new WP_Query( $podargs );
$context['podcasts'] = new Timber\PostQuery($myCollection);
$context['podcast_categories'] = Timber::get_terms('podcast-category');
$context['pagination'] = new $pagination(array(), $myCollection);

$myResearch = new WP_Query( $resargs );
$context['research'] = new Timber\PostQuery($myResearch);
$context['respagination'] = new $pagination(array(), $myResearch);

$myWhitePapers = new WP_Query( $whiteargs );
$context['white_papers'] = new Timber\PostQuery($myWhitePapers);
$context['whitepagination'] = new $pagination(array(), $myWhitePapers);

$context['url'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$context['title'] = 'Archive';

if ( is_day() ) {
    $context['title'] = 'Archive: ' . get_the_date( 'D M Y' );
} else if ( is_month() ) {
    $context['title'] = 'Archive: ' . get_the_date( 'M Y' );
} else if ( is_year() ) {
    $context['title'] = 'Archive: ' . get_the_date( 'Y' );
} else if ( is_tag() ) {
    $context['title'] = single_tag_title( '', false );
} else if ( is_category() ) {
    $context['title'] = single_cat_title( '', false );
    array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} else if ( is_post_type_archive() ) {
    $context['title'] = post_type_archive_title( '', false );
    array_unshift( $templates, '/archives/archive-' . get_post_type() . '.twig' );
}

$context['posts'] = new Timber\PostQuery();
$context['categories'] = Timber::get_terms('categories');

if ( is_tax('research-type', 'white-papers')) {
    $context['title'] = single_cat_title( '', false );
    Timber::render( '/archives/archive-white_papers.twig', $context );
} else if ( is_tax('research-type', 'research-abstracts')) {
    $context['title'] = single_cat_title( '', false );
    Timber::render( '/archives/archive-research_abstracts.twig', $context );
} else {
    Timber::render( $templates, $context );
}

