import $ from 'jquery'

window.jQuery = $;
window.$ = $;

var hasSubscribed = localStorage.getItem("subcribed");

$("#mc-embedded-subscribe-form").submit(function() {
	$(".sl_content-locker__form").hide();
    localStorage.setItem("subcribed", "true");
});

if(hasSubscribed){
	$(".sl_content-locker__form").hide();
};

