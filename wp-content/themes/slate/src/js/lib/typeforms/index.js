import $ from 'jquery'
import * as typeformEmbed from '@typeform/embed'


const typeForms = () => {
    const popup1 = typeformEmbed.makePopup('https://foundationsrecoverynetwork.typeform.com/to/bPqKFX', {
      mode: 'popup',
      hideHeaders: true,
      hideFooters: true,
      onSubmit: function () {
        console.log('Typeform successfully submitted')
      }
    })
    const popup2 = typeformEmbed.makePopup('https://foundationsrecoverynetwork.typeform.com/to/Vy86FL', {
      mode: 'popup',
      hideHeaders: true,
      hideFooters: true,
      onSubmit: function () {
        console.log('Typeform successfully submitted')
      }
    })
    $('.sl_self-assessment').on('click', function () {
      popup1.open();
    });
    $('.sl_love-assessment').on('click', function () {
      popup2.open();
    });
}///typeForms

export default typeForms