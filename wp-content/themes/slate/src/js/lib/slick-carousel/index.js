//slick carousel node module
$(".sl_slider").slick({

    // normal options...
    infinite: true,
  
    // the magic
    responsive: [{
  
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          infinite: true
        }
  
      }, {
  
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          infinite: true
        }
  
      }]
});

//slick vertical carousel node module
$(".sl_vert_slider").slick({

  // normal options...
  arrows: false,
  vertical: true,
  swipe: true,
  autoplay: true,
  adaptiveHeight: true,
  autoplaySpeed: 3000,
  verticalSwiping: true,
  slidesToShow: 3,
  swipteToSlide: true,


  // the magic
  responsive: [{

      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
      }

    }, {

      breakpoint: 600,
      settings: {
        slidesToShow: 3,
      }

    }]
});

//slick facility carousel node module
$(".sl_facility__carousel").slick({

  slidesToShow: 5,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 3000,
  arrows: false,
  swipeToSlide: true,
  accessibility: false,
  infinite: true,
  responsive: [
    {
      breakpoint: 1650,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 1300,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});

//slick carousel node module
$(".sl_tour_slider").slick({

  // normal options...
  prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
  nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
  infinite: true,

  // the magic
  responsive: [{

      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        infinite: true
      }

    }, {

      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        infinite: true
      }

    }]
});