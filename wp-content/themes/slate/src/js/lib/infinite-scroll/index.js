import $ from 'jquery'
import 'infinite-scroll/dist/infinite-scroll.pkgd.js'

window.jQuery = $;
window.$ = $;

if (navigator.appVersion.indexOf("MSIE 10") == -1)
{
    $('.infinite-scroll-button').removeClass( "hide" );

    $('.sl_podcast__box').infiniteScroll({
        // options
        path: window.location.origin + '/recovery-unscripted-podcast/page/{{#}}',
        append: 'div.sl_podcast',
        history: false,
        hideNav: '.sl_pagination',
        debug: true,
        button: '.infinite-scroll-button',
        // load pages on button click
        scrollThreshold: false,
        // disable loading on scroll
        status: '.page-load-status'
    });
}