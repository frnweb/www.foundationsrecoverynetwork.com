import $ from 'jquery'

window.jQuery = $;
window.$ = $;

$(window).scroll(function() {
  if ($(this).scrollTop() > 150)
   {
      $('#sl_contact-mobile').addClass('sl_active');
   }
  else
   {
      $('#sl_contact-mobile').removeClass('sl_active');
   }
});
