$( document ).ready(function() {
// Sets the map center over Kansas City
var latlng = new google.maps.LatLng(41.850033, -87.6500523);
var mapOptions = {
  zoom: 4,
  center: latlng
}

// Initializes new map and passes it mapOptions credentials
var map;

//Stores makersArray to be cleared on new search
var markersArray = [];

//Creates new marker window on click. 
var infoWindow = new google.maps.InfoWindow;

$("#sl_fac_input").one("click", function() {

    // Try HTML5 geolocation.
    if (navigator.geolocation) {

        if(confirm('Would you like to use your current location?')) {

            navigator.geolocation.getCurrentPosition(function(position) {
                var searchData = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                var geocoder  = new google.maps.Geocoder();             // create a geocoder object
                var location  = new google.maps.LatLng(searchData.lat, searchData.lng);    // turn coordinates into an object          
                geocoder.geocode({'latLng': location}, function (results, status) {
                    if(status == google.maps.GeocoderStatus.OK) {           // if geocode success
                        var add=results[0].formatted_address;         // if address found, pass to processing function
                        $("#sl_fac_input").val(add);
                    }
                });

                var icon = {
                    url: '/wp-content/themes/slate/dist/img/icon/map-pin--primary.svg',
                    scaledSize: new google.maps.Size(30,45)
                }

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(searchData.lat, searchData.lng),
                    animation: google.maps.Animation.DROP,
                    icon: icon,
                    map: map
                });
            
                marker.addListener('click', function() {
                    infoWindow.setContent("Your current location")
                    infoWindow.open(map, marker);
                });
    
                calculateDistances(searchData, myFacilityData);
            });
        }
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
        
    function handleLocationError(browserHasGeolocation, infoWindow, searchData) {
        infoWindow.setPosition(searchData);
        infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }

});

    //Sets toggle to show and hide map/card on radio click

    //CARD TOGGLE
    $(".sl_toggle--card").click(function() {
        $(".sl_toggle--card").addClass('sl_active');
        $(".sl_toggle--map").removeClass('sl_active'); 

        //CAROUSEL  
        $(".mapShow").hide();
        $(".sl_facility__carousel").show();
        $(".sl_facility__carousel").slick('setPosition');

        //DECK
        $(".mapShow").hide();
        $(".sl_facility__deck").show();
    });

    //MAP TOGGLE
    $(".sl_toggle--map").click(function() {
        $(".sl_toggle--map").addClass('sl_active');
        $(".sl_toggle--card").removeClass('sl_active');  

        //CAROUSEL
        $(".sl_facility__carousel").hide();
        $(".mapShow").show();
        map.setZoom(4);

        //DECK
        $(".sl_facility__deck").hide();
        $(".mapShow").show();
        map.setZoom(4);
    });

    //RESIDENTIAL TOGGLE
    $(".sl_toggle--residential").click(function() {
        $('.sl_toggle--residential, .sl_cell--Residential, .sl_cell--PHP').toggleClass('sl_inactive');//Toggles button state, Shows and hides the card

        clearOverlays();

        //Conditional that checks if sl_inactive is a class on each of the cards. 
        if ($(".sl_cell--Residential").hasClass("sl_inactive") &&  !$(".sl_cell--Outpatient").hasClass("sl_inactive")){

            for(let i = 0; i < myFacilityData.length; i++){
                if(myFacilityData[i].facilityType === 'Outpatient'){
                    getMarkers(myFacilityData[i]);
                }
            }
        } else if ($(".sl_cell--Residential").hasClass("sl_inactive") && $(".sl_cell--Outpatient").hasClass("sl_inactive")){


        } else if (!$(".sl_cell--Residential").hasClass("sl_inactive") && !$(".sl_cell--Outpatient").hasClass("sl_inactive")) {

            for(let i = 0; i < myFacilityData.length; i++){
                    getMarkers(myFacilityData[i]);
            }
        } else {

            for(let i = 0; i < myFacilityData.length; i++){
                if(myFacilityData[i].facilityType === 'Residential' || myFacilityData[i].facilityType === 'PHP') {
                    getMarkers(myFacilityData[i]);
                }
            }
        }
    });

    //OUTPATIENT TOGGLE
    $(".sl_toggle--outpatient").click(function() {
        $(".sl_toggle--outpatient, .sl_cell--Outpatient").toggleClass('sl_inactive');//Toggles button state, Shows and hides the card
        
        clearOverlays();

        //Conditional that checks if sl_inactive is a class on each of the cards. 
        if ($(".sl_cell--Outpatient").hasClass("sl_inactive") && !$(".sl_cell--Residential").hasClass("sl_inactive")){
            for(let i = 0; i < myFacilityData.length; i++){
                if(myFacilityData[i].facilityType === 'Residential' || myFacilityData[i].facilityType === 'PHP'){
                    getMarkers(myFacilityData[i]);
                }
            }
        } else if ($(".sl_cell--Residential").hasClass("sl_inactive") && $(".sl_cell--Outpatient").hasClass("sl_inactive")){


        } else if (!$(".sl_cell--Outpatient").hasClass("sl_inactive") &&  !$(".sl_cell--Residential").hasClass("sl_inactive")) {

            for(let i = 0; i < myFacilityData.length; i++){
                    getMarkers(myFacilityData[i]);
            }

        } else {

            for(let i = 0; i < myFacilityData.length; i++){
                if(myFacilityData[i].facilityType === 'Outpatient'){
                    getMarkers(myFacilityData[i]);
                }
            }
        }
    });

    //When search button is clicked, grabs value and passes it to the Distance calculator function
    $("#sl_search_trigger").click(function() {
        if ($("#sl_fac_input").val() != "" ){
            var searchData = $("#sl_fac_input").val();
            calculateDistances(searchData, myFacilityData);
        } else {
            $('#facilityTypeSelect').change();
        }
    });

    //runs click button on enter keypress
    $("#sl_fac_input").keydown(function(e){
        if(e.which == 13){
            $("#sl_search_trigger").click();
        }
    });

    $("#sl_fac_input").keyup(function(e){
    if(e.keyCode == 46 || 8){
        if ($("#sl_fac_input").val() === "" ){
            $('#facilityTypeSelect').change();
        }
    }
    });

    //Check if myFacilityData is an object that exists on window load, grabs markers and markup for all facilities from myFacilityData object passed from twig template, loads map.
    if (window.myFacilityData) {
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        for(let i = 0; i < myFacilityData.length; i++){
            getMarkers(myFacilityData[i]);
            addCarouselMarkup(myFacilityData[i]);
        }
    }

//when tab is changed and search field is empty, it will generate markers/cards based on selected tab.
$('#facilityTypeSelect').change(function() {

    var conceptName = $('#facilityTypeSelect').find(":selected").text();

    var searchData = $("#sl_fac_input").val();

    if(conceptName != "All" && searchData == ""){

        $(".sl_facility__carousel").slick('removeSlide', null, null, true);
        clearOverlays();

        for(let i = 0; i < myFacilityData.length; i++){
            if(conceptName === "Residential/PHP"){
                if(myFacilityData[i].facilityType === "Residential" || myFacilityData[i].facilityType === "PHP"){
                    getMarkers(myFacilityData[i]);
                    addCarouselMarkup(myFacilityData[i]);
                }
            } else if (myFacilityData[i].facilityType === conceptName){
                getMarkers(myFacilityData[i]);
                addCarouselMarkup(myFacilityData[i]);
            }
        }
    }
    else if(conceptName != "All" && searchData != ""){
        $(".sl_facility__carousel").slick('removeSlide', null, null, true);
        clearOverlays();
        var searchData = $("#sl_fac_input").val();
        calculateDistances(searchData, myFacilityData);
    }
    else if (conceptName === "All" && searchData == ""){
        $(".sl_facility__carousel").slick('removeSlide', null, null, true);
        clearOverlays();
        for(let i = 0; i < myFacilityData.length; i++){
            getMarkers(myFacilityData[i]);
            addCarouselMarkup(myFacilityData[i])
        }
    }
    else if(conceptName === "All" && searchData != ""){
        $(".sl_facility__carousel").slick('removeSlide', null, null, true);
        clearOverlays();
        var searchData = $("#sl_fac_input").val();
        calculateDistances(searchData, myFacilityData);
    }
});
 
//Function iterates over myFacilityData objects and creates markup for cards. Appends html tags to div container and adds new element to slick. 
function addCarouselMarkup(myFacilityData){

    var element = $('<div class="sl_card sl_card--facility"></div>')

    //Variable for Facility Type Component
    var facility_type = '<span class="sl_facility__type">' + myFacilityData.facilityType + '</span>';

    //Carousel Image
    if (myFacilityData.facilityAccreditation[0] === 'joint_commission') {
        element.append('<div class="sl_card__image" style="background-image:url(' + myFacilityData.facilityImage + ')"><img class="sl_accreditation" src="/wp-content/themes/slate/dist/img/accreditations/jointcommission-logo.png" alt="Joint Commission" />' + facility_type + '</div>');

    } else if ( myFacilityData.facilityAccreditation[0] === 'carf' ) {
        element.append('<div class="sl_card__image" style="background-image:url(' + myFacilityData.facilityImage + ')"><img class="sl_accreditation" src="/wp-content/themes/slate/dist/img/accreditations/carf-logo.png" alt="CARF" />' + facility_type + '</div>');
    }

    //Carousel Content
    element.append('<div class="sl_card__content"><h5>' + myFacilityData.facilityCity + ', ' + myFacilityData.facilityState + '</h5><h3>' + myFacilityData.facilityName + '</h3>'+ myFacilityData.facilityContent +'<a href=/locations/'+ myFacilityData.facilityPage + '/'+ ' class="sl_button sl_button--secondary">Learn More</a>');

    $(".sl_facility__carousel").slick('slickAdd', element);

}

//Function iterates over myFacilityData objects and creates markers based on Lat/Lng. Also sets Marker card content, and adds event listener. setContent only accepts string, not object, can't append.
function getMarkers(myFacilityData){

    var websiteType = (myFacilityData.facilityWebsite != "")? myFacilityData.facilityWebsite : "/locations/" + myFacilityData.facilityPage;
    
    if (myFacilityData.entryType == "contact-deck") {
        var contentString = '<div class="sl_card sl_card--map">' +
            '<div class="sl_row">' +
            '<div class="sl_cell sl_card__image" style="background-image:url(' + myFacilityData.facilityImage + ')">' + 
            '</div>' + 
            '<div class="sl_cell sl_card__content"><span class="sl_facility__type">' + 
            myFacilityData.facilityType + '</span>' + 
            '<h5>' + myFacilityData.facilityCity + ', ' + myFacilityData.facilityState + '</h5>' +
            '<h3>' + myFacilityData.facilityName + '</h3>' +
            '<a href="' + websiteType + '" class="sl_button sl_button--secondary">Visit Website</a>' +
            '<a class="sl_button--directions" href="' + myFacilityData.facilityDirections + '">Get Directions</a>'
            '</div></div></div>'
    } else {
        var contentString = '<div class="sl_card sl_card--map">' +
            '<div class="sl_row">' +
            '<div class="sl_cell sl_card__image" style="background-image:url(' + myFacilityData.facilityImage + ')">' + 
            '</div>' + 
            '<div class="sl_cell sl_card__content"><span class="sl_facility__type">' + 
            myFacilityData.facilityType + '</span>' + 
            '<h5>' + myFacilityData.facilityCity + ', ' + myFacilityData.facilityState + '</h5>' +
            '<h3>' + myFacilityData.facilityName + '</h3>' +
            '<a href="/locations/' + myFacilityData.facilityPage + '/' + '" class="sl_button sl_button--secondary">Learn More</a>'
            '</div></div></div>'
    }

    var icon = {
        url: '/wp-content/themes/slate/dist/img/icon/map-pin--secondary.svg',
        scaledSize: new google.maps.Size(30,45)
    }

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(myFacilityData.facilityLat, myFacilityData.facilityLng),
        animation: google.maps.Animation.DROP,
        icon: icon,
        map: map
    });

    marker.addListener('click', function() {
        infoWindow.setContent(contentString)
        infoWindow.setOptions({
        })
        infoWindow.open(map, marker);
    });

    markersArray.push(marker);
}

//Called to clear all markers from array
function clearOverlays() {
    for (var i = 0; i < markersArray.length; i++ ) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
}

//Filter function based on search value above, grabs all facility Zip Codes and used DistanceMatrix API to return travel distances. Calls calcDistance fucntion after return.
function calculateDistances(searchData,myFacilityData) {
    var origin = searchData;
    var facilityZips = [];

    for(let i = 0; i < myFacilityData.length; i++){
        facilityZips.push(myFacilityData[i].facilityZip)
    }

    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
    {
      origins: [origin],
      destinations: facilityZips,
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.IMPERIAL,
      avoidHighways: false,
      avoidTolls: false
    }, calcDistance);
}

//takes the returned Distances object and only adds cards markup/markers to facilities that are less than 500 miles, and the correct dropdown type.
function calcDistance(response, status) {
    if (status != google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
    } else {
        var origins = response.originAddresses;
        var destinations = response;
        var farFacilities = [];

        clearOverlays();

        //removes all carousel cards
        $(".sl_facility__carousel").slick('removeSlide', null, null, true);

        //grabs corresponding facility tab
        var conceptName = $('#facilityTypeSelect').find(":selected").text()

        for (var i = 0; i < origins.length; i++) {
            var results = response.rows[i].elements;

            for (var j = 0; j < results.length; j++) {
                //changes distance to integer, and removes comma, and "miles"
                if (results[j].status != "ZERO_RESULTS"){
                    var distanceMiles = parseInt(results[j].distance.text.replace(/[^0-9]/g, ''));
                } else {
                    var distanceMiles = 800;
                }
                
                if (distanceMiles < 751 && conceptName === "Residential/PHP") {
                    if(myFacilityData[j].facilityType === "Residential" || myFacilityData[j].facilityType === "PHP"){
                        getMarkers(myFacilityData[j]);
                        addCarouselMarkup(myFacilityData[j]);
                    }
                }
                else if (distanceMiles < 751 && conceptName === myFacilityData[j].facilityType) {                
                    getMarkers(myFacilityData[j]);
                    addCarouselMarkup(myFacilityData[j]);

                } else if (distanceMiles < 751 && conceptName === "All") {
                    getMarkers(myFacilityData[j]);
                    addCarouselMarkup(myFacilityData[j]);

                } else if (distanceMiles > 751 ) {                    
                    farFacilities.push(distanceMiles);
                    noFacilitiesInArea(farFacilities);

                }
            }
        }
    }
}

//Any distance above 500 is added to farFacilities and passed to this function. When it matches the length matches the Facility array, it alerts and generates all Facilities. 
function noFacilitiesInArea(farFacilities) {
    if (farFacilities.length === myFacilityData.length) {
        alert("We don't have any facilities within a 750 mile radius of your searched location. Click OK to see all of our locations mapped out.");
        for(let i = 0; i < myFacilityData.length; i++){
            getMarkers(myFacilityData[i]);
            addCarouselMarkup(myFacilityData[i])
        }
    }
}
});