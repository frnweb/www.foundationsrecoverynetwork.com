<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$context['sidebar'] = Timber::get_sidebar('sidebar.php');
$post = Timber::query_post();
$context['post'] = $post;

//PODCAST CUSTOM POST TYPE
$podargs = array(
    // Get post type project
    'post_type' => 'sl_podcasts_cpts',
    // Order by post date
    'orderby' => array(
        'date' => 'DESC'
    ),
    // Exclude the current page from loop
    'post__not_in' => array( $post->ID )
	);

$context['podcasts'] = Timber::get_posts( $podargs );
$context['podcast_categories'] = Timber::get_terms('podcast-category');
//END PODCAST CUSTOM POST TYPE

//GRABS THE 5 MOST RECENT WEBINAR
$latwebargs = array(
    // Get post type project
    'post_type' => 'sl_webinars_cpts',
    // Get 5 posts
    'posts_per_page' => 5,
    //Gets ACF field webinar_date
    'meta_key' => 'webinar_date',
    // Order by post date
    'orderby' => 'meta_value',
    //reveals latest date in ascending order
    'order' => 'DESC',
    'meta_query' => array(
        array(
           'key'        => 'webinar_date',
           'compare'    => '<=',
           'value'      => date('Y-m-d H:i:s'),
            )
        )  
    );
$context['latwebinars'] = Timber::get_posts( $latwebargs );
//END 5 MOST RECENT WEBINARS

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 
		'single-' . $post->ID . '.twig',
		'post-types/single-' . $post->post_type . '.twig',
		'single.twig'
	), $context );
}
