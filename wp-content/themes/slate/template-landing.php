<?php
/**
 * Template Name: Landing Page
 * Description: Page template for any top level page. Has access to slate modules.
 */

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

$today = date('Ymd');
$todayTime = date('Y-m-d H:i:s');

//GRABS MOST RECENT POSTS
$articleargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '4', // Number of posts
    'category__not_in' => array(61), //Exclude Press Releases
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['posts'] = Timber::get_posts( $articleargs );

//GRABS THE FACILITIES
$facilityargs = array(
    // Get post type project
    'post_type' => 'sl_locations_cpts',
    //Grab all Posts
    'posts_per_page' => -1
    );
$context['facilities'] = Timber::get_posts( $facilityargs );
//GRABS THE FACILITIES

//GRABS THE MOST UPCOMING WEBINAR
$webargs = array(
    // Get post type project
    'post_type' => 'sl_webinars_cpts',
    // Get 5 posts
    'posts_per_page' => 1,
    //Gets ACF field webinar_date
    'meta_key' => 'webinar_date',
    // Order by post date
    'orderby' => 'meta_value',
    //reveals latest date in ascending order
    'order' => 'DESC',
    'meta_query' => array(
        array(
           'key'		=> 'webinar_date',
           'compare'	=> '>=',
           'value'		=> $todayTime,
            )
        )  
    );
$context['webinars'] = Timber::get_posts( $webargs );
//GRABS THE MOST UPCOMING WEBINAR

//GRABS THE 5 MOST RECENT WEBINAR
$latwebargs = array(
    // Get post type project
    'post_type' => 'sl_webinars_cpts',
    // Get 5 posts
    'posts_per_page' => 6,
    //Gets ACF field webinar_date
    'meta_key' => 'webinar_date',
    // Order by post date
    'orderby' => 'meta_value',
    //reveals latest date in ascending order
    'order' => 'DESC',
    'meta_query' => array(
        array(
           'key'		=> 'webinar_date',
           'compare'	=> '<',
           'value'		=> $today,
        )
    )
);
$context['latwebinars'] = Timber::get_posts( $latwebargs );
//GRABS THE 5 MOST RECENT WEBINAR

//GRABS THE 5 MOST RECENT PRESS RELEASES
$pressargs = array(
    // Get post type project
    'post_type' => 'post',
    // Get 5 posts
    'posts_per_page' => 5,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms' => 'press-release'
        ),
    ),
    // Order by post date
    'orderby' => array(
        'date' => 'DESC'
    ));
$context['press_release'] = Timber::get_posts( $pressargs );
//GRABS THE 5 MOST RECENT PRESS RELEASES

//GRABS THE 4 MOST RECENT PODCASTS
$podargs = array(
    // Get post type project
    'post_type' => 'sl_podcasts_cpts',
    // Get 4 posts
    'posts_per_page' => 4,
    // Order by post date
    'orderby' => array(
        'date' => 'DESC'
    ));
$context['podcasts'] = Timber::get_posts( $podargs );
//GRABS THE 4 MOST RECENT PODCASTS

//GRABS THE MOST UPCOMING CONFERENCE
$confargs = array(
    // Get post type project
    'post_type' => 'sl_event_cpts',
    // Get 1 Post
    'posts_per_page' => 1,
    //Gets ACF field webinar_date
    'meta_key' => 'date_start',
    // Order by post date
    'orderby' => 'meta_value',
    //reveals latest date in ascending order
    'order' => 'ASC',
    'meta_query' => array(
        array(
           'key'		=> 'date_start',
           'compare'	=> '>=',
           'value'		=> $today,
            )
        )
    );
$context['conferences'] = Timber::get_posts( $confargs );
//GRABS THE MOST UPCOMING CONFERENCE

//GRABS ALL EVENTS
$eventsargs = array(
    // Get post type project
    'post_type' => 'sl_event_cpts',
   //Gets ACF field webinar_date
    'meta_key' => 'date_start',
    // Order by post date
    'orderby' => 'meta_value',
    //reveals latest date in ascending order
    'order' => 'ASC'
    );
$context['events'] = Timber::get_posts( $eventsargs );
//GRABS ALL EVENTS

$context['location_categories'] = Timber::get_terms('location-category');

Timber::render( array( 'templates/landing-page.twig', 'page.twig' ), $context );
