<?php 

//Authors ok to make public
	//No user page link will show on any page or post unless a "yes" is placed in the "make author public" contact field for the user.
	//anna = id=11
	//Siobhan = id=14
	//Dani = id=17
	//old: $approved_authors = array(11,14);
		//if(!in_array($curauth->id,$approved_authors,true))
	//New: strtolower(get_the_author_meta('public'))=='yes' //Publicizes author page


get_header(); ?>
	<?php
	if(get_post_meta($post->ID, 'pyre_full_width', true) == 'yes') {
		$content_css = 'width:100%';
		$sidebar_css = 'display:none';
	}
	elseif(get_post_meta($post->ID, 'pyre_sidebar_position', true) == 'left') {
		$content_css = 'float:right;';
		$sidebar_css = 'float:left;';
	} elseif(get_post_meta($post->ID, 'pyre_sidebar_position', true) == 'right') {
		$content_css = 'float:left;';
		$sidebar_css = 'float:right;';
	}
	?>
	<div id="content" style="<?php echo $content_css; ?>">
		<?php if(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php global $data; if($data['featured_images'] && has_post_thumbnail()): ?>
			<div class="image">
				<?php the_post_thumbnail('blog-large'); ?>
			</div>
			<?php endif; ?>
			<div class="post-content">
				<?php the_content(); ?>
				<?php wp_link_pages(); ?>
			</div>
			<?php 
			//Authorship Trigger
			if(strtolower(get_the_author_meta('public'))=='yes' ) { 
				?><div class="post_author" style="clear:left; border-top: 1px solid #999; margin-top:50px;">
				<?php the_author_posts_link(); ?><?php if(get_the_author_meta('position')) echo ", " . get_the_author_meta('position'); /* ?><a href="<?php the_author_link(); ?>"><?php the_author(); ?></a><?php */ ?>
				<?php 
				if(get_the_author_meta('googleplus')) : ?>
				
				<link rel="author" href="<?=the_author_meta('googleplus'); ?>?rel=author" /><?php 
				endif; ?>
			
			</div>
			<?php } ?>
			<?php //if($data['comments_pages']): ?>
				<?php //wp_reset_query(); ?>
				<?php //comments_template(); ?>
			<?php //endif; ?>
		</div>
		<?php endif; ?>
	</div>
	<div id="sidebar" style="<?php echo $sidebar_css; ?>"><?php generated_dynamic_sidebar(); ?></div>
<?php get_footer(); ?>