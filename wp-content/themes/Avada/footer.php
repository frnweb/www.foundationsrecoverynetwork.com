		</div>
	</div>
	<div id="footer-contact" class="module cta">
		<div class="inner">
			<div class="row">
				<div class="column cta__content medium-4">
					<div class="cta__card">
					<h2>Creating lifetime relationships for long-term recovery.</h2>
				</div>
				</div>
				<div class="column cta__buttons medium-8">
					<div class="button-holder">
						<div class="call-info"><?php echo do_shortcode('[frn_phone class="frn_phone_button"]'); ?></div>
						<a class="button button--insurance" href="<?php get_home_url(); ?>/insurance-check/" alt="Insurance Check">Get Started Now</a>
					</div>
				</div>
			</div>
		</div>
	</div><!--/#footer-contact-->

	<?php global $data; ?>
	<?php if($data['footer_widgets']): ?>
	<footer class="footer-area">
		<div class="avada-row">
			<section class="columns columns-<?php echo $data['footer_widgets_columns']; ?>">
				<article class="col">
				<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 1')): 
				endif;
				?>
				</article>

				<article class="col">
				<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 2')): 
				endif;
				?>
				</article>

				<article class="col">
				<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 3')): 
				endif;
				?>
				</article>

				<article class="col last">
				<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 4')): 
				endif;
				?>
				</article>
			</section>
		</div>
		<div class="footer-accreditations">
			<div class="inner">
				<div class="row">
					<div class="accred-logo column hide-for-small">
						<img class="jointcommission-logo" alt="The Joint Commission "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/jointcommission.png">
					</div><!--/.accred-logo-->
					<div class="accred-logo column">
						<img class="naatp-logo" alt="NAATP "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/naatp-horizontal-white.png">
					</div><!--/.accred-logo-->
					<div class="accred-logo column">
						<script src="https://static.legitscript.com/seals/3417923.js"></script>
					</div><!--/.accred-logo--->
					<div class="accred-logo column">
						<a id="bbblink" class="rbhzbum" href="https://www.bbb.org/nashville/business-reviews/drug-abuse-information/foundations-recovery-network-in-brentwood-tn-37038606#bbbseal" title="Foundations Recovery Network, LLC, Drug Abuse Information, Brentwood, TN" style="display: block;position: relative;overflow: hidden; width: 150px; height: 57px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="https://seal-nashville.bbb.org/logo/rbhzbum/foundations-recovery-network-37038606.png" width="300" height="57" alt="Foundations Recovery Network, LLC, Drug Abuse Information, Brentwood, TN" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); (function(){var s=document.createElement('script');s.src=bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2Ffoundations-recovery-network-37038606.js');s.type='text/javascript';s.async=true;var st=document.getElementsByTagName('script');st=st[st.length-1];var pt=st.parentNode;pt.insertBefore(s,pt.nextSibling);})();</script>
					</div><!--/.accred-logo--->
					<div class="accred-logo column hide-for-small">
						<img class="carf-logo" alt="CARF "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/CARF_logo.png">
					</div><!--/.accred-logo--->
				</div><!-- /.row -->
			</div><!-- /.inner -->
		</div><!-- /.footer-accreditations -->
	</footer>
	<?php endif; ?>
	<?php if($data['footer_copyright']): ?>
	<footer id="footer">
		<div class="avada-row">
		<?php if($data['icons_footer']): ?>
		<?php
		if($data['icons_footer_new']) {
			$target = '_blank';
		} else {
			$target = '';
		}
		?>
		<ul class="social-networks">
			<?php if($data['facebook_link']): ?>
			<li class="facebook"><a target="<?php echo $target; ?>" href="<?php echo $data['facebook_link']; ?>">Facebook</a>
				<div class="popup">
					<div class="holder">
						<p>Facebook</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['twitter_link']): ?>
			<li class="twitter"><a target="<?php echo $target; ?>" href="<?php echo $data['twitter_link']; ?>">Twitter</a>
				<div class="popup">
					<div class="holder">
						<p>Twitter</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['linkedin_link']): ?>
			<li class="linkedin"><a target="<?php echo $target; ?>" href="<?php echo $data['linkedin_link']; ?>">LinkedIn</a>
				<div class="popup">
					<div class="holder">
						<p>LinkedIn</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['rss_link']): ?>
			<li class="rss"><a target="<?php echo $target; ?>" href="<?php echo $data['rss_link']; ?>">RSS</a>
				<div class="popup">
					<div class="holder">
						<p>RSS</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['dribbble_link']): ?>
			<li class="dribbble"><a target="<?php echo $target; ?>" href="<?php echo $data['dribbble_link']; ?>">Dribbble</a>
				<div class="popup">
					<div class="holder">
						<p>Dribbble</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['youtube_link']): ?>
			<li class="youtube"><a target="<?php echo $target; ?>" href="<?php echo $data['youtube_link']; ?>">Youtube</a>
				<div class="popup">
					<div class="holder">
						<p>Youtube</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['pinterest_link']): ?>
			<li class="pinterest"><a target="<?php echo $target; ?>" href="<?php echo $data['pinterest_link']; ?>" class="pinterest">Pinterest</a>
				<div class="popup">
					<div class="holder">
						<p>Pinterest</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['flickr_link']): ?>
			<li class="flickr"><a target="<?php echo $target; ?>" href="<?php echo $data['flickr_link']; ?>" class="flickr">Flickr</a>
				<div class="popup">
					<div class="holder">
						<p>Flickr</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['vimeo_link']): ?>
			<li class="vimeo"><a target="<?php echo $target; ?>" href="<?php echo $data['vimeo_link']; ?>" class="vimeo">Vimeo</a>
				<div class="popup">
					<div class="holder">
						<p>Vimeo</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['tumblr_link']): ?>
			<li class="tumblr"><a target="<?php echo $target; ?>" href="<?php echo $data['tumblr_link']; ?>" class="tumblr">Tumblr</a>
				<div class="popup">
					<div class="holder">
						<p>Tumblr</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<!-- <?php if($data['google_link']): ?>
			<li class="google"><a target="<?php echo $target; ?>" href="<?php echo $data['google_link']; ?>" class="google">Google+</a>
				<div class="popup">
					<div class="holder">
						<p>Google</p>
					</div>
				</div>
			</li>
			<?php endif; ?> -->
			<?php if($data['digg_link']): ?>
			<li class="digg"><a target="<?php echo $target; ?>" href="<?php echo $data['digg_link']; ?>" class="digg">Digg</a>
				<div class="popup">
					<div class="holder">
						<p>Digg</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['blogger_link']): ?>
			<li class="blogger"><a target="<?php echo $target; ?>" href="<?php echo $data['blogger_link']; ?>" class="blogger">Blogger</a>
				<div class="popup">
					<div class="holder">
						<p>Blogger</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['skype_link']): ?>
			<li class="skype"><a target="<?php echo $target; ?>" href="<?php echo $data['skype_link']; ?>" class="skype">Skype</a>
				<div class="popup">
					<div class="holder">
						<p>Skype</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['myspace_link']): ?>
			<li class="myspace"><a target="<?php echo $target; ?>" href="<?php echo $data['myspace_link']; ?>" class="myspace">Myspace</a>
				<div class="popup">
					<div class="holder">
						<p>Myspace</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['deviantart_link']): ?>
			<li class="deviantart"><a target="<?php echo $target; ?>" href="<?php echo $data['deviantart_link']; ?>" class="deviantart">Deviantart</a>
				<div class="popup">
					<div class="holder">
						<p>Deviantart</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['yahoo_link']): ?>
			<li class="yahoo"><a target="<?php echo $target; ?>" href="<?php echo $data['yahoo_link']; ?>" class="yahoo">Yahoo</a>
				<div class="popup">
					<div class="holder">
						<p>Yahoo</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['reddit_link']): ?>
			<li class="reddit"><a target="<?php echo $target; ?>" href="<?php echo $data['reddit_link']; ?>" class="reddit">Reddit</a>
				<div class="popup">
					<div class="holder">
						<p>Reddit</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['forrst_link']): ?>
			<li class="forrst"><a target="<?php echo $target; ?>" href="<?php echo $data['forrst_link']; ?>" class="forrst">Forrst</a>
				<div class="popup">
					<div class="holder">
						<p>Forrst</p>
					</div>
				</div>
			</li>
			<?php endif; ?>
			<?php if($data['custom_icon_name'] && $data['custom_icon_link'] && $data['custom_icon_image']): ?>
			<li class="custom"><a target="<?php echo $target; ?>" href="<?php echo $data['custom_icon_link']; ?>"><img src="<?php echo $data['custom_icon_image']; ?>" alt="<?php echo $data['custom_icon_name']; ?>" /></a>
				<div class="popup">
					<div class="holder">
						<p><?php echo $data['custom_icon_name']; ?></p>
					</div>
				</div>
			</li>
			<?php endif; ?>
		</ul>
		<?php endif; ?>
			<ul class="copyright">
				<li>
					<?php echo $data['footer_text'] ?>

					<?php echo do_shortcode('[frn_footer startyear="1995" paragraph="no"]'); ?>
				</li>
			</ul>
		</div>
	</footer>
	<?php endif; ?>
	</div><!-- wrapper -->
	<?php //include_once('style_selector.php'); ?>
	<?php echo $data['space_body']; ?>

	<?php wp_footer(); ?>
</body>
</html>