<?php
/*
*
Template Name: New Homepage
*
*/
get_header(); ?>
	<div id="content" class="full-width new-homepage">
		<div id="billboard" class="module duo duo--billboard">
			<div class="inner expanded">
				<div class="row">
					<div class="column duo__content medium-6">
						<div class="duo__card">
							<div class="accred-container">
								<div class="accred-logo">
									<img class="jointcommission-logo" alt="The Joint Commission "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/jointcommission.png">
								</div><!--/.accred-logo-->
								<div class="accred-logo">
									<img class="naatp-logo" alt="NAATP "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/naatp-stacked-white.png">
								</div><!--/.accred-logo-->
								<div class="accred-logo">
									<script src="https://static.legitscript.com/seals/3417923.js"></script>
								</div><!--/.accred-logo-->
								<div class="accred-logo">
									<img class="carf-logo" alt="CARF "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/CARF_logo.png">
								</div><!--/.accred-logo-->
							</div><!--/.accred-container-->
							<h1>Providing Integrated Treatment</h1>
							<p>For Substance Abuse & Mental Health Issues</p>
							<div class="call-info"><?php echo do_shortcode('[frn_phone class="frn_phone_button"]'); ?></div>
						</div><!--/.duo__card-->
					</div><!--/.duo__content-->
					<div class="column duo__media medium-6">
						<div class="media-container" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/testimonial-video.jpg)">
							<a class="button button--video" href="#" alt="Insurance Check" data-featherlight="#video-modal">Hear Margaret's story</a>
						</div>
					</div><!--/.duo__media-->
				</div><!--/.row-->
			</div><!--/.inner.expanded-->
		</div><!--/#billboard-->
		<div id="residential" class="module deck">
			<div class="inner">
				<h2>View Residential Treatment</h2>
				<div class="row hide-for-small">
					<div class="column medium-4">
						<div class="card">
							<a href="/residential-treatment/black-bear-lodge-georgia/" alt="Black Bear Lodge Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/blackbear.jpg)">
							<span>More Info +</span></a>
							<h3>Black Bear Lodge</h3>
							<p>Sautee Nacoochee, GA</p>
						</div><!--/.card-->
					</div><!--/.column-->
					<div class="column medium-4">
						<div class="card">
							<a href="/residential-treatment/the-canyon-malibu-california/" alt="The Canyon Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/canyon.jpg)">
							<span>More Info +</span></a>
							<h3>The Canyon at Peace Park</h3>
							<p>Malibu, CA</p>
						</div><!--/.card-->
					</div><!--/.column-->
					<div class="column medium-4">
						<div class="card">
							<a href="/residential-treatment/michaels-house-palm-springs-california/" alt="Michael's House Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/michaelshouse.jpg)">
							<span>More Info +</span></a>
							<h3>Michael's House</h3>
							<p>Palm Springs, CA</p>
						</div><!--/.card-->
					</div><!--/.column-->
					<div class="column medium-4">
						<div class="card">
							<a href="/residential-treatment/skywood-recovery-center/" alt="Skywood Recovery Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/skywood.jpg)">
								<span>More Info +</span></a>
							<h3>Skywood Recovery</h3>
							<p>Augusta, MI</p>
						</div><!--/.card-->
					</div><!--/.column-->
					<div class="column medium-4">
						<div class="card">
							<a href="/residential-treatment/oaks-la-paloma-memphis-tennessee/" alt="The Oaks Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/oaks.jpg)">
							<span>More Info +</span></a>
							<h3>The Oaks at La Paloma</h3>
							<p>Memphis, TN</p>
						</div><!--/.card-->
					</div><!--/.column-->
					<div class="column medium-4">
						<div class="card">
							<a href="/residential-treatment/talbott-recovery-atlanta/" alt="Talbott Recovery Info"style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/Talbott.jpg)">
							<span>More Info +</span></a>
							<h3>Talbott Recovery</h3>
							<p>Atlanta, GA</p>
						</div><!--/.card-->
					</div><!--/.column-->
				</div><!--/.row-->
		
				<div id="facility-slider" class="facility__slider show-for-small-only">
					<div class="facility__card">
						<a class="facilty__image" href="/residential-treatment/black-bear-lodge-georgia/" alt="Black Bear Lodge Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/blackbear.jpg)">
						<span>More Info +</span></a>
						<h3><a href="/residential-treatment/black-bear-lodge-georgia/" alt="Black Bear Lodge Info">Black Bear Lodge</a></h3>
						<p>Sautee Nacoochee, GA</p>
					</div><!--/.facility__card-->
					<div class="facility__card">
						<a class="facilty__image" href="/residential-treatment/the-canyon-malibu-california/" alt="The Canyon Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/canyon.jpg)">
						<span>More Info +</span></a>
						<h3><a href="/residential-treatment/the-canyon-malibu-california/" alt="The Canyon Info">The Canyon at Peace Park</a></h3>
						<p>Malibu, CA</p>
					</div><!--/.facility__card-->
					<div class="facility__card">
						<a class="facilty__image" href="/residential-treatment/michaels-house-palm-springs-california/" alt="Michael's House Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/michaelshouse.jpg)">
						<span>More Info +</span></a>
						<h3><a href="/residential-treatment/michaels-house-palm-springs-california/" alt="Michael's House Info">Michael's House</a></h3>
						<p>Palm Springs, CA</p>
					</div><!--/.facility__card-->
					<div class="facility__card">
						<a class="facilty__image" href="/residential-treatment/skywood-recovery-center/" alt="Skywood Recovery Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/skywood.jpg)">
							<span>More Info +</span></a>
						<h3><a href="/residential-treatment/skywood-recovery-center/" alt="Skywood Recovery Info">Skywood Recovery</a></h3>
						<p>Augusta, MI</p>
					</div><!--/.facility__card-->
					<div class="facility__card">
						<a class="facilty__image" href="/residential-treatment/oaks-la-paloma-memphis-tennessee/" alt="The Oaks Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/oaks.jpg)">
						<span>More Info +</span></a>
						<h3><a href="/residential-treatment/oaks-la-paloma-memphis-tennessee/" alt="The Oaks Info"> The Oaks at La Paloma</a></h3>
						<p>Memphis, TN</p>
					</div><!--/.facility__card-->
					<div class="facility__card">
						<a class="facilty__image" href="/residential-treatment/talbott-recovery-atlanta/" alt="Talbott Recovery Info" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/Talbott.jpg)">
						<span>More Info +</span></a>
						<h3><a href="/residential-treatment/talbott-recovery-atlanta/" alt="Talbott Recovery Info">Talbott Recovery</a></h3>
						<p>Atlanta, GA</p>
					</div> <!--/.facility__card-->
				</div><!--/#facility-slider-->
			</div><!--/.inner-->
		</div><!---/#residential-->
		<div id="insurance" class="module duo duo--insurance">
			<div class="inner">
				<div class="row">
					<div class="column duo__content medium-6">
						<div class="duo__card">
							<h2>Insurance Providers</h2>
							<p>Do you have questions about your insurance policy? We have a special team focused solely on communicating with insurance companies to sort out coverage details for you.</p>
							<div class="call-info"><?php echo do_shortcode('[frn_phone class="frn_phone_button"]'); ?></div>
							<a class="button button--insurance" href="<?php get_home_url(); ?>/insurance-check/" alt="Insurance Check">Insurance Check</a>
						</div><!--/.duo__card-->
					</div><!--/.duo__content-->
					<div class="column duo__media medium-6">
						<div class="media-container">
							<div class="row small-up-2">
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/cigna.png" alt="cigna" class="insurance-logos">
								</div><!--/.column-->
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/magellan.png" alt="magellan" class="insurance-logos">
								</div><!--/.column-->
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/aetna.png" alt="aetna" class="insurance-logos">
								</div><!--/.column-->
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/humana.png" alt="humana" class="insurance-logos">
								</div><!--/.column-->
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/valueoptions.png" alt="valueoptions" class="insurance-logos">
								</div><!--/.column-->
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/beaconhealth.png" alt="beacon health" class="insurance-logos">
								</div><!--/.column-->
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/bluecrossblueshield.png" alt="bluecross blueshield" class="insurance-logos">
								</div><!--/.column-->
								<div class="column">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance/coresource.png" alt="coresource" class="insurance-logos">
								</div><!--/.column-->
							</div><!--/.row.small-up-1.medium-up-2-->
						</div><!--/.media-container-->
					</div><!--/.duo__media-->
				</div><!--/.row-->
			</div><!--/.inner-->
		</div><!--/#insurance-->
		<div id="treatment" class="module duo duo--tabs">
			<div class="inner expanded">
				<div class="row hide-for-small">
					<div class="column duo__media medium-6">
						<div id="highlight-tabs" class="treatment-image-holder row">
							<div id="treatment-1" class="treatment-image column medium-6 active" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/residential-care.jpg)">
								<h3>Residential Care</h3>
								<span>Learn More +</span>
							</div><!--/#treatment-1-->
							<div id="treatment-2" class="treatment-image column medium-6" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/outpatient-care.jpg)">
								<h3>Outpatient Care</h3>
								<span>Learn More +</span>
							</div><!--/#treatment-2-->
							<div id="treatment-3" class="treatment-image column medium-6" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/treatment-model.jpg)">
								<h3>FRN Treatment Model</h3>
								<span>Learn More +</span>
							</div><!--/#treatment-3-->
							<div id="treatment-4" class="treatment-image column medium-6" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/integrated-treatment.jpg)">
								<h3>Integrated Treatment</h3>
								<span>Learn More +</span>
							</div><!--/#treatment-4-->
						</div><!--/#highlight-tabs-->
					</div><!--/.duo__media-->
					<div class="column duo__content medium-6">
						<div class="duo__card">
							<div id="treatment-content-1" class="treatment-card active">
								<h2>Residential Care</h2>
								<p>Residential programs allow patients to dedicate all of their time and energy toward their healing and growth. In this environment, everyday distractions and temptations simply aren’t available. By focusing on yourself, you improve your own life while preparing to return to those you love as a better parent, child, partner or friend.</p>
								<a href="/residential-treatment/" alt="Residential Treatment">Learn More</a>
							</div><!--/#treatment-content-1-->
							<div id="treatment-content-2" class="treatment-card">
								<h2>Outpatient Care</h2>
								<p>For those who need a less-structured setting for treatment, FRN offers a host of outpatient services at a variety of locations across the country. Our outpatient programs are singularly dedicated to helping patients improve their overall quality of life. We have adapted our integrated treatment model to fit patients' busy schedules with convenient sessions day and evening.</p>
								<a href="/outpatient-treatment/" alt="Outpatient Treatment">Learn More</a>
							</div><!--/#treatment-content-2-->
							<div id="treatment-content-3" class="treatment-card">
								<h2>FRN Treatment Model</h2>
								<p>The FRN treatment model focuses on the goals, needs and desires of the individual patient. Creating unique, patient-centered treatment plans produces a higher rate of sustained recovery. This customized approach, along with our top-notch staff and innovative therapeutic methods, sets us apart from other rehab options.</p>
								<a href="/about-us/" alt="FRN Treatment Model">Learn More</a>
							</div><!--/#treatment-content-3-->
							<div id="treatment-content-4" class="treatment-card">
								<h2>Integrated Treatment</h2>
								<p>FRN is the country’s premier provider of integrated treatment, addressing both mental health and substance use disorders in the same setting to offer the best chance of lasting recovery. We treat each individual as a whole person so that everyone can regain their motivation and inspiration, reclaiming their identities and living healthier, happier lives.</p>
								<a href="/about-us/integrated-treatment-model/" alt="Integrated Treatment">Learn More</a>
							</div><!--/#treatment-content-4-->
						</div><!--/.duo__card-->
					</div><!--/.duo__content-->
				</div><!--/.row-->
				<div class="treatment-holder show-for-small-only">
					<div class="treatment-images" data-featherlight="#residential-modal" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/residential-care.jpg)">
						<h3>Residential Care</h3>
					</div><!--/.treatment-images-->
					<div class="treatment-images" data-featherlight="#outpatient-modal" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/outpatient-care.jpg)">
						<h3>Outpatient Care</h3>
					</div><!--/.treatment-images-->
					<div class="treatment-images" data-featherlight="#treatment-modal" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/treatment-model.jpg)">
						<h3>FRN Treatment Model</h3>
					</div><!--/.treatment-images-->
					<div class="treatment-images" data-featherlight="#integrated-modal" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/integrated-treatment.jpg)">
						<h3>Integrated Treatment</h3>
					</div><!--/.treatment-images-->
				</div><!--/.treatment-holder-->
			</div><!--/.inner.expanded-->
		</div><!--/#treatment-->
		<div id="accreditations" class="module accreditations">
			<div class="inner">
				<h2>About Our Accreditation</h2>
				<p>The process of accreditation ensures Foundations Recovery Network maintains a strong therapeutic community of top-notch clinicians who focus on both effective treatment and patient safety. Accredited treatment centers attract the most qualified clinicians and staff because they know they’ll be working in an environment where quality care comes first.</p>
				<div class="row small-up-1 medium-up-2">
					<div class="column">
						<div class="accred-card" data-featherlight="#legitscript-modal">
							<script src="https://static.legitscript.com/seals/3417923.js"></script>
							<h3>LegitScript</h3>
							<span>Learn more +</span>
						</div><!--/.accred-card-->
					</div><!--/.column-->
					<div class="column">
						<div class="accred-card" data-featherlight="#jointcommission-modal">
							<img class="jointcommission-logo" alt="The Joint Commission "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/jointcommission.png">
							<h3>Joint Commission</h3>
							<span>Learn more +</span>
						</div><!--/.accred-card-->
					</div><!--/.column-->
					<div class="column">
						<div class="accred-card" data-featherlight="#naatp-modal">
							<img class="naatp-logo" alt="NAATP "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/naatp-stacked-color-full.svg">
							<h3>NAATP</h3>
							<span>Learn more +</span>
						</div><!--/.accred-card-->
					</div><!--/.column-->
					<div class="column">
						<div class="accred-card" data-featherlight="#carf-modal">
							<img class="carf-logo" alt="CARF "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/CARF_logo.png">
							<h3>CARF</h3>
							<span>Learn more +</span>
						</div><!--/.accred-card-->
					</div><!--/.column-->
				</div><!--/.row-->
			</div><!--/.inner-->
		</div><!--/#accreditations-->
		<div id="community" class="module content-block">
			<div class="inner">
				<h2>Foundations' Community Involvement</h2>
			</div><!--/.inner-->
		</div><!--/#community-->
		<div id="foundations-events" class="module duo duo--basic">
			<div class="inner expanded">
				<div class="row">
					<div class="column duo__content medium-6">
						<div class="duo__card">
							<h2>Foundations Events</h2>
							<p>Foundations Events is constantly seeking to add value to the treatment community, and one way we do this is through our conferences. We have hosted 50+ national conferences and have earned a reputation as the leader in behavioral healthcare events. Experts from around the country provide unique workshops and keynote presentations on a wide variety of subjects, giving attendees the chance to earn up to 24 hours of continuing education credits.</p>
							<a class="button button--secondary" href="/events/conferences/" alt="View Events">View Events</a>
						</div><!--/.duo__card-->
					</div><!--/.duo__content-->
					<div class="column duo__media medium-6">
						<div class="media-container" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/foundations-events.jpg)"></div>
					</div><!--/.duo__meida-->
				</div><!--/.row-->
			</div><!--/.inner.expanded-->
		</div><!--/#foundations-events-->
		<div id="heroes-6k" class="module duo duo--basic">
			<div class="inner expanded">
				<div class="row">
					<div class="column duo__media medium-6">
						<div class="media-container" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/images/homepage/heroes.jpg)"></div>
					</div><!--/.duo__media-->
					<div class="column duo__content medium-6 media-left">
						<div class="duo__card">
							<h2>Heroes in Recovery</h2>
							<p>Heroes in Recovery is a movement that seeks to remove the stigma associated with getting addiction treatment and being in recovery. We believe it’s important for the public to understand, embrace and encourage people who are finding a way to thrive and live the lives that they desire and deserve.</p>
							<p>The centerpiece of the Heroes in Recovery movement is shared recovery stories. When those who are in recovery from addiction and mental health issues take time to talk about their journey to healing, others are inspired to get treatment or stay on course in sobriety.</p>
							<a class="button button--secondary" href="/about-us/heroes-in-recovery/" alt="Join the Movement">Join the Movement</a>
						</div><!--/.duo__card-->
					</div><!--/.duo__content-->
				</div><!--/.row-->
			</div><!--/.inner.expanded-->
		</div><!--/#heroes-6k-->
		<div id="recent-articles" class="module content-block">
			<div class="inner">
				<h2>Recent Articles</h2>
			</div><!--/.inner-->
		</div><!--/#featured-articles-->
		<div id="top-post" class="module duo duo--post">
			<div class="inner"> 	
			<?php 
			// the query
			$featured = new WP_Query( 
				array(
					'post_type' => 'post',
					'posts_per_page' => 1,
				) ); ?>

			<?php if ( $featured->have_posts() ) : ?>

				<!-- the loop -->
				<?php while ( $featured->have_posts() ) : $featured->the_post();?>
					<div class="row">
						<div class="post__media column medium-6">
							<div class="media-container"
							<?php if(has_post_thumbnail()): ?>
								style="background-image: url( <?php the_post_thumbnail_url(); ?> )"
							<?php endif; ?>
							></div>
						</div><!--/.post__media-->
						<div class="post__content column medium-6">
							<div class="post__card">
								<h2><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
								<?php
								$trimexcerpt = get_the_excerpt();
								$shortexcerpt = wp_trim_words( $trimexcerpt, $num_words = 30, $more = '… ' ); 

								echo '<p>' . preg_replace('/\[frn_toc\]/', '', $shortexcerpt) . '</p>'; 
								?>
								<a class="button button--secondary" href="<?php the_permalink()?>">Read Article</a>
							</div><!--/.post__card-->
						</div><!--/.post__content-->
					</div><!--/.row-->
				<?php endwhile; ?>
			<?php endif; ?>
			<!-- end of the loop -->
			<?php wp_reset_postdata(); ?>
			</div><!--/.inner-->
		</div><!--/#top-post-->
		<div id="top-three" class="module deck deck--post">
			<div class="inner"> 
				<div class="row medium-up-3">	
			<?php 
			// the query
			$featured = new WP_Query( 
				array(
					'post_type' => 'post',
					'offset' => 1,
					'posts_per_page' => 3,
				) ); ?>

			<?php if ( $featured->have_posts() ) : ?>

				<!-- the loop -->
				<?php while ( $featured->have_posts() ) : $featured->the_post();?>
		
						<div class="post__holder column">
							<div class="post__media">
								<div class="media-container" style="background-image: url( <?php the_post_thumbnail_url(); ?> )"></div>
							</div><!--/.post__media-->
							<div class="post__card">
								<h3><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h3>
								<a class="button button--secondary" href="<?php the_permalink()?>">Read Article</a>
							</div><!--/.post__card-->
						</div><!--/.post__holder-->
				<?php endwhile; ?>
			<?php endif; ?>
			<!-- end of the loop -->
			<?php wp_reset_postdata(); ?>
				</div><!--/.row-->
			</div><!--/.inner-->
		</div><!--/#top-three-->

	</div><!--/#content-->
<!-- MODALS -->
<div class="lightbox" id="video-modal">
	<iframe width="720" height="405" src="https://www.youtube.com/embed/DC_iHe5X9sQ?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div><!--/#legitscript-modal-->
<div class="lightbox" id="legitscript-modal">
	<h2>LegitScript</h2>
	<p>LegitScript verification assures our patients that we follow best practices when it comes to applicable healthcare laws and regulations. This third-party certification demonstrates that we have been vetted thoroughly and proven our commitment to integrity and transparency. Every website verified by LegitScript belongs to a qualified treatment center that demands the best from its clinicians and staff.</p>
</div><!--/#legitscript-modal-->
<div class="lightbox" id="jointcommission-modal">
	<h2>Joint Commission</h2>
	<p>The Gold Seal of Approval from the Joint Commission recognizes our commitment to safety and quality of care. The Joint Commission is the leading organization ensuring hospitals and healthcare providers meet the highest standards.</p>
</div><!--/#jointcomission-modal-->
<div class="lightbox" id="naatp-modal">
	<h2>NAATP</h2>
	<p>The National Association of Addiction Treatment Providers has been the addiction service profession’s vision, voice and thought leader since 1978. NAATP is a nonprofit professional society of top treatment providers throughout the continuum of care.</p>
</div><!--/#naatp-modal-->
<div class="lightbox" id="carf-modal">
	<h2>CARF</h2>
	<p>CARF accreditation focuses on both quality of treatment and results. It signals our commitment to peak performance and compassionate, quality care for our patients.</p>
</div><!--/#carf-modal-->
<div class="lightbox" id="residential-modal">
	<h2>Residential Care</h2>
	<p>Residential programs allow patients to dedicate all of their time and energy toward their healing and growth. In this environment, everyday distractions and temptations simply aren’t available. By focusing on yourself, you improve your own life while preparing to return to those you love as a better parent, child, partner or friend.</p>
	<a href="/residential-treatment/" alt="Residential Treatment">Learn More</a>
</div><!--/#residential-modal-->
<div class="lightbox" id="outpatient-modal">
	<h2>Outpatient Care</h2>
	<p>For those who need a less-structured setting for treatment, FRN offers a host of outpatient services at a variety of locations across the country. Our outpatient programs are singularly dedicated to not only helping patients begin their sobriety, but also improve their overall quality of life. We have adapted our integrated treatment model to fit patients' busy schedules with convenient sessions day and evening.</p>
	<a href="/outpatient-treatment/" alt="Outpatient Treatment">Learn More</a>
</div><!--/#outpatient-modal-->
<div class="lightbox" id="treatment-modal">
	<h2>FRN Treatment Model</h2>
	<p>The FRN treatment model focuses on the goals, needs and desires of the individual patient. Creating unique, patient-centered treatment plans produces a higher rate of sustained recovery. This customized approach, along with our top-notch staff and innovative therapeutic methods, sets us apart from other rehab options.</p>
	<a href="/about-us/" alt="FRN Treatment Model">Learn More</a>
</div><!--/#treatment-modal-->
<div class="lightbox" id="integrated-modal">
	<h2>Integrated Treatment</h2>
	<p>FRN is the country’s premier provider of integrated treatment, addressing both mental health and substance use disorders in the same setting to offer the best chance of lasting recovery. We treat each individual as a whole person so that everyone can regain their motivation and inspiration, reclaiming their identities and living healthier, happier lives.</p>
	<a href="/about-us/integrated-treatment-model/" alt="Integrated Treatment">Learn More</a>
</div><!--/#integrated-modal-->


<?php get_footer(); ?>