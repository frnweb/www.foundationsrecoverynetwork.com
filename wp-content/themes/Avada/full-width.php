<?php
// Template Name: Full Width
get_header(); ?>
	<div id="content" class="full-width">
		<?php while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php global $data; if($data['featured_images'] && has_post_thumbnail()): ?>
			<div class="image">
				<?php the_post_thumbnail('blog-large'); ?>
			</div>
			<?php endif; ?>
			<div class="post-content">
				<?php the_content(); ?>
			</div>
			<?php 
			//Authorship Trigger
			if(strtolower(get_the_author_meta('public'))=='yes' ) { 
				?><div class="post_author" style="clear:left; border-top: 1px solid #999; margin-top:50px;">
				<?php the_author_posts_link(); ?><?php if(get_the_author_meta('position')) echo ", " . get_the_author_meta('position'); /* ?><a href="<?php the_author_link(); ?>"><?php the_author(); ?></a><?php */ ?>
				<?php 
				if(get_the_author_meta('googleplus')) : ?>
				
				<link rel="author" href="<?=the_author_meta('googleplus'); ?>?rel=author" /><?php 
				endif; ?>
			
			</div>
			<?php } ?>
			<?php //if($data['comments_pages']): ?>
				<?php //wp_reset_query(); ?>
				<?php //comments_template(); ?>
			<?php //endif; ?>
		</div>
		<?php endwhile; ?>
	</div>
<?php get_footer(); ?>